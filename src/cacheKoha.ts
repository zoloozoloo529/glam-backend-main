import mongoose, { Connection, ConnectOptions } from 'mongoose'
import { IKohaItemCreate } from './interfaces'


async function cacheKoha(): Promise<void> {
    const databaseURL = 'mongodb://root:8b2CscR8rXV3KBCY@103.85.184.17:27017/glam?readPreference=secondaryPreferred&connectTimeoutMS=10000&authSource=admin&authMechanism=DEFAULT'
    const options: ConnectOptions = {
        autoIndex: false,
        serverSelectionTimeoutMS: 5000,
        socketTimeoutMS: 45000,
        family: 4
    }
    const convertedData: {
        name?: string
        mainImg?: string
        itemInf?: {
            artist?: {
                name?:string
            }
        },
        viewCount?: number
        fields?:Array<{
            name?: string
            value?: string
        }>
        biblionumber?: number,
        book_name?: string,
        author_koha?: string,
        isbn_code?: string,
        press_year?: number,
        url_image?: string,
        publishercode?: string,
        pages?: string,
        press_org_koha?: string,
        koha_languages?: string
        type?:string
    }[] = []

    await mongoose.connect(databaseURL, options)
    console.log('🚀 ~ Connected to MongoDB');
    const KohaItem = mongoose.model('KohaItem', new mongoose.Schema({
        name: { type: String },
        mainImg: { type: String },
        itemInf: {
          artist: {
            name:{ type: String },
          }
        },
        viewCount: { type: Number, default: 0 },
        fields: [{
          name: { type: String },
          value: { type: String },
        }],
        biblionumber: { type: Number, unique:true, required: true },
        book_name: { type: String },
        author_koha: { type: String },
        isbn_code: { type: String },
        press_year: { type: Number },
        url_image: { type: String },
        publishercode: { type: String },
        pages: { type: String },
        press_org_koha: { type: String },
        koha_languages: { type: String },
        type: { type: String, enum: ['koha'], required: true },
        status: { type: Number, default: 1 }
      },
      {
        timestamps: true
      }), 'kohaItems');
    const cachedCount = await KohaItem.countDocuments({})
    console.log("🚀 ~ Current cached koha books count:", cachedCount)
    

    const checkKohaCountResponse = await fetch(`https://e-lib.cis.mn:3002/book/find_koha_all`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({sortBy:"biblionumber", sortOrder:"desc"})
    })
    const checkKohaCountText = await checkKohaCountResponse.text()
    const checkKohaCount = JSON.parse(checkKohaCountText)
    const total = checkKohaCount.metaData.numberOfRecords
    console.log("🚀 ~ Total book count from KOHA server:", total)

    console.log('🚀 ~ fetching data...')
    const kohaData = await fetch(`https://e-lib.cis.mn:3002/book/find_koha_all`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({page:1,take:total-cachedCount, sortBy:"biblionumber", sortOrder:"desc"})
    })
    const kohaText = await kohaData.text()
    const koha = JSON.parse(kohaText)
    let totalData
    if(koha.data){
        totalData = koha.data
        console.log("🚀 ~ Uncached DATA-length:", totalData.length)
        console.log('🚀 ~ Converting data...')
        totalData.map((el: any) => {
        convertedData.push({
            name: el.book_name,
            mainImg: el.url_image as string || "",
            itemInf: {
                artist: {
                    name: el.author_koha || ""
                }
            },
            fields: [
                {
                    name: "ISBN дугаар",
                    value: el.isbn_code as string || ""
                },
                {
                    name: "Хэвлэгдсэн он",
                    value: el.press_year  as string || ""
                },
                {
                    name: "Хэвлэлийн газар",
                    value: el.publishercode  as string || ""
                },
                {
                    name: "Хуудас",
                    value: el.pages  as string || ""
                },
                {
                    name: "Хэл",
                    value: el.koha_languages  as string || ""
                },
            ],
            biblionumber: el.biblionumber,
            book_name: el.book_name,
            author_koha: el.author_koha,
            isbn_code: el.isbn_code,
            press_year: el.press_year,
            url_image: el.url_image,
            publishercode: el.publishercode,
            pages: el.pages,
            press_org_koha: el.press_org_koha,
            koha_languages: el.koha_languages,
            type: 'koha'
        })
        })
        console.log(`🚀 ~ successfully converted data.`,)
        // console.log(`🚀 ~ Converted`, convertedData)

        console.log(`🚀 ~ Pushing data to database...`)
        const pushInInstance = 100000
        let inserted = 0
        for (let first = 0, second = pushInInstance; first < totalData.length; second+=pushInInstance) {
            const toPush = convertedData.slice(first, second)
            inserted+=toPush.length
            const result = await KohaItem.insertMany(toPush)
            if(!result){
                console.log(`🚀 ~ Failed to push data.`)
                break
            }
            console.log(`🚀 ~ Pushing data... ${inserted/totalData.length*100}/100 `)
            first+=pushInInstance
        }

        await mongoose.disconnect();
        console.log('🚀 ~ Disconnected from MongoDB');
    }else{
        totalData = []
        console.log("🚀 ~ Uncached DATA-length:", totalData.length)
        await mongoose.disconnect();
        console.log('🚀 ~ Disconnected from MongoDB');
    }
}

cacheKoha();