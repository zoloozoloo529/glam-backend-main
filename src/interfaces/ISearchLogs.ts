import { Document } from 'mongoose'

export interface ISearchLog extends Document {
  value: string,
  user?: string,
  status: number
}

export interface ICreateSearchLog {
  value: string,
  user?: string
}
