import { Document } from "mongoose";

export interface IUserLog extends Document {
  user: string;
  system: string;
  type: string;
  address: string;
  user_agent: string;
  params: string;
  data: string;
}

export interface IUserLogCreate {
    user: string;
    system?: string;
    params?: string;
    type: string;
    address: string;
    user_agent: string;
    data: string;
}