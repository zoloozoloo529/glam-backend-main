export interface IBaseHeritage {
    name: string;
    description: string;
    img?: string;
    order: number;
    hidden: boolean;
}

export interface IHeritageRefCreate {
    firstname: string;
    lastname: string;
    registernumber: string;
    heritagetypeid: string;
    heritagesubtypeid: string;
    provinceid: string;
    regionid: string;
    address: string;
    content: string;
    images: any;
}

export interface IHeritageItem {
    name: string
    mainImg: string
    itemInf: {
        artist: {
            name:string
        }
    },
    viewCount: number
    saveCount: number
    fields?:Array<{
        name?: string
        value?: string
    }>
    itemId: string
    heritage_type: string
    unesco_heritage_id: string
    unesco_heritage_name: string
    species_name: string
    species_id: string
    intangible_heritage_desc: string
    heritage_area: string
    heritage_attachments: string[]
    location: string[]
    area_coordinate: string[]
    keeper_name: string
    status: number
}

export interface IHeritageItemCreate {
    name?: string
    mainImg?: string
    itemInf?: {
        artist?: {
            name?:string
        }
    },
    viewCount?: number
    saveCount?: number
    fields?:Array<{
        name?: string
        value?: string
    }>
    itemId?: string
    heritage_type?: string
    unesco_heritage_id?: string
    unesco_heritage_name?: string
    species_name?: string
    species_id?: string
    intangible_heritage_desc?: string
    heritage_area?: string
    heritage_attachments?: string[]
    location?: string[]
    area_coordinate?: string[]
    keeper_name?: string
}

export interface IHeritageItemUpdate {
    name?: string
    mainImg?: string
    itemInf?: {
        artist?: {
            name?:string
        }
    },
    viewCount?: number
    saveCount?: number
    fields?:Array<{
        name?: string
        value?: string
    }>
    itemId?: string
    heritage_type?: string
    unesco_heritage_id?: string
    unesco_heritage_name?: string
    species_name?: string
    species_id?: string
    intangible_heritage_desc?: string
    heritage_area?: string
    heritage_attachments?: string[]
    location?: string[]
    area_coordinate?: string[]
    keeper_name?: string
}