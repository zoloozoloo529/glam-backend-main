export interface IKohaFilter {
    book_name?: string
    isbn?: string
    author_name?: string
    publisher_name?: string
    press_year_min?: number
    press_year_max?: number
}

export interface ILibraryFilter {
    book_category_list: number[],
    book_type_list: number[],
    press_year_min: number,
    press_year_max: number,
    price_min: number,
    price_max: number
}

export interface IHeritageFilter {
    heritage_type: 'Хөдлөх өв' | 'Биет бус өв' | 'Үл хөдлөх өв' | 'Баримтат өв'
    heritage_name?: string
    keeper_name?: string
    heritage_century_id?: string
    species_id?: string
    species_name?: string
    date_range_a?: string
    date_range_b?: string
    province?: string[]
    region?: string[]
}

export interface IArtworkFilter {
    book_name: string
    isbn: string
    author_name: string
    publisher_name: string
    press_year: number
}

export interface IArchiveFilter {
    book_name: string
    isbn: string
    author_name: string
    publisher_name: string
    press_year: number
}

export interface ISearchFilter {
    koha?: IKohaFilter
    heritage?: IHeritageFilter
    artwork?: IArtworkFilter
    education?: IArchiveFilter
}

export interface ISearchBody {
    search?: string
    cache?: boolean
    species_id?: string
    century_id?: string
    keeper_name?: string
    date_range_a?: Date
    date_range_b?: Date
    type?: string
    limit?: number
    page?: number
    offset?: number
    sort_by?: string
    sort_order?: string
    filter?: {
        koha?: IKohaFilter
        heritage?: IHeritageFilter
        artwork?: IArtworkFilter
        archive?: IArchiveFilter
        library?: ILibraryFilter
    }
}
