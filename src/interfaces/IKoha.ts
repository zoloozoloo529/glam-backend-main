import { Document } from 'mongoose'

export interface IKohaItem extends Document {
    name: string
    mainImg: string
    itemInf: {
        artist: {
            name:string
        }
    },
    viewCount: number
    saveCount: number
    fields?:Array<{
        name?: string
        value?: string
    }>
    biblionumber: number,
    book_name: string,
    author_koha: string,
    isbn_code: string,
    press_year: number,
    url_image: string,
    publishercode: string,
    pages: string,
    press_org_koha: string,
    koha_languages: string
    type:string
    status: number
}

export interface IKohaItemCreate {
    name?: string
    mainImg?: string
    itemInf?: {
        artist?: {
            name?:string
        }
    },
    viewCount?: number
    saveCount?: number
    fields?:Array<{
        name?: string
        value?: string
    }>
    biblionumber?: number,
    book_name?: string,
    author_koha?: string,
    isbn_code?: string,
    press_year?: number,
    url_image?: string,
    publishercode?: string,
    pages?: string,
    press_org_koha?: string,
    koha_languages?: string
    type?:string
}

export interface IKohaItemUpdate{
    name?: string
    mainImg?: string
    itemInf?: {
        artist?: {
            name?:string
        }
    },
    viewCount?: number
    fields?:Array<{
        name?: string
        value?: string
    }>
    biblionumber?: number,
    book_name?: string,
    author_koha?: string,
    isbn_code?: string,
    press_year?: number,
    url_image?: string,
    publishercode?: string,
    pages?: string,
    press_org_koha?: string,
    koha_languages?: string
    type?:string
}