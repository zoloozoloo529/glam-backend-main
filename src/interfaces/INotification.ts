import { Document } from 'mongoose'

export interface IBaseNotification extends Document {
    attributes: object
    scheduledAt: Date
    createdBy: string
    updatedBy: string
    user: string
    status: number
}

interface NotificationAttributes {
    Title: string
    Body: string
}

export interface INotificationCreate {
    createdBy: string
    updatedBy: string
    title: string
    body: string
    channels: string[]
    attributes: NotificationAttributes
}

export interface INotification extends INotificationCreate, Document {
    ID: string
    createdAt: Date
    updatedAt: Date
}
export type INotificationUpdate = Partial<INotificationCreate>
