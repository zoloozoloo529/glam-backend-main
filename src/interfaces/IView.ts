import { Document } from 'mongoose'

export interface IView extends Document {
  itemId: string
  type: string
  heritageType: string
  artworkCategory: string
  viewCount: number
  saveCount: number
} 

export interface IViewCreate {
  itemId: string
  type: string
  heritageType?: string
  artworkCategory?: string
}