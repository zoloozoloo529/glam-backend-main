import { Document } from 'mongoose'

export interface IBaseContentItem extends Document {
    img: string
    link: string
    order: number
    hidden: boolean
    status: number
    languages: Map<string, { name: string; description: string }>
}
export interface IContentItem extends IBaseContentItem, Document {}
export type IContentItemCreate = Partial<IBaseContentItem>
