import { Document } from 'mongoose'

export interface IUser extends Document {
    email: string
    password: string
    role: string
    verified: boolean
    verification: string
    firstname: string
    lastname: string
    fcm_token: string
    age: number
    img: string
    isForeigner: boolean
    country: string
    library_password: string
    library_access_token: string
    library_isConnected: boolean
    dan: {
        isConnected: boolean
        data: IDanCitizenIDCardInfo
    }
    activeStatus: number
    status: number
}

export interface ICreateUser {
    email: string
    password: string
    age?: number
    verification: string
    firstname?: string
    lastname?: string
    isForeigner?: boolean
    country?: string
    img?: string
}

export interface IUpdateUser {
    email?: string
    password?: string
    role?: string
    verified?: boolean
    verification?: string
    firstname?: string
    fcm_token?: string
    lastname?: string
    age?: IUser['age']
    library_password?: string
    library_access_token?: string
    library_isConnected?: boolean
    dan?: {
        isConnected?: boolean
        data?: IDanCitizenIDCardInfo
    }
    img?: string
    activeStatus?: number
    status?: number
}

export interface IDanCitizenIDCardInfo {
    aimagCityCode?: string,
    addressStreetName?: string,
    addressDetial?: string,
    aimagCityName?: string,
    regnum?: string,
    civilId?: string,
    firstname?: string,
    lastname?: string,
    surname?: string,
    image?: string,
    nationality?: string,
    gender?: string,
    birthDate?: Date,
    birthDateAsText?: string,
    birthPlace?: string,
    passportIssueDate?: string,
    passportExpireDate?: string,
    passportAddress?: string,
    soumDistrictCode?: string,
    soumDistrictName?: string,
    bagKhorooName?: string,
    addressTownName?: string,
    addressApartmentName?: string,
    bagKhorooCode?: string,
    addressRegionName?: string
}
