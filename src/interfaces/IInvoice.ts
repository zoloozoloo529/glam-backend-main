import { Document } from 'mongoose'

export interface IInvoice extends Document {
  user: string
  isPaid: boolean
  steppePayment: any
  billNumber: string
  amount: number
}

export interface IInvoiceCreate {
  user: string
  steppePayment: any
  billNumber: string
  amount: number
}