import { Document } from 'mongoose';
import { IUser } from './IUser';
import { IResearchFolder } from './IResearchFolder';


export enum researchType {
    "note",
    'quote',
    'highlight'
}

export interface IBaseResearch extends Document {
    user: IUser['_id']
    title: string
    note: string
    edition: string
    locale: string
    settings: JSON
    reference: string
    type: "note" | 'quote' | 'highlight',
    contentItemId: string
    contentItemType: string
    folderId: IResearchFolder['_id']
    publisher?: string
    pageNumber: number
}

export interface IResearch extends IBaseResearch, Document {
    // contentResearchID: string
    // contentResearch: string
}
// export interface ICreateResearch extends IBaseResearch, Document {
//     folderId?: string
// }
export type ICreateResearch = Partial<IBaseResearch> & {
    folderId?: string
}