import { Document } from 'mongoose'

export interface ILocation extends Document {
  latitude: number
  longitude: number
  zoom: number
  name: string
  description: string
  type: string
  image?: string
  address: string
  status: number
  timetable: string
  protection?: string
  heritage_id?: string
} 

export interface ILocationCreate {
  latitude: number
  longitude: number
  name: string
  description: string
  type: string
  image?: string
  address: string
  status?: number
  timetable?: string
  protection?: string
  heritage_id?: string
}