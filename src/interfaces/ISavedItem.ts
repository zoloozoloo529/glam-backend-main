import { Document } from 'mongoose'

export interface ISavedItem extends Document {
    user_id: string
    type: string
    item_id: string
    item_name: string
    main_img: string
    heritage_type :string
}

export interface ISavedItemCreate {
    user_id: string
    type: string
    item_id: string
    item_name: string
    main_img?: string
    heritage_type? :string
}