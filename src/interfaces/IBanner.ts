import { Document } from 'mongoose'

export interface IBaseBanner extends Document {
    name: string
    img: string
    order: number
    type: string
    link: string
    menu: string
    hidden: boolean
}
export interface IBanner extends IBaseBanner, Document {}
export type IBannerCreate = Partial<IBaseBanner>
