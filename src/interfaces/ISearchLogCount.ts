export interface ISearchLogCount {
  value: string
  count: number
}