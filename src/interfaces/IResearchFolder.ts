import { Document } from "mongoose";
import { IUser } from "./IUser";


export interface IResearchFolder extends Document {
    title: string
    user: IUser['_id']
    isShared: boolean
    inviteAccepted: boolean
    folderOwner: {
        _id: IUser['_id']
        email: IUser['email']
        firstname: IUser['firstname']
        lastname: IUser['lastname']
        img: IUser['img']
    },
    folderMembers: Array<{
        _id: IUser['_id']
        firstname: string;
        lastname: string;
        email: string;
        img: string;
    }>;
    status: number
    invitedMembers: IUser['_id'][]
    researches: string[]
}
export interface IResearchFolderAddMember {
    isShared: boolean;
    // folderMembers: Array<{
    //     firstname: string;
    //     lastname: string;
    //     email: string;
    //     img: string;
    // }>;
    invitedMembers: IUser['_id'][]

}
export interface IResearchFolderInviteMember {
    isShared: boolean;
    invitedMembers: IUser['_id'][]
}
export type ICreateResearchFolder = Partial<IResearchFolder>