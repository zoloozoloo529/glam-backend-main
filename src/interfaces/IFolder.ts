import { Document } from 'mongoose'
import { IUser } from './IUser';

export interface IFolder extends Document {
  name: string
  user: IUser['_id'];
  color: string;
  items: {
    artwork: string[];
    koha: string[];
    archive: string[];
    heritage: string[];
    library: string[];
    location?: string[];
  },
  type: string;
  itemCount: number
  status: number
}

export interface IFolderCreate {
  name: string
  color: string;
  user: IUser['_id']
  type?: string;
}

export interface IAddItems {
  artwork?: string[];
  koha?: string[];
  archive?: string[];
  library?: string[];
  heritage?: string[];
  location?: string[];
}