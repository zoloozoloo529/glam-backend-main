import { IAddItems, IFolder, IFolderCreate, IPagination } from '@root/interfaces'
import { Folder } from '@root/models'
import { Types } from 'mongoose'

const createFolder = async (data: IFolderCreate):Promise<IFolder> => {
  const result = await Folder.create(data)
  return result
}

const updateFolder = async (id: Types.ObjectId,  data: any): Promise<IFolder> => {
  const result = await Folder.findByIdAndUpdate(id, data, {
    new: true
  })
  return result
}

const deleteFolder = async (id: Types.ObjectId): Promise<IFolder> => {
  const result = await Folder.findByIdAndUpdate(id, { status: 0 }, {
    new: true
  })
  return result
}

const getFolders = async (userId: string, search:string, pagination:IPagination, sort:any, filter: any):Promise<IFolder[]> => {
  const result = await Folder.find({user: userId, status: 1, ...filter, $or: [{name: {$regex: search, $options: 'i'}}]}).skip(pagination.offset).limit(pagination.limit).sort(sort)
  return result
}
const getFolderCount =  async (userId: string, search:string, filter: any):Promise<number> => {
  const result =  await Folder.countDocuments({user: userId, status: 1, ...filter, $or: [{name: {$regex: search, $options: 'i'}}]})
  return result
}

const getFoldersWithPagination = async (userId: string, pagination:IPagination, sort: any):Promise<IFolder[]> => {
  const result = await Folder.find({user: userId}).skip(pagination.offset).limit(pagination.limit).sort(sort)
  return result
}
const getFoldersCount = async (userId: string):Promise<number> => {
  const result = await Folder.countDocuments({user: userId})
  return result
}

const getFoldersWithFilter = async (filter: any):Promise<IFolder[]> => {
  const result = await Folder.find({status: 1, ...filter}).sort({name: 1})
  return result
}

const getFolder = async (filter: any):Promise<IFolder> => {
  const result = await Folder.findOne({status: 1, ...filter})
  return result
}

// const dsgsfg = async (filter: any):Promise<any> => {
//   const result = await Folder.deleteMany({...filter})
//   return "success"
// }

const getFolderForMiddleware = async (filter: any):Promise<IFolder> => {
  const result = await Folder.findOne({...filter})
  return result
}

const addItemsToFolder = async (id: Types.ObjectId, items: IAddItems):Promise<IFolder> => {
  const result = await Folder.findByIdAndUpdate(id, {$push: {"items.artwork": {$each: items.artwork}, "items.koha": {$each: items.koha}}}, {new: true})
  return result
}

const addItem = async (id: Types.ObjectId, item: string, type: string):Promise<IFolder> => {
  let result
  if(type === "artwork"){
    result =await Folder.findByIdAndUpdate(id, {$push: {"items.artwork": item}}, {new: true})
  }
  if(type === "koha"){
    result =await Folder.findByIdAndUpdate(id, {$push: {"items.koha": item}}, {new: true})
  }
  if(type === "heritage"){
    result =await Folder.findByIdAndUpdate(id, {$push: {"items.heritage": item}}, {new: true})
  }
  if(type === "archive"){
    result =await Folder.findByIdAndUpdate(id, {$push: {"items.archive": item}}, {new: true})
  }
  if(type === "library"){
    result =await Folder.findByIdAndUpdate(id, {$push: {"items.library": item}}, {new: true})
  }
  if(type === "location"){
    result =await Folder.findByIdAndUpdate(id, {$push: {"items.location": item}}, {new: true})
  }
  return result
}

const removeItem = async (id: Types.ObjectId, item: string, type: any):Promise<IFolder> => {
  let result
  if(type === "artwork"){
    result =await Folder.findByIdAndUpdate(id, {$pull: {"items.artwork": item}}, {new: true})
  }
  if(type === "koha"){
    result =await Folder.findByIdAndUpdate(id, {$pull: {"items.koha": item}}, {new: true})
  }
  if(type === "heritage"){
    result =await Folder.findByIdAndUpdate(id, {$pull: {"items.heritage": item}}, {new: true})
  }
  if(type === "archive"){
    result =await Folder.findByIdAndUpdate(id, {$pull: {"items.archive": item}}, {new: true})
  }
  if(type === "library"){
    result =await Folder.findByIdAndUpdate(id, {$pull: {"items.library": item}}, {new: true})
  }
  if(type === "location"){
    result =await Folder.findByIdAndUpdate(id, {$pull: {"items.location": item}}, {new: true})
  }
  return result
}

const FolderService = {
  createFolder,
  updateFolder,
  getFolders,
  getFolder,
  addItemsToFolder,
  addItem,
  getFolderCount,
  deleteFolder,
  getFolderForMiddleware,
  getFoldersWithFilter,
  getFoldersWithPagination,
  getFoldersCount,
  removeItem
  // dsgsfg

}

export default FolderService