import {
    INotification,
    INotificationCreate,
    INotificationUpdate
} from '@root/interfaces'
import { Notification } from '@root/models'
import { Types } from 'mongoose'

const create = async (data: INotificationCreate): Promise<INotification> => {
    const result = await Notification.create(data)
    return result
}

const findById = async (id: Types.ObjectId): Promise<INotification> => {
    const result = await Notification.findById(id)
    return result
}
const updateNotification = async (
    id: Types.ObjectId,
    data: INotificationUpdate
): Promise<INotification> => {
    const result = await Notification.findByIdAndUpdate(id, data, {
        new: true
    })
    return result
}
const NotificationService = {
    create,
    findById,
    findByIdAndUpdate: updateNotification
}

export default NotificationService
