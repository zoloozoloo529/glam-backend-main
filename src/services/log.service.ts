import { ICreateSearchLog, IPagination, ISearchLog, IUserLog } from '@root/interfaces'
import { ISearchLogCount } from '@root/interfaces/ISearchLogCount'
import { SearchLog, SearchLogCount, UserLog } from '@root/models'
import { Types } from 'mongoose'


const pushSearchLog = async (data:ICreateSearchLog):Promise<ISearchLog> => {
  const result = await SearchLog.create(data)
  return result
} 

const getSearchLogs = async (pagination: IPagination): Promise<ISearchLog[]> => {
  const result = await SearchLog.find().skip(pagination.offset).limit(pagination.limit).sort({_id: -1})
  return result
}
const getSearchLogsWithFilter = async (filter: any, pagination: IPagination): Promise<ISearchLog[]> => {
  const result = await SearchLog.find({...filter}).skip(pagination.offset).limit(pagination.limit).sort({_id: -1})
  return result
}
const getSearchLogsCountDocument = async (filter: any): Promise<number> => {
  const result = await SearchLog.countDocuments({...filter})
  return result
}
const getSearchLogCount = async ():Promise<number> => {
  const result = await SearchLog.countDocuments()
  return result
}
const updateSearchLog = async (id: Types.ObjectId, data: any): Promise<ISearchLog> => {
  const result = await SearchLog.findByIdAndUpdate(id, data, {
    new: true
  })
  return result
}
const deleteUserLogs = async (userId: string): Promise<string> => {
  await SearchLog.updateMany({ user: userId }, { $set: { status: 0}}, {
    new: true
  })
  return "success"
}
const getSearchDocument = async (filter: any): Promise<number> => {
  const result = await SearchLog.countDocuments({ ...filter})
  return result
}




//search Log Counts sevrices

const addSearchLogCount = async (value: string):Promise<ISearchLogCount> => {
  const existLogCount = await SearchLogCount.findOne({value})
  if(!existLogCount) {
    return await SearchLogCount.create({value})
  }
  existLogCount.count++
  await existLogCount.save()
  return existLogCount
}

const getSearchLogCounts = async (pagination: IPagination):Promise<ISearchLogCount[]> => {
  const result = await SearchLogCount.find().skip(pagination.offset).limit(pagination.limit).sort({count: -1})
  return result
}

const getTotalSearchLogCount = async ():Promise<number> => {
  const result = await SearchLogCount.countDocuments()
  return result
}

const getUserLogs = async (filter:any, pagination: IPagination, sort: any): Promise<IUserLog[]> => {
  const result = await UserLog.find(filter).skip(pagination.offset).limit(pagination.limit).sort(sort)
  return result
}

const LogService = {
  pushSearchLog,
  getSearchLogs,
  getSearchLogCount,
  getSearchLogCounts,
  updateSearchLog,
  deleteUserLogs,
  addSearchLogCount,
  getTotalSearchLogCount,
  getSearchLogsWithFilter,
  getSearchLogsCountDocument,
  getSearchDocument,
  getUserLogs
}

export default LogService