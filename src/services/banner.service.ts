import { IBanner, IBannerCreate } from '@root/interfaces'
import { Banner } from '@root/models'
import { Types } from 'mongoose'

const create = async (data: IBannerCreate): Promise<IBanner> => {
    const result = await Banner.create(data)
    return result
}
const updateBanner = async (id: string, data: IBanner): Promise<IBanner> => {
    const result = await Banner.findByIdAndUpdate(id, data, {
        new: true
    })
    return result
}
const deleteBanner = async (id: Types.ObjectId): Promise<IBanner> => {
    const result = await Banner.findByIdAndUpdate(
        id,
        { $set: { status: 0 } },
        {
            new: true
        }
    )
    return result
}

const getBanners = async (): Promise<IBanner[]> => {
    const result = await Banner.find({ status: 1 }).sort({ order: 1 })
    return result
}

const getBannersPublic = async (): Promise<IBanner[]> => {
    const result = await Banner.find({ status: 1,hidden:false }).sort({ order: 1 })
    return result
}

const BannerService = {
    create,
    updateBanner,
    deleteBanner,
    getBanners,
    getBannersPublic
}

export default BannerService
