import config from "@root/config"
import FetchService from "../fetch.service"
import { IPagination } from "@root/interfaces"

const getBiblios = async (filter: any, sort?: string, order?: string):Promise<any> => {
  const search = filter.search as string || ''
  sort = sort || "timestamp"
  order = order || "desc"
  console.log('search: ', search)
  const query =`
  {
    paginate(page:${filter.page}, size:${filter.limit}, filters: [
      {
        column:"title", condition:contains, value: "${search}"
      }
    ], sorts: [{
      column: "${sort}", order: ${order}}]){
      total
      biblio{
        biblionumber
        subtitle
        title
        author
        timestamp
      }
    }
  }
  `
  console.log('string: ', JSON.stringify(query))
  const result = await FetchService.post(config.koha.base_url,  {query: query})
  console.log("🚀 ~ file: biblio.service.ts:31 ~ getBiblios ~ result:", result)
  const biblio: { _id: any; name: any; type: string, itemInf: { artist: { name: any } }; createdAt: any }[] = []
  result.data.paginate.biblio.map((el: any) => {
    biblio.push({
      _id: `${el.biblionumber}`,
      name: el.title,
      itemInf: {
        artist: {
          name: el.author
        }
      },
      type: 'koha',
      createdAt: el.timestamp
    })
  })
  return {biblio: biblio, total: result.data.paginate.total}
}

const getBiblioWithIds = async (ids: string[], pagination: IPagination):Promise<any> => {
  if(ids.length == 0){
    return { biblio: [], total: 0 }
  }
  let filters = '['

  for(let i = 0; i < ids.length; i++) {
    filters += `{
      column: "biblionumber",
      condition: equals,
      value: "${ids[i]}"
    }`
  }
  filters += ']'

  const query = `
  {
    paginate(page: ${pagination.page}, size: ${pagination.limit}, groupFilters: {
      combine: or
      filters: ${filters}
    })
    {
      total
      biblio{
        biblionumber
        subtitle
        title
        author
      }
    }
  }
  `
  const result = await FetchService.post(config.koha.base_url, { query })
  const biblio: { _id: any; name: any; type: string, itemInf: { artist: { name: any } }; createdAt: any }[] = []
  result.data.paginate.biblio.map((el: any) => {
    biblio.push({
      _id: `${el.biblionumber}`,
      name: el.title,
      itemInf: {
        artist: {
          name: el.author
        }
      },
      type: 'koha',
      createdAt: el.timestamp
    })
  })
  return {biblio: biblio, total: result.data.paginate.total}
}


const KOHABiblioService = {
  getBiblios,
  getBiblioWithIds
}

export default KOHABiblioService