import { ICreateResearch, IResearch } from '@root/interfaces';
import { Research } from '@root/models';
import { Types } from 'mongoose'

const create = async (data: ICreateResearch): Promise<IResearch> => {
    const createdResearch = await Research.create(data)
    return createdResearch
}

const findById = async (id: string): Promise<IResearch> => {
    const result = await Research.findById(id)
    return result

}
const find = async (filter: any): Promise<IResearch[]> => {
    const result = await Research.find({ status: 1, ...filter })
    return result
}

const update = async (id: Types.ObjectId, data: ICreateResearch): Promise<IResearch> => {
    const result = await Research.findByIdAndUpdate(id, data, {
        new: true
    })
    return result
}

const deleteResearch = async (id: Types.ObjectId): Promise<IResearch> => {
    const result = await Research.findByIdAndUpdate(id, { status: 0 }, {
        new: true
    })
    return result
}


const ResearchService = {
    create,
    find,
    findById,
    update,
    delete: deleteResearch
}
export default ResearchService