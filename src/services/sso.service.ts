import config from '@root/config'
import FetchService from './fetch.service'

const getUsers = async (filter: any): Promise<any> => {
    filter.organization = config.sso.organization
    filter.project = config.sso.project
    console.log('filter: ', filter)
    const keys = Object.keys(filter)
    let qs = ''
    for(let i = 0; i < keys.length; i++) {
        qs = qs + `${keys[i]}=${filter[keys[i]]}`
        if(i !== keys.length - 1) {
            qs =qs + '&'
        }
    }

    console.log(`${config.sso.base_url}/thirdparty/users?${qs}`)
    const result = await FetchService.get(
        `${config.sso.base_url}/thirdparty/users?${qs}`
    )

    return result
}

const createUser = async (data: any): Promise<any> => {
    data.organization = config.sso.organization
    data.project = config.sso.project
    console.log('data', data)

    const result = await FetchService.post(
        `${config.sso.base_url}/thirdparty/users`,
        data
    )

    return result
}

const updateUser = async (id: string, data: any):Promise<any> => {
    data.organization = config.sso.organization
    data.project = config.sso.project
    const result = await FetchService.patch(`${config.sso.base_url}/admin/organizations/${data.organization}/projects/${data.project}/users/${id}`, data)
    return result
}

const forgotPassword = async (email: string): Promise<any> => {
    const data:any = {}
    data.username = email
    data.organization = config.sso.organization
    data.project = config.sso.project
    const result = await FetchService.patch(
        `${config.sso.base_url}/thirdparty/users`,
        data
    )
    return result
}
const getUserById = async (id: string): Promise<any> => {
    const result = await FetchService.get(`${config.sso.base_url}/thirdparty/users/${id}`)
    return result
}

const SSOService = { getUsers, createUser, forgotPassword, updateUser, getUserById }

export default SSOService
