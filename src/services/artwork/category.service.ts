import config from '@root/config'
import FetchService from '../fetch.service'

const getCategories = async (parent?: string):Promise<any> =>  {
  const options = {
    authorization: config.artwork.api_key
  }
  let result
  if(!parent) {
    result = await FetchService.get(`${config.artwork.base_url}/categories`, options)
  }
  else {
    result = await FetchService.get(`${config.artwork.base_url}/categories?parent=${parent}`, options)
  }
  return result
}

const ArtworkCategoryService = {
  getCategories
}

export default ArtworkCategoryService