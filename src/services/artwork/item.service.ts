import config from "@root/config"
import FetchService from "../fetch.service"

const getItems = async ( filter: any ): Promise<any> => {
  const keys = Object.keys(filter)
  let qs = ''
  for(let i = 0; i < keys.length; i++) {
      qs = qs + `${keys[i]}=${filter[keys[i]]}`
      if(i !== keys.length - 1) {
          qs =qs + '&'
      }
  }
  const options = {
    authorization: config.artwork.api_key
  }
  const result = await FetchService.get(`${config.artwork.base_url}/items/?${qs}`, options)
  for(let j = 0; j < result.data.length; j++) {
    for(let i = 0; i < result.data[j].fields.length; i++) {
      if(result.data[j].fields[i].type === 'text') {
        result.data[j].fields[i].value = result.data[j].fields[i].t_value
        continue;
      }
      if(result.data[j].fields[i].type === 'number') {
        result.data[j].fields[i].value = result.data[j].fields[i].n_value
        continue;
      }
      if(result.data[j].fields[i].type === 'date') {
        result.data[j].fields[i].value = result.data[j].fields[i].d_value
        continue;
      }
      if(result.data[j].fields[i].type === 'link') {
        result.data[j].fields[i].value = result.data[j].fields[i].l_value
        continue;
      }
      if(result.data[j].fields[i].type === 'selection') {
        result.data[j].fields[i].value = result.data[j].fields[i].s_value
        continue;
      }
      if(result.data[j].fields[i].type === 'image') {
        result.data[j].fields[i].value = result.data[j].fields[i].i_value
        continue;
      }
      if(result.data[j].fields[i].type === 'location') {
        result.data[j].fields[i].value = result.data[j].fields[i].m_value
        continue;
      }
    }
    result.data[j].type = 'artwork'
  }

  return {data:result.data, pagination:result.pagination, status:result.status, type: "artwork"}
}

const getEduItems = async ( filter: any ): Promise<any> => {
  const keys = Object.keys(filter)
  let qs = ''
  for(let i = 0; i < keys.length; i++) {
      qs = qs + `${keys[i]}=${filter[keys[i]]}`
      if(i !== keys.length - 1) {
          qs =qs + '&'
      }
  }
  const options = {
    authorization: config.artwork.api_key
  }
  const result = await FetchService.get(`${config.artwork.base_url}/items/edu/?${qs}`, options)
  for(let j = 0; j < result.data.length; j++) {
    for(let i = 0; i < result.data[j].fields.length; i++) {
      if(result.data[j].fields[i].type === 'text') {
        result.data[j].fields[i].value = result.data[j].fields[i].t_value
        continue;
      }
      if(result.data[j].fields[i].type === 'number') {
        result.data[j].fields[i].value = result.data[j].fields[i].n_value
        continue;
      }
      if(result.data[j].fields[i].type === 'date') {
        result.data[j].fields[i].value = result.data[j].fields[i].d_value
        continue;
      }
      if(result.data[j].fields[i].type === 'link') {
        result.data[j].fields[i].value = result.data[j].fields[i].l_value
        continue;
      }
      if(result.data[j].fields[i].type === 'selection') {
        result.data[j].fields[i].value = result.data[j].fields[i].s_value
        continue;
      }
      if(result.data[j].fields[i].type === 'image') {
        result.data[j].fields[i].value = result.data[j].fields[i].i_value
        continue;
      }
      if(result.data[j].fields[i].type === 'location') {
        result.data[j].fields[i].value = result.data[j].fields[i].m_value
        continue;
      }
    }
    result.data[j].type = 'education'
  }
  // return result
  return {data:result.data, pagination:result.pagination, status:result.status, type: "education"}
}

const getItemsByIds = async (items: string[], filter: any):Promise<any> => {
  const options = {
    authorization: config.artwork.api_key
  }
  const keys = Object.keys(filter)
  let qs = ''
  for(let i = 0; i < keys.length; i++) {
      qs = qs + `${keys[i]}=${filter[keys[i]]}`
      if(i !== keys.length - 1) {
          qs =qs + '&'
      }
  }
  const result = await FetchService.patch(`${config.artwork.base_url}/items?${qs}`, {items}, options)
  for(let j = 0; j < result.data.length; j++) {
    for(let i = 0; i < result.data[j].fields.length; i++) {
      if(result.data[j].fields[i].type === 'text') {
        result.data[j].fields[i].value = result.data[j].fields[i].t_value
      }
      if(result.data[j].fields[i].type === 'number') {
        result.data[j].fields[i].value = result.data[j].fields[i].n_value
      }
      if(result.data[j].fields[i].type === 'date') {
        result.data[j].fields[i].value = result.data[j].fields[i].d_value
      }
      if(result.data[j].fields[i].type === 'link') {
        result.data[j].fields[i].value = result.data[j].fields[i].l_value
      }
      if(result.data[j].fields[i].type === 'selection') {
        result.data[j].fields[i].value = result.data[j].fields[i].s_value
      }
      if(result.data[j].fields[i].type === 'image') {
        result.data[j].fields[i].value = result.data[j].fields[i].i_value
      }
      if(result.data[j].fields[i].type === 'location') {
        result.data[j].fields[i].value = result.data[j].fields[i].m_value
      }
    }
    result.data[j].type = 'artwork'
  }
  return {data:result.data, pagination:result.pagination, type: "artwork", status:result.status}
}

const getItem = async (id: string):Promise<any> => {
  const options = {
    authorization: config.artwork.api_key
  }
  const result = await FetchService.get(`${config.artwork.base_url}/items/${id}`, options)

  const links = [] as any[]
  let link 
  const linkIndexs = []
  for(let i = 0; i < result.data.fields.length; i++) {
    if(result.data.fields[i].type === 'text') {
      result.data.fields[i].value = result.data.fields[i].t_value
    }
    if(result.data.fields[i].type === 'number') {
      result.data.fields[i].value = result.data.fields[i].n_value
    }
    if(result.data.fields[i].type === 'date') {
      result.data.fields[i].value = result.data.fields[i].d_value
    }
    if(result.data.fields[i].type === 'link') {
      result.data.fields[i].value = result.data.fields[i].l_value
      linkIndexs.push(i)
    }
    if(result.data.fields[i].type === 'selection') {
      result.data.fields[i].value = result.data.fields[i].s_value
    }
    if(result.data.fields[i].type === 'image') {
      result.data.fields[i].value = result.data.fields[i].i_value
    }
    if(result.data.fields[i].type === 'location') {
      result.data.fields[i].value = result.data.fields[i].m_value
    }
  }
  for (let i = 0, j = 0; i<linkIndexs.length; i++, j++) {
    link  = result.data.fields.splice(linkIndexs[i]-j, 1)
    links.push(link[0])
  }
  result.data.type = 'artwork'
  result.data.links = links

  return result.data
}

const getRawItem = async (id: string):Promise<any> => {
  const options = {
    authorization: config.artwork.api_key
  }
  const result = await FetchService.get(`${config.artwork.base_url}/items/${id}`, options)
  return result.data
}

const ArtworkItemService = {
  getItems,
  getItemsByIds,
  getItem,
  getRawItem,
  getEduItems
}

export default ArtworkItemService