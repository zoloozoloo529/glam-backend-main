import config from '@root/config'
import FetchService from '../fetch.service'

const getTags = async (filter: any):Promise<any> => {
  const options = {
    authorization: config.artwork.api_key
  }
  const keys = Object.keys(filter)
  let qs = ''
  for(let i = 0; i < keys.length; i++) {
      qs = qs + `${keys[i]}=${filter[keys[i]]}`
      if(i !== keys.length - 1) {
          qs =qs + '&'
      }
  }
  const result = await FetchService.get(`${config.artwork.base_url}/tags?${qs}`, options)
  return result
}

const ArtworkTagService = {
  getTags
}

export default ArtworkTagService