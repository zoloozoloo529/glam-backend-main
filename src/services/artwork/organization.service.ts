import config from '@root/config'
import FetchService from '../fetch.service'

const getOrganizations = async (filter: any):Promise<any> =>  {
  const options = {
    authorization: config.artwork.api_key
  }
  let result
  result = await FetchService.get(`${config.artwork.base_url}/organizations`, options)
  return result
}

const ArtworkCategoryService = {
    getOrganizations
}

export default ArtworkCategoryService