import fetch from 'node-fetch'
import logger from '@root/loaders/logger'
import { AbortSignal } from 'abort-controller'

async function get(
    url: string,
    options?: {
        authorization: any
        signal?: AbortSignal
    }
): Promise<any> {
    const response = await fetch(url, {
        headers: {
            Authorization: options ? options.authorization || '' : ''
        }
    })

    const text = await response.text()

    try {
        console.log('parse: ', JSON.parse(text))
        return JSON.parse(text)
    } catch (e) {
        logger.error('Response not json', text)
        return text
    }
}

async function post(
    url: string,
    body: any,
    options?: {
        authorization: any
    }
): Promise<any> {
    const headers: any = {
        Authorization: options ? options.authorization || '' : '',
    };
    let requestBody: any;

    if (typeof body.getHeaders === 'function') {
        requestBody = body;
    } else {
        headers['Content-Type'] = 'application/json';
        requestBody = JSON.stringify(body);
    }

    const response = await fetch(url, {
        method: 'POST',
        headers,
        body: requestBody
    })

    const text = await response.text()
    logger.info(`REQUEST URL: ${url}`, 'FETCH-SERVICE-POST')
    logger.info(`REQUEST BODY: ${JSON.stringify(body)}`, 'FETCH-SERVICE-POST')
    logger.info(`RESPONSE STATUS ${response.status}`, 'FETCH-SERVICE-POST')
    logger.info(`RESPONSE BODY ${text}`, 'FETCH-SERVICE-POST')

    try {
        return JSON.parse(text)
    } catch (e) {
        logger.error('Response not json', text)
        return text
    }
}

const patch = async (
    url: string,
    body: any,
    options?: {
        authorization: any
    }
): Promise<any> => {
    const response = await fetch(url, {
        method: 'PATCH',
        headers: {
            Authorization: options ? options.authorization || '' : '',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    })
    console.log(`response: ${JSON.stringify(response)}`)
    const text = await response.text()
    logger.info(`REQUEST URL: ${url}`, 'FETCH-SERVICE-POST')
    logger.info(`REQUEST BODY: ${JSON.stringify(body)}`, 'FETCH-SERVICE-POST')
    logger.info(`RESPONSE STATUS ${response.status}`, 'FETCH-SERVICE-POST')
    logger.info(`RESPONSE BODY ${text}`, 'FETCH-SERVICE-POST')

    try {
        return JSON.parse(text)
    } catch (e) {
        logger.error('Response not json', text)
        return text
    }
}

const FetchService = {
    get,
    post,
    patch
}

export default FetchService
