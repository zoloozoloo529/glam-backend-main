export { default as SSOService } from './sso.service'
export { default as EmailService } from './mail.service'
export { default as FetchService } from './fetch.service'
export { default as LogService } from './log.service'
export { default as FolderService } from './folder.service'
export { default as ViewService } from './view.service'
export { default as UserService } from './user.service'
export { default as LocationService } from './location.service'
export { default as ResearchService } from './research.service'
export { default as ResearchFolderService } from './researchFolder.service'
// Artwork

export { default as ArtworkCategoryService } from './artwork/category.service'
export { default as ArtworkItemService } from './artwork/item.service'
export { default as ArtworkTagService } from './artwork/tag.service'
export { default as OrganizationService } from './artwork/organization.service'

// Library
export { default as LibraryAuthService } from './library/auth.service'
export { default as LibraryBookService } from './library/book.service'
export { default as LibraryKohaService } from './library/koha/koha.service'
// Heritage

export { default as HeritageService } from './heritage/heritage.service'
export { default as ArchiveService } from './heritage/archive.service'

// KOHA

export { default as KOHABiblioService } from './koha/biblio.service'