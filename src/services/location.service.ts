import { ILocation, ILocationCreate, IPagination } from '@root/interfaces'
import { Location } from '@root/models'
import { Types } from 'mongoose'

const createLocation = async (data: ILocationCreate):Promise<ILocation> => {
  const result = await Location.create(data)
  return result
}

const updateLocation = async (id: Types.ObjectId,  data: any): Promise<ILocation> => {
  const result = await Location.findByIdAndUpdate(id, data, {
    new: true
  })
  return result
}

const deleteLocation = async (id: Types.ObjectId): Promise<ILocation> => {
  const result = await Location.findByIdAndUpdate(id, { status: 0 }, {
    new: true
  })
  return result
}

const getLocations = async (search:string, filter: any, pagination: IPagination, sort: any):Promise<ILocation[]> => {
  const result = await Location.find({status: 1, ...filter, name: {$regex: search, $options: 'i'}}).skip(pagination.offset).limit(pagination.limit).sort(sort)
  return result
}

const getLocationCount =  async (search:string, filter: any):Promise<number> => {
  const result =  await Location.countDocuments({status: 1, ...filter, $or: [{name: {$regex: search, $options: 'i'}}]})
  return result
}

const getLocation = async (filter: any):Promise<ILocation> => {
  const result = await Location.findOne({status: 1, ...filter})
  return result
}

const LocationService = {
  createLocation,
  updateLocation,
  getLocations,
  getLocation,
  getLocationCount,
  deleteLocation
}

export default LocationService