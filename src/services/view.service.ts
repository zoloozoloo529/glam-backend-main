import { IPagination, IView, IViewCreate } from '@root/interfaces'
import { View } from '@root/models'

const createViewCount = async (data: IViewCreate): Promise<IView> => {
  const result = await View.create(data)
  return result
}

const addViewCount = async (itemId: string): Promise<IView> => {
  const result = await View.findOneAndUpdate({itemId: itemId}, { $inc: { viewCount: 1 } }, { new: true })
  return result
} 

const addSaveCount = async (itemId: string): Promise<IView> => {
  const result = await View.findOneAndUpdate({ itemId: itemId }, { $inc: { saveCount: 1 } }, { new: true })
  return result
}

const getViewCount = async (itemId: string):Promise<IView> => {
 const result = await View.findOne({ itemId: itemId })
 return result 
}

const getAllViewCount = async(filter: any, pagination: IPagination, sort: any):Promise<IView[]> => {
  const result = await View.find(filter).skip(pagination.offset).limit(pagination.limit).sort(sort)
  return result
}

const ViewService = {
  createViewCount,
  addViewCount,
  getViewCount,
  addSaveCount,
  getAllViewCount
}

export default ViewService