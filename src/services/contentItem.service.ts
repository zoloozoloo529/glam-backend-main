import { IContentItem, IContentItemCreate } from '@root/interfaces/IContentItem'
import { ContentItem } from '@root/models'
import { Types } from 'mongoose'

const create = async (data: IContentItemCreate): Promise<IContentItem> => {
    const createdContentItem = await ContentItem.create(data)
    return createdContentItem
}

const updateContentItem = async (
    id: Types.ObjectId,
    data: Partial<IContentItem>
): Promise<IContentItem> => {
    const updatedContentItem = await ContentItem.findByIdAndUpdate(id, data, {
        new: true
    })
    return updatedContentItem
}

const deleteContentItem = async (id: Types.ObjectId): Promise<IContentItem> => {
    const deletedContentItem = await ContentItem.findByIdAndUpdate(
        id,
        { status: 0 },
        { new: true }
    )
    return deletedContentItem
}

const find = async (criteria: {
    hidden?: boolean
    status?: number
}): Promise<IContentItem[]> => {
    const contentItems = await ContentItem.find(criteria).sort({
        order: 1
    })
    return contentItems
}

const findOne = async (id: Types.ObjectId): Promise<IContentItem> => {
    const retrievedContentItem = await ContentItem.findOne({
        _id: id,
        status: 1
    })
    return retrievedContentItem
}

const ContentItemService = {
    create,
    find,
    findOne,
    update: updateContentItem,
    delete: deleteContentItem
}

export default ContentItemService
