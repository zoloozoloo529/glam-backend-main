import {
    IPagination,
    IUser,
    ICreateUser,
    IUpdateUser,
    IUserLog,
    IUserLogCreate,
} from "@root/interfaces";
import { User, UserLog } from "@root/models";
import { Types } from "mongoose";

const createUser = async (data: ICreateUser): Promise<IUser> => {
    const result = await User.create(data)
    return result
}

const updateUser = async (id: Types.ObjectId, data: IUpdateUser): Promise<IUser> => {
    const result = await User.findOneAndUpdate({ _id: id, status: 1 }, data, { new: true })
    return result
}

const getUsers = async (filter: any, search: string, pagination: IPagination, sort: any): Promise<IUser[]> => {
    const result = await User.find({ status: 1, ...filter, $or: [{ firstname: { $regex: search, $options: 'i' } }, { lastname: { $regex: search, $options: 'i' } }, { email: { $regex: search, $options: 'i' } }] }).skip(pagination.offset).limit(pagination.limit).sort(sort)
    return result
}

const getUsersWithFilter = async (filter: any): Promise<IUser[]> => {
    const result = await User.find({ status: 1, ...filter })
    return result
}

const getUser = async (filter: any): Promise<IUser> => {
    const result = await User.findOne({ status: 1, ...filter })
    return result
}

const getUsersCount = async (filter: any, search: string): Promise<number> => {
    const result = await User.countDocuments({ status: 1, ...filter, $or: [{ firstname: { $regex: search, $options: 'i' } }, { lastname: { $regex: search, $options: 'i' } }, { email: { $regex: search, $options: 'i' } }] })
    return result
}

const getUserById = async (id: Types.ObjectId): Promise<IUser> => {
    console.log("🚀 ~ file: user.service.ts ~ line 68 ~ getUserById ~ id", id);
    const result = await User.findOne({ _id: id, status: 1 })
    console.log("🚀 ~ file: user.service.ts ~ line 70 ~ getUserById ~ result", result);
    return result
}


//User Log services =>

const getViewItemCount = async (filter: any): Promise<number> => {
    const data = await UserLog.countDocuments({ ...filter })
    return data
}

const getUserLogs = async (filter: any, pagination: IPagination, sort: any): Promise<IUserLog[]> => {
    const result = await UserLog.find({ ...filter }).skip(pagination.offset).limit(pagination.limit).sort(sort)
    return result
}
const getUserLogsCount = async (filter: any): Promise<number> => {
    const result = await UserLog.countDocuments({ ...filter })
    return result
}
const getUserLogsByFilter = async (filter: any, sort: any, pagination: IPagination): Promise<IUserLog[]> => {
    const result = await UserLog.find({ ...filter }).skip(pagination.offset).limit(pagination.limit).sort({ _id: -1 })
    return result
}
const addLog = async (data: IUserLogCreate): Promise<IUserLog> =>
    UserLog.create(data)

const deleteUser = async (id: Types.ObjectId): Promise<any> => {
    const result = await User.findOneAndDelete({ _id: id})
    return result
}

const UserService = {
    createUser,
    updateUser,
    getUserById,
    getUsers,
    getUsersCount,
    getUsersWithFilter,
    getUser,
    getUserLogs,
    getUserLogsCount,
    getUserLogsByFilter,
    getViewItemCount,
    addLog,
    deleteUser
};

export default UserService;
