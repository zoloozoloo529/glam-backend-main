import { ICreateResearchFolder, IResearchFolder, IResearchFolderAddMember } from "@root/interfaces"
import { ResearchFolder } from "@root/models"
import { Types } from 'mongoose'


const create = async (data: ICreateResearchFolder): Promise<IResearchFolder> => {
    const createdResearch = await ResearchFolder.create(data)
    return createdResearch
}

const findByIdAndUpdate = async (id: Types.ObjectId, data: ICreateResearchFolder): Promise<IResearchFolder> => {
    const result = await ResearchFolder.findByIdAndUpdate(id, data, {
        new: true
    })
    return result
}
const findOne = async (filter: any): Promise<IResearchFolder | null> => {
    const result = await ResearchFolder.findOne({ status: 1, ...filter })
    return result

}
const find = async (filter: any): Promise<IResearchFolder[]> => {
    const result = await ResearchFolder.find({ status: 1, ...filter })
    return result

}
const findById = async (id: Types.ObjectId): Promise<IResearchFolder> => {
    const result = await ResearchFolder.findById(id)
    return result
}

const inviteUser = async (id: Types.ObjectId, data: IResearchFolderAddMember
): Promise<IResearchFolder> => {
    const result = await ResearchFolder.findByIdAndUpdate(id, data, {
        new: true
    })
    return result

}

const ResearchFolderService = {
    create,
    update: findByIdAndUpdate,
    find,
    findOne,
    findById,
    inviteUser

}

export default ResearchFolderService