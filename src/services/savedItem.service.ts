import { IPagination, ISavedItem, ISavedItemCreate } from "@root/interfaces"
import { SavedItem } from "@root/models"

const createSavedItem = async ( data: ISavedItemCreate): Promise<ISavedItem> => {
    const result = await SavedItem.create(data)
    return result
}

const getSavedItems = async (filter: any, search: string, pagination: IPagination, sort: any): Promise<ISavedItem[]> => {
    const result = await SavedItem.find({status: 1, ...filter, $or: [{item_name: {$regex: search, $options: 'i'}}]}).skip(pagination.offset).limit(pagination.limit).sort(sort)
    return result
}

const getSavedItemsCount =  async (search:string, filter: any):Promise<number> => {
    const result =  await SavedItem.countDocuments({status: 1, ...filter, $or: [{name: {$regex: search, $options: 'i'}}]})
    return result
  }

const checkExisting = async (filter:any): Promise<ISavedItem[]> => {
    const result = await SavedItem.find({status: 1, ...filter})
    return result
}

const getSavedItemById = async (id: any): Promise<ISavedItem> => {
    const result = await SavedItem.findById(id)
    return result
}

const deleteSavedItem = async (id: any): Promise<any> => {
    const result = await SavedItem.findByIdAndRemove(id)
    return result
}

const SavedItemService = {
    createSavedItem,
    getSavedItems,
    getSavedItemsCount,
    getSavedItemById,
    deleteSavedItem,
    checkExisting
}

export default SavedItemService