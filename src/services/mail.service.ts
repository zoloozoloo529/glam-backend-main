import * as nodemailer from 'nodemailer'
import config from '@root/config'
import { MailOptions } from 'nodemailer/lib/json-transport'

const sendMail = async (data: any): Promise<any> => {
  const transporter = nodemailer.createTransport({
    host: config.email.host,
    port: parseInt(config.email.port),
    // secure: false, // upgrade later with STARTTLS
    auth: {
        user: config.email.username,
        pass: config.email.password
    }
  })

  const message: MailOptions = {
      from: 'noreply@devorchin.com',
      to: data.to,
      subject: data.subject,
      text: data.text ?? '',
      html: data.html ?? data.text
  }
  let emailResponse
  try {
      emailResponse = await transporter.sendMail(message)
      console.log('emailResponse: ', emailResponse)
  } catch (e) {
      console.log(e)
  }

  return emailResponse
}

const EmailService = {
  sendMail
}

export default EmailService