import config from '@root/config'
import { IPagination } from '@root/interfaces'
import FetchService from '@root/services/fetch.service'

const getArchivesByIds = async (
    ids: string[],
    pagination: IPagination
): Promise<any> => {
    const data = {} as any
    data.page_no = pagination.page || 1
    data.per_page = pagination.limit || 6
    // data.sort = typeof filter.sort === 'undefined' ? "created_at desc" : `${filter.sort} desc`      heritage_name V name
    data.sort = 'created_at desc'
    data.filter = [
        {
            field_name: 'id',
            values: ids || [''],
            operation: 'in',
            field_type: 'uuid'
        }
    ]
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/media/list`,
        data,
        options
    )
    const convertedData: any[] = []
    if(result.data){
        result.data.map((el: any) => {
            convertedData.push({
                _id: el.id,
                name: el.heritage_name,
                mainImg: `${config.heritage.base_url}/static-files/w-2000${el.path}`,
                tags:
                    el.labels != null
                        ? [
                              {
                                  name: el.labels
                              }
                          ]
                        : [],
                type: 'archive'
            })
        })
    }
    return {
        data: convertedData,
        pagination: {
            limit: result.pagination.per_page,
            page: result.pagination.current_page_no,
            offset: pagination.offset,
            total: result.pagination.total_elements
        },
        status: result.response_code === 200 ? 'success' : 'error',
        type: 'archive'
    }
}

const getArchives = async (filter: any, advanced: any): Promise<any> => {
    const data = {} as any
    data.page_no = filter.page
    data.per_page = filter.limit
    data.sort = filter.sort_by === 'name' ? 'heritage_name' : 'created_at'
    data.sort += ` ${filter.sort_order}`
    data.filter = [
        {
            field_name: 'heritage_name',
            value: filter.search,
            operation: 'like',
            field_type: 'text'
        },
        {
            field_name: 'heritage_type',
            values: ['Баримтат өв'],
            operation: 'in',
            field_type: 'text'
        }
    ]
    if (advanced?.province) {
        data.filter.push({
            field_name: 'provinces',
            values: [advanced.province],
            operation: '&&',
            field_type: 'array'
        })
    }
    if (advanced?.region) {
        data.filter.push({
            field_name: 'regions',
            values: [advanced.region],
            operation: '&&',
            field_type: 'array'
        })
    }

    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/media/list`,
        data,
        options
    )
    const convertedData: any[] = []
    if(result.data){
        result.data.map((el: any) => {
            convertedData.push({
                _id: el.id,
                name: el.heritage_name,
                mainImg: `${config.heritage.base_url}/static-files/w-2000${el.path}`,
                type: 'archive'
            })
        })
    }
    return {
        data: convertedData,
        pagination: {
            limit: filter.limit,
            page: filter.page,
            offset: filter.offset,
            total: result.pagination.total_elements || 0
        },
        status:
            result.response_code === 200
                ? 'success'
                : result.response_code === 201
                ? 'success'
                : 'error',

        type: 'archive'
    }
}

const ArchiveService = {
    getArchivesByIds,
    getArchives
}

export default ArchiveService
