import config from '@root/config'
import { IHeritageFilter, IPagination, ISearchBody } from '@root/interfaces'
import FetchService from '@root/services/fetch.service'
import ViewService from '@root/services/view.service'

const getHeritages = async (
    filter: ISearchBody,
    advanced?: IHeritageFilter
): Promise<any> => {
    const data = {
        page_no: filter.page,
        per_page: filter.limit,
        sort: filter.sort_by + ' ' + filter.sort_order,
        filter: [
            {
                field_name: 'heritage_name',
                value: filter.search,
                operation: 'like',
                field_type: 'text'
            },
            {
                field_name: 'heritage_type',
                values: advanced?.heritage_type || [
                    'Хөдлөх өв',
                    'Биет бус өв',
                    'Үл хөдлөх өв',
                    'Баримтат өв'
                ],
                operation: 'in',
                field_type: 'text'
            }
        ]
    }
    if (advanced?.province) {
        data.filter.push({
            field_name: 'provinces',
            values: advanced.province,
            operation: '&&',
            field_type: 'array'
        })
    }
    if (advanced?.region) {
        data.filter.push({
            field_name: 'regions',
            values: advanced.region,
            operation: '&&',
            field_type: 'array'
        })
    }
    if (advanced?.keeper_name) {
        data.filter.push({
            field_name: 'keeper_name',
            value: advanced.keeper_name,
            operation: 'like',
            field_type: 'string'
        })
    }
    if (advanced?.heritage_century_id) {
        data.filter.push({
            field_name: 'heritage_century_id',
            value: advanced.heritage_century_id,
            operation: '=',
            field_type: 'uuid'
        })
    }
    if (advanced?.species_id) {
        data.filter.push({
            field_name: 'species_id',
            value: advanced.species_id,
            operation: '=',
            field_type: 'uuid'
        })
    }
    if (advanced?.species_name) {
        data.filter.push({
            field_name: 'species_name',
            value: advanced.species_name,
            operation: 'like',
            field_type: 'string'
        })
    }
    if (advanced?.date_range_a) {
        data.filter.push({
            field_name: 'created_at',
            value: advanced.date_range_a,
            operation: '>',
            field_type: 'date'
        })
    }
    if (advanced?.date_range_b) {
        data.filter.push({
            field_name: 'created_at',
            value: advanced.date_range_b,
            operation: '<',
            field_type: 'date'
        })
    }

    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/media/list`,
        data,
        options
    )
    const convertedData: any[] = []
    if (result.data) {
        result.data.map((el: any) => {
            convertedData.push({
                _id: el.id,
                name: el.heritage_name,
                heritage_type: el.heritage_type,
                keeper_name: el.keeper_name,
                heritage_century_id: el.heritage_century_id,
                heritage_century_name: el.heritage_century_name,
                species_id: el.species_id,
                species_name: el.species_name,
                created_at: el.created_at,
                mainImg: `${config.heritage.base_url}/static-files/w-400${el.path}`,
                type: 'heritage'
            })
        })
    }

    // for (let i = 0; i < filter.limit; i++){
    //     const heritageViewCount = await ViewService.getViewCount(convertedData[i])
    //     if(heritageViewCount) {
    //         convertedData[i].stats = heritageViewCount
    //     } else {
    //         await ViewService.createViewCount({itemId: convertedData[i], type:"heritage"})
    //         convertedData[i].stats = await ViewService.getViewCount(convertedData[i])
    //     }
    // }
    return {
        data: convertedData,
        pagination: {
            limit: filter.limit,
            page: filter.page,
            offset: filter.offset,
            total: result.pagination.total_elements || 0
        },
        status:
            result.response_code === 200
                ? 'success'
                : result.response_code === 201
                    ? 'success'
                    : 'error',

        type: 'heritage'
    }
}

const getHeritagesByIds = async (
    ids: string[],
    pagination: IPagination
): Promise<any> => {
    const data = {} as any
    data.page_no = pagination.page || 1
    data.per_page = pagination.limit || 6
    // data.sort = typeof filter.sort === 'undefined' ? "created_at desc" : `${filter.sort} desc`      heritage_name V name
    data.sort = 'created_at desc'
    data.filter = [
        {
            field_name: 'id',
            values: ids || [''],
            operation: 'in',
            field_type: 'uuid'
        }
    ]
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/media/list`,
        data,
        options
    )
    const convertedData: any[] = []
    if (result.data) {
        result.data.map((el: any) => {
            convertedData.push({
                _id: el.id,
                name: el.heritage_name,
                mainImg: `${config.heritage.base_url}/static-files/w-2000${el.path}`,
                tags:
                    el.labels != null
                        ? [
                            {
                                name: el.labels
                            }
                        ]
                        : [],
                type: 'heritage'
            })
        })
    }
    return {
        data: convertedData,
        pagination: {
            limit: result.pagination.per_page,
            page: result.pagination.current_page_no,
            offset: pagination.offset,
            total: result.pagination.total_elements
        },
        status: result.response_code === 200 ? 'success' : 'error',
        type: 'heritage'
    }
}

const getHeritageById = async (id: string, heritage_type: string): Promise<any> => {
    // const data = {} as any
    // data.page_no = 1
    // data.per_page = 1
    // data.sort = typeof filter.sort === 'undefined' ? "created_at desc" : `${filter.sort} desc`      heritage_name V name
    // data.sort = 'created_at desc'
    // data.filter = [
    //     {
    //         field_name: 'id',
    //         values: [id] || [''],
    //         operation: 'in',
    //         field_type: 'uuid'
    //     }
    // ]
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.get(
        `${config.heritage.base_url}/heritage/external/media/${heritage_type}/heritage/${id}`,
        options
    )
    let convertedData
    if (result.data) {
        let mainImg
        if (result.data.heritage_attachments[0]) {
            mainImg = `${config.heritage.base_url}/static-files/w-2000${result.data.heritage_attachments[0].path}`
        }
        convertedData = result.data
        convertedData.mainImg = mainImg
        // mainImg
        // convertedData = {

        // }
        // convertedData = {
        //     _id: result.data[0].id,
        //     name: result.data[0].heritage_name,
        //     mainImg: `${config.heritage.base_url}/static-files/w-2000${result.data[0].path}`,
        //     tags:
        //         result.data[0].labels != null
        //             ? [
        //                   {
        //                       name: result.data[0].labels
        //                   }
        //               ]
        //             : [],
        //     fields: [
        //         {
        //             name: 'Өвийн дугаар',
        //             type: 'text',
        //             value: result.data[0].heritage_number
        //         },
        //         {
        //             name: 'Өвийн төрөл',
        //             type: 'text',
        //             value: result.data[0].heritage_type
        //         },
        //         // {
        //         //     name: "Аймаг/хот/",
        //         //     type:"text",
        //         //     value: province?.data[0]?.province_name
        //         // },
        //         // {
        //         //     name: "Сум/дүүрэг/",
        //         //     type:"text",
        //         //     value: region?.data[0]?.region_name
        //         // },
        //         {
        //             name: 'Үүсгэсэн огноо',
        //             type: 'date',
        //             value: result.data[0].created_at
        //         }
        //     ],
        //     type: 'heritage'
        // }
        // const province = await FetchService.post(
        //     `${config.heritage.base_url}/heritage/external/province/list`,
        //     {
        //         filter: [
        //             {
        //                 field_name: 'id',
        //                 value: result.data[0].provinces,
        //                 operation: '=',
        //                 field_type: 'uuid'
        //             }
        //         ]
        //     },
        //     options
        // )
        // if (province.data) {
        //     convertedData.fields.push({
        //         name: 'Аймаг/хот/',
        //         type: 'text',
        //         value: province?.data[0]?.province_name
        //     })
        // }
        // const region = await FetchService.post(
        //     `${config.heritage.base_url}/heritage/external/region/${result.data[0].provinces}/list`,
        //     {
        //         filter: [
        //             {
        //                 field_name: 'id',
        //                 value: result.data[0].regions,
        //                 operation: '=',
        //                 field_type: 'uuid'
        //             }
        //         ]
        //     },
        //     options
        // )
        // if (province.data) {
        //     convertedData.fields.push({
        //         name: 'Сум/дүүрэг/',
        //         type: 'text',
        //         value: region?.data[0]?.region_name
        //     })
        // }
    }
    return {
        data: convertedData,
        status: result.response_code === 200 ? 'success' : 'error',
        // type: 'heritage'
    }
}

const getProvinces = async (id: string): Promise<any> => {
    //id:"" for all provinces
    const data = {} as any
    if (id !== '') {
        data.filter = [
            {
                field_name: 'id',
                value: id,
                operation: '=',
                field_type: 'uuid'
            }
        ]
    }

    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/province/list`,
        data,
        options
    )

    return result
}

const getRegions = async (province: string, id: string): Promise<any> => {
    // id:"" for all regions
    const data = {} as any
    if (id !== '') {
        data.filter = [
            {
                field_name: 'id',
                value: id,
                operation: '=',
                field_type: 'uuid'
            }
        ]
    }

    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/region/${province}/list`,
        data,
        options
    )

    return result
}

const getHeritageTypes = async (): Promise<any> => {
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/refs/list`,
        {
            "sort": "created_at asc",
        },
        options
    )
    return {
        data: result.data,
        status: result.response_code === 200 ? 'success' : 'error'
    }

}

const getHeritageSubTypes = async (id: string): Promise<any> => {
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/refs/${id}/list`,
        {
            sort: "created_at asc"
        },
        options
    )
    return result
}

const createHeritageRef = async (data: any): Promise<any> => {
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/reference/document/create`,
        data,
        options
    )
    if (result?.response_code === 200) {
        return result
    }
    throw Error(result?.response_msg || 'Error creating heritage reference document')
}

const getHeritagesRefs = async (data: any): Promise<any> => {
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/reference/document/list`,
        data,
        options
    )
    return result
}

const getHeritageRefById = async (id: string): Promise<any> => {
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.get(
        `${config.heritage.base_url}/heritage/external/reference/document/${id}`,
        options
    )
    return result
}

const getImmovableHeritages = async (): Promise<any> => {
    const data = {
        "page_no": 1,
        "per_page": 1000,
        "sort": "heritage_name asc",
        "filter": [
            {
                "field_name": "heritage_type",
                "value": "Үл хөдлөх өв",
                "operation": "=",
                "field_type": "text"
            }
        ]
    }

    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }

    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/media/list`,
        data,
        options
    )

    let convertedData
    if (result.data) {
        convertedData = result
        for (let i = 0; i < result.pagination.total_elements; i++) {
            const result = await FetchService.get(
                `${config.heritage.base_url}/heritage/external/media/immovable/heritage/${convertedData.data[i].id}`,
                options
            )
            convertedData.data[i] = {}
            convertedData.data[i] =
                Object.assign(convertedData.data[i],
                    {
                        "id": result.data.id,
                        "heritage_type": result.data.heritage_type,
                        "province": result.data.province_name,
                        "region": result.data.region_name,
                        "heritage_name": result.data.heritage_name,
                        "memorial_description": result.data.memorial_area_public_description,
                        "protection_priority": result.data.protection_priority_name,
                        "latitude": result.data.location_latitude,
                        "longitude": result.data.location_longitude,
                        "heritage_description": result.data.heritage_public_description,
                        "memorial_latitude": result.data.memorial_area_latitude,
                        "memorial_longitude": result.data.memorial_area_longitude,
                        "heritage_attachments": result.data.heritage_attachments
                    }
                )
            if (convertedData.data[i].heritage_attachments[0]) {
                Object.assign(convertedData.data[i], { "mainImg": `${config.heritage.base_url}/static-files/w-2000${convertedData.data[i].heritage_attachments[0].path}` })
            }
        }
    }
    convertedData = Object.assign(convertedData, { "total": result.pagination.total_elements })
    return convertedData
}

const getHeritageCenturies = async (): Promise<any> => {
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/refs/heritage/century/list`,
        {},
        options
    )
    return {
        data: result,
        status: result.response_code === 200 ? 'success' : 'error'
    }

}

const getHeritageMovableSpecies = async (): Promise<any> => {
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/refs/movable/species/list`,
        {},
        options
    )
    return {
        data: result,
        status: result.response_code === 200 ? 'success' : 'error'
    }
}

const getHeritageImmovableSpecies = async (): Promise<any> => {
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/refs/immovable/species/list`,
        {},
        options
    )
    return {
        data: result,
        status: result.response_code === 200 ? 'success' : 'error'
    }
}

const getHeritageIntangibleSpecies = async (): Promise<any> => {
    const options = {
        authorization: `Basic ${btoa(
            `${config.heritage.auth_username}:${config.heritage.auth_password}`
        )}`
    }
    const result = await FetchService.post(
        `${config.heritage.base_url}/heritage/external/refs/intangible/type/list`,
        {},
        options
    )
    return {
        data: result,
        status: result.response_code === 200 ? 'success' : 'error'
    }
}

const HeritageService = {
    getHeritages,
    getHeritagesByIds,
    getHeritageById,
    getHeritageTypes,
    getHeritageSubTypes,
    createHeritageRef,
    getHeritagesRefs,
    getHeritageRefById,
    getProvinces,
    getRegions,
    getImmovableHeritages,
    getHeritageCenturies,
    getHeritageImmovableSpecies,
    getHeritageMovableSpecies,
    getHeritageIntangibleSpecies
}

export default HeritageService
