import config from '@root/config'
import { ILibraryFilter, ISearchBody } from '@root/interfaces'
import FetchService from '../fetch.service'

// const getSpecialItems = async () => {

//   const options = {
//     authorization: config.digital_library.api_key
//   }

//   const result = await FetchService.get(`${config.digital_library.base_url}/special-items`, options)
//   console.log("result: ", result.data[0])
//   return result.data
// }

const searchBooks = async (
  filter: ISearchBody,
  advanced?: ILibraryFilter
): Promise<any> => {
  let data = advanced as any || {}
  const final = [] as any[]
  data.search_text = filter.search || ""
  data.page = filter.page
  data.take = filter.limit
  data.sortBy = filter.sort_by === "name" ? "book_name" : filter.sort_by === "date" ? "created_at" : "book_name"
  data.sortOrder = filter.sort_order || "asc"
  console.log("🚀 ~ file: book.service.ts:26 ~ data:", data)
  const result = await FetchService.post(`${config.digital_library.library_pro_base_url}/book/find_all`, data)
  if(result.data){
    result.data.map((el:any) => {
      final.push({
          _id: el.id.toString(),
          name: el.book_name,
          itemInf: {
              artist:{
                  name: `${el.authors[0]?.last_name[0]}`+`. `+`${el.authors[0]?.first_name}` || ""
              }
          },
          mainImg: `${config.digital_library.library_pro_base_url}/file/${el.url_image}`,
          type: "library"
      })
    })
  }
  const pagination = {
    limit:result.metaData.recordsPerPage,
    page:result.metaData.pageNumber,
    total:result.metaData.numberOfRecords
  } as any
  pagination.offset = pagination.limit * pagination.page
  return {data:final, pagination, status:result.success?"success":"error", type: "library" }
}

const getSpecialItems = async ():Promise<any> => {
  const result = await FetchService.get(`${config.digital_library.library_pro_base_url}/book/find_special_books`)
  return result
}

const getNewBooks = async ():Promise<any> => {
  const result = await FetchService.get(`${config.digital_library.library_pro_base_url}/book/find_new_books`)
  return result
}

const getSimilarBooks = async (id: number ):Promise<any> => {
  const result = await FetchService.post(`${config.digital_library.library_pro_base_url}/book/get_similar_books`, { id })
  return result
}

const getBook = async (id: string ):Promise<any> => {
  const result = await FetchService.get(`${config.digital_library.library_pro_base_url}/book/get_book_info_all/${id}`)
  return result
}

const getBooks = async (filter: any ):Promise<any> => {
  const result = await FetchService.post(`${config.digital_library.library_pro_base_url}/book/find_all`, filter)
  return result
}

const getCategories = async ():Promise<any> => {
  const result = await FetchService.get(`${config.digital_library.library_pro_base_url}/book/category_list`)
  return result
}

const getTypes = async ():Promise<any> => {
  const result = await FetchService.get(`${config.digital_library.library_pro_base_url}/book/type_list`)
  return result
}

const checkBiblioFromDL = async (id: string ):Promise<any> => {
  const result = await FetchService.get(`${config.digital_library.library_pro_base_url}/book/check_biblionumber/${id}`)
  return result
}

const LibraryBookService = {
  getSpecialItems,
  getNewBooks,
  getBook,
  getBooks,
  checkBiblioFromDL,
  getCategories,
  getSimilarBooks,
  getTypes,
  searchBooks
}

export default LibraryBookService