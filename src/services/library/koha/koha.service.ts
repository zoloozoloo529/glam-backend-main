import config from '@root/config'
import { IKohaFilter, IPagination } from '@root/interfaces'
import { KohaItem } from '@root/models'
import FetchService from '@root/services/fetch.service'


const getKohaBooks = async (filter: any, advanced: IKohaFilter):Promise<any> => {
    let data = advanced as any || {}
    data.book_name = filter.search || ""
    data.page = filter.page
    data.take = filter.limit
    // filter.sort_by === "name" ? data.sortBy = "book_name" : 
    data.sortBy = filter.sort_by === "name" ? "book_name" : filter.sort_by === "date" ? "press_year" : "book_name"
    data.sortOrder = filter.sort_order || "asc"
    // console.log("🚀🚀🚀🚀🚀🚀🚀🚀 ~ file: koha.service.ts:14 ~ getKohaBooks ~ data:", data)

    const result = await FetchService.post(`${config.digital_library.library_pro_base_url}/book/find_koha_all`, data)
    const convertedData: any[] = []
    if(result.data){
        result.data.map((el: any) => {
            convertedData.push({
            _id: `${el.biblionumber}`,
            name: el.book_name,
            mainImg: el.url_image,
            itemInf: {
                artist: {
                name: el.author_koha
                }
            },
            // fields: [
            //     {
            //         name: "isbn_code",
            //         value: el.isbn_code
            //     },
            //     {
            //         name: "press_year",
            //         value: el.press_year
            //     },
            //     {
            //         name: "publishercode",
            //         value: el.publishercode
            //     },
            //     {
            //         name: "pages",
            //         value: el.pages
            //     },
            //     {
            //         name: "press_org_koha",
            //         value: el.press_org_koha
            //     },
            //     {
            //         name: "koha_languages",
            //         value: el.koha_languages
            //     },
            // ],
            type: 'koha'
            })
        })
    }
    return {
        data:convertedData,
        pagination:{
            limit:filter.limit,
            page:filter.page,
            offset: filter.offset,
            total:result.metaData.numberOfRecords
        },
        status:result.success?"success":"error",
        type:"koha"
    }
}

const getKohaItemsWithBiblios = async (ids: string[], pagination: IPagination):Promise<any> => {
    let data = {} as any
    data.biblionumbers = ""
    for(let i = 0; i < ids.length; i++) {
        if(ids[i]){
            data.biblionumbers += ids[i]
        }
    }
    // data.press_year = filter.search || ""
    // data.isbn = filter.search || ""
    // data.author_name = filter.search || ""
    // data.publisher_name = filter.search || ""
    data.page = pagination.page || "1"
    data.take = pagination.limit || "10"
    data.sortBy = "biblionumber"
    data.sortOrder = "asc"

    const result = await FetchService.post(`${config.digital_library.library_pro_base_url}/book/find_koha_check_list`, data)
    const convertedData: any[] = []
    if(result.data){
        result.data.map((el: any) => {
            convertedData.push({
            _id: `${el.biblionumber}`,
            name: el.book_name,
            mainImg: el.url_image,
            itemInf: {
                artist: {
                name: el.author_koha
                }
            },
            fields: [
                {
                    name: "ISBN дугаар",
                    value: el.isbn_code
                },
                {
                    name: "Хэвлэгдсэн он",
                    value: el.press_year
                },
                {
                    name: "Хэвлэлийн газар",
                    value: el.publishercode
                },
                {
                    name: "Хуудас",
                    value: el.pages
                },
                // {
                //     name: "press_org_koha",
                //     value: el.press_org_koha
                // },
                {
                    name: "Хэл",
                    value: el.koha_languages
                },
            ],
            type: 'koha'
            })
        })
    }
    return {data:convertedData, pagination:{ limit:result.metaData.recordsPerPage, page:result.metaData.pageNumber, offset: pagination.offset, total:result.metaData.numberOfRecords}, status:result.success?"success":"error" }
}


// From cached data

const getKohaFromCache = async (search:string, pagination:IPagination, sort:any, rawfilter: any):Promise<any> => {
    const press_year_min = rawfilter?.press_year_min
    const press_year_max = rawfilter?.press_year_max
    const isbn = rawfilter?.isbn
    const author_name = rawfilter?.author_name
    const publisher_name = rawfilter?.publisher_name
    let filter = {} as any
    const result:any[] =[]
    if(press_year_min) {
        if(press_year_max) {
          filter = {
            'press_year': { $gt: press_year_min, $lt: press_year_max }
          }
        }
        else {
          filter = {
            'press_year': { $gt: press_year_min }
          }
        }
    }
    else {
        if(press_year_max) {
            filter = {
                'press_year': { $lt: press_year_max }
            }
        }
    }
    if(isbn) {
        filter.isbn_code = { $regex: isbn }
    }
    if(author_name) {
        filter.author_koha = { $regex: author_name }
    }
    if(publisher_name) {
        filter.publisher_name = { $regex: publisher_name }
    }
    const data = await KohaItem.find({status: 1, ...filter, $or: [{name: {$regex: search, $options: 'i'}}]}).skip(pagination.offset).limit(pagination.limit).sort(sort)
    const total =  await KohaItem.countDocuments({status: 1, ...filter, $or: [{name: {$regex: search, $options: 'i'}}]})
    data.map((el:any) => {
        result.push({
            _id: `${el.biblionumber}`,
            name: el.name,
            mainImg: el.mainImg,
            itemInf: el.itemInf,
            isbn: el.isbn_code,
            fields: el.fields,
            type: 'koha'
        })
    })
    return {
        data: result,
        pagination:{
            limit:filter.limit,
            page:filter.page,
            offset: filter.offset,
            total:total || NaN
        },
        status:"success",
        type:"koha"
    }
  }

const getKohaByBibliosFromCache = async (ids: string[]):Promise<any> => {
    let data = {} as any
    data.biblionumbers = ""
    const numberIds:number[] = ids.map(el => parseInt(el))
    const result = await KohaItem.find({biblionumber : { $in: numberIds }})
    const convertedData: any[] = []
    if(result){
        result.map((el: any) => {
            convertedData.push({
            _id: `${el.biblionumber}`,
            name: el.book_name,
            mainImg: el.url_image,
            itemInf: {
                artist: {
                name: el.author_koha
                }
            },
            type: 'koha'
            })
        })
    }
    return {data:convertedData, pagination: {limit:ids.length, total:convertedData.length}, status:"success", type: "koha" }
}

const getKohaByBiblioFromCache = async (id: string ):Promise<any> => {
    const numberId: number = parseInt(id)
    let convertedData
    const result = await KohaItem.findOne({biblionumber : numberId})
    if(result){
        convertedData = {
            _id: `${result.biblionumber}`,
            name: result.book_name,
            mainImg: result.url_image,
            itemInf: {
                artist: {
                name: result.author_koha
                }
            },
            type: 'koha'
        }
    }
    return convertedData
}

const LibraryKohaService = {
    getKohaBooks,
    getKohaItemsWithBiblios,
    getKohaFromCache,
    getKohaByBibliosFromCache,
    getKohaByBiblioFromCache
}

export default LibraryKohaService