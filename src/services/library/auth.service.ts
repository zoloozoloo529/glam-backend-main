import config from '@root/config'
import FetchService from '../fetch.service'

// const getSpecialItems = async () => {

//   const options = {
//     authorization: config.digital_library.api_key
//   }

//   const result = await FetchService.get(`${config.digital_library.base_url}/special-items`, options)
//   console.log("result: ", result.data[0])
//   return result.data
// }

const login = async (email: string, password: string ):Promise<any> => {
  const result = await FetchService.post(`${config.digital_library.library_pro_base_url}/auth/login`, {email, password})
  return result
}

const koha_book_take_history = async (token: string, search: string, page: string, take: string, sortBy: string, sortOrder: string,):Promise<any> => {
  const options = {
    authorization: `Bearer ${token}`
  }
  const result = await FetchService.get(`${config.digital_library.library_pro_base_url}/patron/koha_book_take_history?search=${search}&page=${page}&take=${take}&sortBy=${sortBy}&sortOrder=${sortOrder}&`, options)
  return result
}

const koha_book_take_list = async (token: string, search: string, page: string, take: string, sortBy: string, sortOrder: string,):Promise<any> => {
  const options = {
    authorization: `Bearer ${token}`
  }
  const result = await FetchService.get(`${config.digital_library.library_pro_base_url}/patron/koha_book_take_list?search=${search}&page=${page}&take=${take}&sortBy=${sortBy}&sortOrder=${sortOrder}&`, options)
  return result
}

const LibraryAuthService = {
  login,
  koha_book_take_history,
  koha_book_take_list
}

export default LibraryAuthService