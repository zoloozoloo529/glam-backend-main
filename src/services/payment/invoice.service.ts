import config from '@root/config'
import { IInvoice, IInvoiceCreate } from '@root/interfaces'
import { Invoice } from '@root/models'
import { Types } from 'mongoose'
import { v4 as uuid } from 'uuid'
import SteppePaymentService from './steppepayment.service'
const createInvoice = async (
    user: any,
    entries: Types.ObjectId[],
    amount: number
): Promise<IInvoice> => {
    console.log('user', user)
    const entryObjects = await Invoice.find({
        _id: {
            $in: entries
        },
        'isPaid': false
    })
    console.log(entryObjects)
    const billNumber = uuid()

    // const amount = entryObjects.reduce((acc: number, el) => {
    //     console.log(el.payment.amount)
    //     acc += el.payment.amount

    //     return acc
    // }, 0)

    console.log(user.claims.username, billNumber, amount)

    const paymentInvoice = await SteppePaymentService.createInvoice(
        user.claims.username,
        billNumber,
        config.steppepayment.isTestAmount == '1' ? 100 : amount,
        {
            failRedirectUrl: "https://entry.spok.mn/success",
            successRedirectUrl: "https://entry.spok.mn/success" 
        }
    )

    const data: IInvoiceCreate = {
        billNumber,
        user: user.claims.username,
        amount: amount,
        steppePayment: paymentInvoice.data
    }

    const invoice = await Invoice.create(data)

    return invoice
}

const getInvoice = async (filter: any): Promise<IInvoice> => {
    return Invoice.findOne(filter)
}

const getInvoices = async (filter: any): Promise<IInvoice[]> => {
    const invoices = await Invoice.find(filter)
    return invoices
}

const InvoiceService = {
    getInvoice,
    getInvoices,
    createInvoice
}

export default InvoiceService
