import config from '@root/config'
import FetchService from '../fetch.service'

const createInvoice = async (
    username: string,
    billNumber: string,
    amount: number,
    options: any,
    api_key = config.steppepayment.api_key
): Promise<any> => {
    const url = `${config.steppepayment.base_url}/v1/thirdparty/users/${username}/invoices`

    const body = {
        billNumber,
        amount,
        ...options
    }

    await createUser(username, api_key)

    const result = await FetchService.post(url, body, {
        authorization: `Token ${api_key}`
    })

    console.log('result', result)

    return result
}

const createUser = async (
    username: string,
    api_key = config.steppepayment.api_key
): Promise<any> => {
    const url = `${config.steppepayment.base_url}/v1/thirdparty/users`

    const body = {
        username
    }

    const result = await FetchService.post(url, body, {
        authorization: `Token ${api_key}`
    })

    return result.data
}

const getAccessToken = async (
    username: string,
    api_key = config.steppepayment.api_key
): Promise<any> => {
    const url = `${config.steppepayment.base_url}/v1/thirdparty/users/${username}/token`

    const result = await FetchService.get(url, {
        authorization: `Token ${api_key}`
    })

    return result.data
}

const SteppePaymentService = {
    createInvoice,
    createUser,
    getAccessToken
}

export default SteppePaymentService
