import { celebrate, Joi, Segments } from 'celebrate'


const searchBody = celebrate({
    [Segments.BODY]: Joi.object({
        search: Joi.string().optional().allow(''),
        cache: Joi.bool().optional(),
        type: Joi.string().optional().allow(''),
        limit: Joi.number().optional().min(1),
        page: Joi.number().optional().min(1),
        sort_by: Joi.string().optional(),
        sort_order: Joi.string().optional(),
        filter: Joi.any().optional()
    }).options({ stripUnknown: true })
})

export default {
    searchBody
}
