import { celebrate, Joi, Segments } from 'celebrate'

const register = celebrate({
    [Segments.BODY]: Joi.object({
        email: Joi.string().email({ tlds: false }).allow(''),
        password: Joi.string().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#,.-_=+()^])[A-Za-z\d@$!%*?&#,.-_=+()^]{8,}$/).required().messages({'string.pattern.base': 'Нууц үг хамгийн багадаа 8 тэмдэгт агуулсан байх, ядаж нэг том үсэг, ядаж нэг тоо, ядаж нэг тусгай тэмдэгт орсон байх ёстой(@$!%*?&#,.-_=+()^)' }),
        firstname: Joi.string().optional(),
        lastname: Joi.string().optional(),
        img: Joi.string().optional()
    }).options({ stripUnknown: true })
})

const resetPassword = celebrate({
    [Segments.BODY]: Joi.object({
        oldPassword: Joi.string().required(),
        newPassword: Joi.string().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#,.-_=+()^])[A-Za-z\d@$!%*?&#,.-_=+()^]{8,}$/).required().messages({'string.pattern.base': 'Нууц үг хамгийн багадаа 8 тэмдэгт агуулсан байх, ядаж нэг том үсэг, ядаж нэг тоо, ядаж нэг тусгай тэмдэгт орсон байх ёстой(@$!%*?&#,.-_=+()^)' }),
    }).options({ stripUnknown: true })
})

export default {
    register,
    resetPassword
}
