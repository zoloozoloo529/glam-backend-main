import logger from '@root/loaders/logger'

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const config = {
    env: process.env.NODE_ENV,

    host: process.env.NODE_HOST,

    frontendHost: process.env.FRONTEND_HOST,
    /**
     * Server port
     */
    port: parseInt(process.env.NODE_PORT, 10) || 3000,

    /**
     * Maximum time for the controller to execute
     */
    apiMaxTimeoutMS: parseInt(process.env.API_MAX_TIMEOUT_MS, 10) || 100000,

    /**
     * Winston log level
     */
    logs: {
        morgan: ':remote-addr - :remote-user [:date[clf]] ":method :url :auth_token HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"'
    },

    /**
     * API configs
     */
    api: {
        prefix: '/v1'
    },

    /**
     * CORS allowed origins by array
     */
    corsAllowedOrigins: process.env.CORS_ALLOWED_ORIGINS.split(',') || [
        process.env.NODE_HOST
    ],

    mongoDB: {
        primaryURI:
            process.env.NODE_ENV === 'development'
                ? process.env.MONGODB_DEV_URI
                : process.env.MONGODB_PROD_URI
    },

    crypto: {
        hash_iv: process.env.CRYPTO_HASH_IV,
        hash_key: process.env.CRYPTO_HASH_KEY
    },

    jwt: {
        secret: process.env.JWT_SECRET
    },

    aws: {
        credentials: {
            accessKeyId: process.env.AWS_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
        },
        s3: {
            base_url: process.env.S3_BASE_URL
        }
    },
    email: {
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        username: process.env.EMAIL_USERNAME,
        password: process.env.EMAIL_PASSWORD
    },

    sso: {
        base_url: process.env.SSO_BASE_URL,
        organization: process.env.SSO_ORGANIZATION,
        project: process.env.SSO_PROJECT,
        client: process.env.SSO_CLIENT
    },

    dan: {
        base_url: process.env.DAN_BASE_URL,
        client_id: process.env.DAN_CLIENT_ID,
        client_secret: process.env.DAN_CLIENT_SECRET,
        redirect_url: process.env.DAN_REDIRECT_URL,
        xyp_access_token: process.env.DAN_XYP_ACCESS_TOKEN
    },

    artwork: {
        base_url: process.env.ARTWORK_BASE_URL,
        api_key: process.env.ARTWORK_API_KEY
    },

    digital_library: {
        // TEST server
        // library_base_url: process.env.LIBRARY_BASEURL,  
        library_pro_base_url: process.env.LIBRARY_BASE_URL
    },

    heritage: {
        base_url: process.env.HERITAGE_BASE_URL,
        auth_username: process.env.AUTH_USERNAME,
        auth_password: process.env.AUTH_PASSWORD
    },

    koha: {
        base_url: process.env.KOHA_BASE_URL
    },

    steppepayment: {
        isTestAmount: process.env.IS_PAYMENT_TEST_AMOUNT,
        base_url: process.env.STEPPEPAYMENT_BASE_URL,
        api_key: process.env.STEPPEPAYMENT_API_KEY
    },

    imageStatic: process.env.IMAGE_STATIC_URL
}

export function configureEnvironment(): void {
    if (!config.mongoDB.primaryURI) {
        logger.error(
            'No mongo connection string. Set MONGODB_DEV_URI/MONGODB_PROD_URI environment variable.'
        )
        process.exit(1)
    }
}

export default config
