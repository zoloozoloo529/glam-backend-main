import { LogService } from '@root/services'
import { Request, Response, NextFunction } from 'express'
import { ICreateSearchLog } from '@root/interfaces'

async function saveSearchLog(req: Request, res: Response, next: NextFunction):Promise<void> {
  const search = req.body.search as string
  console.log("🚀 ~ file: saveLog.ts:7 ~ saveSearchLog ~ search:", search)
  const { user } = req
  let data:ICreateSearchLog
  if(search) {
    if( user === 'anonymous'){
      data = {
        value: search.toLowerCase(),
        user: "anonymous"
      }
    }
    else{
      data = {
        value: search.toLowerCase(),
        user: user.claims._id
      }
    }
    await LogService.pushSearchLog(data)
    await LogService.addSearchLogCount(search.toLowerCase())

  } 
  next()
}

export default { saveSearchLog }