import { NextFunction, Request, Response } from 'express'

async function sort(req: Request, _res: Response, next: NextFunction): Promise<void> {
  const sortString = req.query.sort as string || '-_id'
  let sortValue
  let sortField
  if(sortString[0] === '-') {
    sortValue = -1
    sortField = sortString.slice(1, sortString.length)
  }
  else {
    sortValue = 1
    sortField = sortString
  }
  req.sort = {} 
  req.sort[`${sortField}`] = sortValue
  next()
}

async function searchSort(req: Request, _res: Response, next: NextFunction): Promise<void> {
  const sortString = req.query.sort as string || 'name.desc'
  const sort = sortString.split(',') 
  req.sort.field = sort[0]
  req.sort.order = sort[1]
  console.log("🚀 ~ file: sort.middleware.ts:25 ~ searchSort ~ req.sort:", req.sort)
  next()
}

export default {
   sort,
   searchSort
}