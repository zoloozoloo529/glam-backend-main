import { Request, Response, NextFunction } from 'express'

const AdminVerify = async (
  req: Request,
  res: Response,
  next: NextFunction
):Promise<void | any> => {
    const { user } = req
    if(user.claims.role !== "admin"){
        return res.resourceForbidden()
    }
    next()
}

const AdminMiddleware = {
    AdminVerify
}

export default AdminMiddleware