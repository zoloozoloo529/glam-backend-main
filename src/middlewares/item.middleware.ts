import { Request, Response, NextFunction } from 'express'
import { ViewService } from '@root/services'

const updateViewCount = async (
  req: Request,
  res: Response,
  next: NextFunction
):Promise<void | any> => {
  const { id, type } = req.params
  const existView = await ViewService.getViewCount(id)
  if(!existView) {
    await ViewService.createViewCount({itemId: id, type:type})
  }
  await ViewService.addViewCount(id)
  next()
}

//maybe better write it in FINALLY, not as middleware
const updateSaveCount = async (
  req: Request,
  res: Response,
  next: NextFunction
):Promise<void | any> => {
  const { id } = req.params
  const type  = req.body.itemType
  const existView = await ViewService.getViewCount(id)
  if(!existView) {
    await ViewService.createViewCount({itemId: id, type:type})
  }
  await ViewService.addSaveCount(id)
  next()
}

const bookMiddleware = {
  updateViewCount,
  updateSaveCount
}

export default bookMiddleware