import { SearchLog } from '@root/models'
import { Request, Response, NextFunction } from 'express'

async function validateUserWithHistory(
  req: Request,
  res: Response,
  next: NextFunction
):Promise<void | any> {
  const { id } = req.params
  const { user } = req
  const result = await SearchLog.find({_id: id, user: user.claims._id})
  if(!result) {
    return res.resourceForbidden()
  }
  next()
}

export default { validateUserWithHistory }