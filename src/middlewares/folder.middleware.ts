import { FolderService } from '@root/services'
import { Request, Response, NextFunction } from 'express'

async function validateUserPermission(
  req: Request,
  res: Response,
  next: NextFunction
):Promise<void | any> {
  const { id } = req.params
  const { user } = req
  const result = await FolderService.getFolderForMiddleware({_id: id, user: user.claims._id})
  if(!result) {
    return res.resourceForbidden()
  }
  next()
}

async function validateDeletePermission(
  req: Request,
  res: Response,
  next: NextFunction
):Promise<void | any> {
  const { id } = req.params
  const { user } = req
  const result = await FolderService.getFolderForMiddleware({_id: id, user: user.claims._id})
  if(!result || result.type === 'all') {
    return res.resourceForbidden()
  }
  next()
}

export default { validateUserPermission, validateDeletePermission }