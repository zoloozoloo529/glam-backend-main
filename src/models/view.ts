import { IView } from '@root/interfaces'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
  {
    itemId: { type: String, required: true },
    type: { type: String, required: true },
    viewCount: { type: Number, default: 0 },
    saveCount: { type: Number, default: 0 }
  },
  {
    timestamps: true
  }
)

const collectionName = 'views'
const modelName = 'View'

export default mongoose.primary.model<IView>(
  modelName,
  schema,
  collectionName
)