import { ILocation } from '@root/interfaces'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
  {
    latitude: { type: Number, required: true },
    longitude: { type: Number, required: true },
    zoom: { type: Number, default: 17 },
    name: { type: String, required: true },
    description: { type: String },
    type: { type: String, required: true, enum: ["museum", "library", "theatre", "artwork", "immovable heritage"] },
    image: { type: String },
    address: { type: String },
    status: { type: Number, default: 1 },
    timetable: { type: {} },
    protection: { type: String },
    heritage_id: { type: String }
  },
  {
    timestamps: true
  }
)

const collectionName = 'locations'
const modelName = 'Location'

export default mongoose.primary.model<ILocation>(
  modelName,
  schema,
  collectionName
)