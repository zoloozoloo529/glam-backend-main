import { IResearch } from '@root/interfaces'
import mongoose, { Schema } from 'mongoose'


const schema = new Schema(
    {
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        title: {
            type: String,
        },
        note: {
            type: String,
        },
        edition: {
            type: String,
        },
        locale: {
            type: String,
        },
        settings: {
            type: Object,
            required: true
        },
        contentItemId: {
            type: String,
            required: true
        },
        contentItemType: {
            type: String,
            required: true
        },
        publisher: {
            type: String,
        },
        pageNumber: {
            type: Number,
        },
        reference: {
            type: String,
            required: true
        },
        type: {
            type: String,
            required: true
        },
        folderId: {
            type: Schema.Types.ObjectId,
            ref: 'ResearchFolder',
        },
        status: {
            type: Number,
            default: 1
        }
    },
    {
        timestamps: true,
    })


const modelName = 'Research'
const collectionName = 'researches'

export default mongoose.primary.model<IResearch>(
    modelName,
    schema,
    collectionName
)