import { IFolder } from '@root/interfaces'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
    {
        name: { type: String, required: true },
        user: { type: Schema.Types.ObjectId, ref: 'User' },
        color: { type: String, required: true, default: 'F15E22' },
        type: {
            type: String,
            default: 'normal',
            enum: ['normal', 'all', 'location']
        },
        items: {
            artwork: [{ type: Schema.Types.ObjectId }],
            koha: [{ type: String }],
            library: [{ type: String }],
            archive: [{ type: String }],
            heritage: [{ type: String }],
            location: [{ type: String }]
        }, 
        itemCount: { type: Number, default: 0 },
        status: { type: Number, default: 1 }
    },
    {
        timestamps: true
    }
)

const modelName = 'Folder'
const collectionName = 'folders'

export default mongoose.primary.model<IFolder>(
    modelName,
    schema,
    collectionName
)
