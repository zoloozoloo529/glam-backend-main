import { IUser } from '@root/interfaces'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
    {
        email: { type: String, required: true },
        password: { type: String, required: true },
        role: {
            type: String,
            required: true,
            default: 'user',
            enum: ['admin', 'organization-user', 'user']
        },
        verified: { type: Boolean, default: false },
        verification: { type: String, required: true },
        fcm_token: { type: String },
        firstname: { type: String, required: true },
        lastname: { type: String, required: true },
        img: { type: String },
        age: { type: Number },
        isForeigner: { type: Boolean, default: false },
        country: { type: String, default: 'Монгол' },
        library_password: { type: String },
        library_access_token: { type: String },
        library_isConnected: { type: Boolean, default: false },
        dan: {
            isConnected: { type: Boolean, default: false },
            data: {
                aimagCityCode: { type: String },
                addressStreetName: { type: String },
                addressDetial: { type: String },
                aimagCityName: { type: String },
                regnum: { type: String },
                civilId: { type: String },
                firstname: { type: String },
                lastname: { type: String },
                surname: { type: String },
                image: { type: String },
                nationality: { type: String },
                gender: { type: String },
                birthDate: { type: Date },
                birthDateAsText: { type: String },
                birthPlace: { type: String },
                passportIssueDate: { type: String },
                passportExpireDate: { type: String },
                passportAddress: { type: String },
                soumDistrictCode: { type: String },
                soumDistrictName: { type: String },
                bagKhorooName: { type: String },
                addressTownName: { type: String },
                addressApartmentName: { type: String },
                bagKhorooCode: { type: String },
                addressRegionName: { type: String }
            }
        },
        activeStatus: { type: Number, default: 1 },
        status: { type: Number, default: 1 }
    },
    {
        timestamps: true
    }
)

const collectionName = 'users'
const modelName = 'User'

export default mongoose.primary.model<IUser>(modelName, schema, collectionName)
