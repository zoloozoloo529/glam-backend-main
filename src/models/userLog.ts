import mongoose, { Schema } from 'mongoose'
import { IUserLog } from '@root/interfaces'

const schema: Schema = new Schema(
    {
        user: { type: String },
        system: { type: String, default: "user", enum: ["user", "admin"] },
        type: { type: String, required: true, enum:["EditUser", "ChangeRole", "ViewUser", "CreateFolder", "UpdateFolder", "DeleteFolder", "SaveItem", "UnsaveItem", "ViewFolder", "ViewItem", "UpgradeSession", "EditInfo", "Login", "Register", "Resend", "ResetPassword", "ForgetPassword", "Verify", "DeleteSearch", "DeleteAllSearch", "Search", "ConnectDL", "UpdateDLPassword"] },
        address: { type: String, required: true },
        user_agent: { type: String, required: true },
        params: { type: String },
        data: { type: String }
    },
    { 
        timestamps: true
    }
)

const modelName = 'UserLog'
const collectionName = 'user_logs'

export default mongoose.primary.model<IUserLog>(modelName, schema, collectionName)
