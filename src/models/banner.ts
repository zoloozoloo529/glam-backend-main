import { IBanner } from '@root/interfaces'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
    {
        name: { type: String, required: true },
        img: { type: String, required: true },
        link: { type: String, required: true },
        order: { type: Number, required: true },
        hidden: { type: Boolean, required: true },
        status: { type: Number, required: true, default: 1 }
    },
    {
        timestamps: true
    }
)

const modelName = 'Banner'
const collectionName = 'banners'
export default mongoose.primary.model<IBanner>(
    modelName,
    schema,
    collectionName
)
