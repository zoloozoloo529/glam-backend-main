import { IInvoice } from '@root/interfaces'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema (
  {
    user: { type: String, required: true },
    amount: { type: Number },
    isPaid: { type: Boolean, default: false },
    steppePayment: { type: Schema.Types.Mixed },
    billNumber: { type: String, required: true }   
  },
  {
    timestamps: true
  }
)

const collectionName = 'invoices'
const modelName = 'Invoice'

export default mongoose.primary.model<IInvoice>(
  modelName,
  schema,
  collectionName
)