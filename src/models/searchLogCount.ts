import { ISearchLogCount } from '@root/interfaces/ISearchLogCount'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
  {
    value: { type: String, unique: true },
    count: { type: Number, default: 1 }
  },
  {
    timestamps: true
  }
)

const modelName = 'SearchLogCount'
const collectionName = 'searchLogCount'

export default mongoose.primary.model<ISearchLogCount>(
  modelName,
  schema,
  collectionName
)