import mongoose, { Schema } from 'mongoose'
import { INotification } from '@root/interfaces'

const schema = new Schema(
    {
        attributes: { type: Schema.Types.Mixed },
        scheduledAt: { type: Date },
        createdBy: { type: Schema.Types.ObjectId, ref: 'User' },
        updatedBy: { type: Schema.Types.ObjectId, ref: 'User' },
        user: { type: Schema.Types.ObjectId, ref: 'User' },
        status: { type: Number, default: 1 }
    },
    {
        timestamps: true
    }
)

const modelName = 'Notification'
const collectionName = 'notifications'

export default mongoose.primary.model<INotification>(
    modelName,
    schema,
    collectionName
)
