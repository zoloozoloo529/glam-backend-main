import { IResearchFolder } from '@root/interfaces'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
    {
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        title: {
            type: String,
            required: true
        },
        isShared: {
            type: Boolean,
            required: true,
            default: false
        },
        inviteAccepted: {
            type: Boolean,
            default: false
        },
        folderOwner:
        {
            _id: {
                type: Schema.Types.ObjectId,
                ref: 'User',
                required: true
            },
            email: {
                type: String,
                required: true
            },
            firstname: {
                type: String,
                required: true
            },
            lastname: {
                type: String,
                required: true
            },
            img: {
                type: String,
            }
        }
        ,
        invitedMembers: [
            {
                type: Schema.Types.ObjectId,
                ref: 'User',
                required: true
            }
        ],
        folderMembers: [
            {
                firstname: {
                    type: String,
                    required: true
                },
                lastname: {
                    type: String,
                    required: true
                },
                email: {
                    type: String,
                    required: true
                },
                img: {
                    type: String,
                    required: true
                }
            }
        ],
        status: {
            type: Number,
            required: true,
            default: 1
        }
    }
)

const modelName = 'ResearchFolder'
const collectionName = 'researchFolder'


export default mongoose.primary.model<IResearchFolder>(
    modelName,
    schema,
    collectionName
)
// import { Request, Response } from 'express';
// import { Op } from 'sequelize';
// import { ResearchType, Research, ContentItem, ResearchSharedFolder, ContentResearch } from './models'; // Import your Sequelize models

// interface ResearchBody {
//     title: string;
//     note: string;
//     page_number: number;
//     reference: string;
//     type: string; // Assuming it's a string, adjust accordingly
//     edition: string;
//     publisher?: string;
//     locale: string;
//     settings: any; // Adjust the type accordingly
//     content_item_id: string;
//     folder_id?: string;
// }

// export async function researchPost(req: Request, res: Response) {
//     const user = req.user; // Assuming user is attached to the request by middleware
//     const body: ResearchBody = req.body;

//     try {
//         // Validation and decoding logic goes here

//         const researchType = await ResearchType.findOne({ where: { code: body.type } });
//         if (!researchType) {
//             return res.status(400).json({ error: "Can't get type, check it again please" });
//         }

//         let research: Research;

//         await Sequelize.transaction(async (transaction) => {
//             // Transaction logic goes here

//             const contentItem = await ContentItem.findOne({
//                 where: { id: body.content_item_id, deleted_at: null },
//                 transaction,
//             });

//             if (!contentItem) {
//                 return res.status(400).json({ error: "Can't find content item" });
//             }

//             let folderInfo: ResearchSharedFolder | null = null;
//             if (body.folder_id) {
//                 folderInfo = await ResearchSharedFolder.findOne({
//                     where: {
//                         folder_id: body.folder_id,
//                         [Op.or]: [
//                             { user_id: user.id, is_shared: false },
//                             { user_id: user.id, is_shared: true, invite_accepted: true },
//                         ],
//                     },
//                     transaction,
//                 });

//                 if (!folderInfo) {
//                     const newErr = new Error("Folder find error");
//                     return Promise.reject(newErr);
//                 }
//             }

//             switch (researchType.code) {
//                 case 'quote':
//                     research = await Research.create({
//                         user_id: user.id,
//                         page_number: body.page_number,
//                         type_id: researchType.id,
//                         edition: body.edition,
//                         publisher: body.publisher!,
//                         locale: body.locale,
//                     }, { transaction });
//                     break;
//                 case 'master_note':
//                     research = await Research.create({
//                         user_id: user.id,
//                         note: body.note,
//                         type_id: researchType.id,
//                     }, { transaction });
//                     break;
//                 case 'note':
//                     research = await Research.create({
//                         user_id: user.id,
//                         page_number: body.page_number,
//                         title: body.title,
//                         note: body.note,
//                         type_id: researchType.id,
//                     }, { transaction });
//                     break;
//                 default:
//                     throw new Error("Invalid research type");
//             }

//             research.settings = body.settings;
//             research.reference = body.reference;

//             let contentResearchCheck = await ContentResearch.findOne({
//                 where: { content_item_id: contentItem.id, user_id: user.id },
//                 transaction,
//             });

//             if (contentResearchCheck) {
//                 research.contentResearch = contentResearchCheck;
//             } else {
//                 const newContentResearch = await ContentResearch.create({
//                     user_id: user.id,
//                     content_id: contentItem.content_id, // Adjust accordingly based on your model
//                     content_item_id: contentItem.id,
//                 }, { transaction });

//                 research.contentResearch = newContentResearch;
//             }

//             if (folderInfo) {
//                 research.folder_id = folderInfo.folder_id;
//             }

//             await research.save({ transaction });

//             if (!contentResearchCheck) {
//                 const contentResearch = await ContentResearch.create({
//                     content_item_id: contentItem.id,
//                     content_id: contentItem.content_id,
//                     user_id: user.id,
//                 }, { transaction });
//             }
//         });

//         return res.json(research);
//     } catch (error) {
//         console.error(error);
//         return res.status(500).json({ error: 'Error to create research' });
//     }
// }
