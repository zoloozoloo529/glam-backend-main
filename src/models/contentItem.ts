import { IContentItem } from '@root/interfaces/IContentItem'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
    {
        img: { type: String, required: true },
        link: { type: String, required: true },
        order: { type: Number, required: true },
        hidden: { type: Boolean, required: true, default: false },
        status: { type: Number, required: true, default: 1 },
        languages: {
            type: Map,
            of: {
                name: { type: String, required: true },
                description: { type: String, required: true }
            },
            required: true
        }
    },
    {
        timestamps: true
    }
)

const modelName = 'ContentItem'
const collectionName = 'contentItems'
export default mongoose.primary.model<IContentItem>(
    modelName,
    schema,
    collectionName
)
