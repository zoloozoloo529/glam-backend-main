import { IKohaItem } from '@root/interfaces'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
  {
    name: { type: String },
    mainImg: { type: String },
    itemInf: {
      artist: {
        name:{ type: String },
      }
    },
    viewCount: { type: Number, default: 0 },
    saveCount: { type: Number, default: 0 },
    fields: [{
      name: { type: String },
      value: { type: String },
    }],
    biblionumber: { type: Number, unique:true, required: true },
    book_name: { type: String },
    author_koha: { type: String },
    isbn_code: { type: String },
    press_year: { type: Number },
    url_image: { type: String },
    publishercode: { type: String },
    pages: { type: String },
    press_org_koha: { type: String },
    koha_languages: { type: String },
    type: { type: String, enum: ['koha'], required: true },
    status: { type: Number, default: 1 }
  },
  {
    timestamps: true
  }
)

const modelName = 'KohaItem'
const collectionName = 'kohaItems'

export default mongoose.primary.model<IKohaItem>(
  modelName,
  schema,
  collectionName
)