import { ISavedItem } from "@root/interfaces/ISavedItem";
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
    {
        user_id: { type: String, required: true},
        type: { type: String, required: true, enum:["artwork","library","archive","heritage","koha"]},
        item_id: { type: String, required: true },
        item_name: {type: String, required: true },
        main_img: {type: String},
        heritage_type: {type: String}
    },{
        timestamps: true
    }
)

const modelName = "SavedItem"
const collectionName = "saved_items"

export default mongoose.primary.model<ISavedItem>(modelName, schema, collectionName)