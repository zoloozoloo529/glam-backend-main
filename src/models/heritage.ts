import { IHeritageItem } from '@root/interfaces'
import mongoose, { Schema } from 'mongoose'

const schema = new Schema(
    {
        name: { type: String },
        mainImg: { type: String },
        itemInf: {
        artist: {
            name:{ type: String },
        }
        },
        viewCount: { type: Number, default: 0 },
        saveCount: { type: Number, default: 0 },
        fields: [{
            name: { type: String },
            value: { type: String },
        }],
        itemId: { type: String },
        heritage_type : { type: String },
        unesco_heritage_id : { type: String },
        unesco_heritage_name : { type: String },
        species_name : { type: String },
        species_id : { type: String },
        intangible_heritage_desc : { type: String },
        heritage_area : { type: String },
        heritage_attachments :[],
        location : [],
        area_coordinate : [],
        keeper_name : { type: String },
    },
    {
        timestamps: true
    }
)

const modelName = 'HeritageItem'
const collectionName = 'heritageItems'

export default mongoose.primary.model<IHeritageItem>(
  modelName,
  schema,
  collectionName
)