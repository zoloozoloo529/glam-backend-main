import { ISearchLog } from '@root/interfaces'
import mongoose,{ Schema } from 'mongoose'

const schema = new Schema(
  {
    value: { type: String, required: true },
    user: { type: String },
    status: { type: Number, default: 1 }
  },
  {
    timestamps: true
  }
)

const modelName = 'SearchLog'
const collectionName = 'searchLogs'

export default mongoose.primary.model<ISearchLog>(
  modelName,
  schema,
  collectionName
)