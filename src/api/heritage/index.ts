import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { createHeritageRef, getCenturies, getHeritageRefById, getHeritageRefList, getHeritageSubTypes, getHeritageTypes, getIntangibleSpecies, getProvinces, getRegions, getImmovableSpecies, getMovableSpecies } from './heritage.controller'
import { authenticationMiddleware } from '@root/middlewares'

const HeritageRouter = Router()

HeritageRouter.get('/provinces', asyncHandler(getProvinces))
HeritageRouter.get('/provinces/:province/regions', asyncHandler(getRegions))
HeritageRouter.get('/types', asyncHandler(getHeritageTypes))
HeritageRouter.get('/types/:type', asyncHandler(getHeritageSubTypes))
HeritageRouter.post('/ref', authenticationMiddleware.checkJwt, authenticationMiddleware.checkVerification, asyncHandler(createHeritageRef))
HeritageRouter.get('/ref/:id', authenticationMiddleware.checkJwt, authenticationMiddleware.checkVerification, asyncHandler(getHeritageRefById))
HeritageRouter.post('/refs', authenticationMiddleware.checkJwt, authenticationMiddleware.checkVerification, asyncHandler(getHeritageRefList))
HeritageRouter.get('/centuries', asyncHandler(getCenturies))
HeritageRouter.get('/species/intangible', asyncHandler(getIntangibleSpecies))
HeritageRouter.get('/species/immovable', asyncHandler(getImmovableSpecies))
HeritageRouter.get('/species/movable', asyncHandler(getMovableSpecies))

export default HeritageRouter