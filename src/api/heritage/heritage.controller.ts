import { Request, Response } from 'express'
import { HeritageService } from '@root/services'
import FormData from 'form-data'

export async function getProvinces(
  req: Request,
  res: Response
): Promise<any> {
  let result
  try {
    result = await HeritageService.getProvinces("")
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}

export async function getRegions(
  req: Request,
  res: Response
): Promise<any> {
  const { province } = req.params
  let result
  try {
    result = await HeritageService.getRegions(province, "")
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}

// getHeritageTypes,
//   getHeritageSubTypes,
//   createHeritageRef,
//   getHeritagesRefs,
//   getHeritageRefById,

export async function getHeritageTypes(
  req: Request,
  res: Response
): Promise<any> {
  let result
  try {
    result = await HeritageService.getHeritageTypes()
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}

export async function getHeritageSubTypes(
  req: Request,
  res: Response
): Promise<any> {
  const { type } = req.params
  let result
  try {
    result = await HeritageService.getHeritageSubTypes(type)
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}

export async function createHeritageRef(
  req: Request,
  res: Response
): Promise<any> {
  const image = req.file as any;

  const { type, subtype, region, province, address,
    content, registerNumber, firstName, lastName
  } = req.body
  const formData = new FormData();


  formData.append('images', image.data, {
    filename: image.originalname,
    contentType: image.mimetype,
  });
  formData.append('firstname', firstName);
  formData.append('lastname', lastName);
  formData.append('registernumber', registerNumber);
  formData.append('provinceid', province);
  formData.append('regionid', region);
  formData.append('address', address);
  formData.append('heritagetypeid', type);
  formData.append('heritagesubtypeid', subtype);
  formData.append('content', content);


  let result;

  try {
    result = await HeritageService.createHeritageRef(formData)
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}
export async function getHeritageRefById(
  req: Request,
  res: Response
): Promise<any> {
  const { id } = req.params
  let result
  try {
    result = await HeritageService.getHeritageRefById(id)
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}

export async function getHeritageRefList(
  req: Request,
  res: Response
): Promise<any> {
  const { registerNumber } = req.body

  let result
  const data = {
    "default_param": [
      {
        "key": "register_number",
        "value": registerNumber

      }
    ]
  }
  try {
    result = await HeritageService.getHeritagesRefs(data)
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}

export async function getCenturies(
  req: Request,
  res: Response
): Promise<any> {
  let result
  try {
    result = await HeritageService.getHeritageCenturies()
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}

export async function getMovableSpecies(
  req: Request,
  res: Response
): Promise<any> {
  let result
  try {
    result = await HeritageService.getHeritageMovableSpecies()
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}

export async function getImmovableSpecies(
  req: Request,
  res: Response
): Promise<any> {
  let result
  try {
    result = await HeritageService.getHeritageImmovableSpecies()
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}

export async function getIntangibleSpecies(
  req: Request,
  res: Response
): Promise<any> {
  let result
  try {
    result = await HeritageService.getHeritageIntangibleSpecies()
  }
  catch (error) {
    return res.badRequest(new Error(error))
  }
  finally {
    return res.respondWithData(result.data)
  }
}