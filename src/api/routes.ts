import express, { Router } from 'express'
import asyncHandler from 'express-async-handler'
import AuthRouter from '@root/api/auth'
import ItemRouter from './item'
import AdminRouter from './admin'
import ArtworkRouter from './artwork'
import { authenticationMiddleware } from '@root/middlewares'
import UserRouter from './user'
import LibraryRouter from './library'
import UploadRouter from './upload'
import KOHARouter from './koha'
import LocationRouter from './location'
import HeritageRouter from './heritage'
import BannerRouter from './banner'
import ContentItemRouter from './contentItem'
import NotificationRouter from './notification'
import ResearchFolderRouter from './research/folder'
import ResearchRouter from './research'
import SavedItemRouter from './savedItem'
import path from 'path'

const router = Router()

router.use('/admin', asyncHandler(AdminRouter))
router.use('/auth', asyncHandler(AuthRouter))
router.use(
    '/user',
    authenticationMiddleware.checkJwt,
    authenticationMiddleware.checkVerification,
    asyncHandler(UserRouter)
)
router.use('/items', asyncHandler(ItemRouter))
router.use('/artwork', asyncHandler(ArtworkRouter))
router.use('/heritage', asyncHandler(HeritageRouter))
router.use('/library', asyncHandler(LibraryRouter))
router.use('/upload', asyncHandler(UploadRouter))
router.use('/koha', asyncHandler(KOHARouter))
router.use('/location', asyncHandler(LocationRouter))
router.use('/banner', asyncHandler(BannerRouter))
router.use('/content-item', asyncHandler(ContentItemRouter))
router.use('/notification', asyncHandler(NotificationRouter))
router.use('/saved', asyncHandler(SavedItemRouter))

router.use('/research', authenticationMiddleware.checkJwt,
    authenticationMiddleware.checkVerification, asyncHandler(ResearchRouter))

router.use('/research/folder',
    authenticationMiddleware.checkJwt,
    authenticationMiddleware.checkVerification,
    asyncHandler(ResearchFolderRouter))

router.use('/static', express.static(path.join(__dirname, '../../..','public/uploads')))

export default router
