import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { getBiblios, getBiblioWithIds, getItem, getNewItems } from './biblio.controller'
import paginationMiddleware from '@root/middlewares/pagination.middleware'


const BiblioRouter = Router()

BiblioRouter.post('/', paginationMiddleware.paginate, asyncHandler(getBiblioWithIds))
BiblioRouter.get('/', paginationMiddleware.paginate, asyncHandler(getBiblios))
BiblioRouter.get('/new', asyncHandler(getNewItems))
BiblioRouter.get('/:id', asyncHandler(getItem))

export default BiblioRouter