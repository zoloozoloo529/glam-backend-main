import logger from "@root/loaders/logger";
import { KOHABiblioService, LibraryBookService, ViewService } from "@root/services";
import { Request, Response } from "express";

export async function getBiblioWithIds(
  req: Request,
  res: Response
):Promise<any> {
  let { pagination } = req
  let { ids } = req.body
  const result = await KOHABiblioService.getBiblioWithIds(ids, pagination);
  pagination.total = result.total
  return res.respondWithData(result.biblio, pagination)
}

export async function getBiblios(
  req: Request,
  res: Response
):Promise<any> {
  let { pagination } = req
  let search = req.params.search
  const filter = {
    page: pagination.page,
    limit: pagination.limit,
    search: search
  }
  const result = await KOHABiblioService.getBiblios(filter);
  pagination.total = result.total
  return res.respondWithData(result.biblio, pagination)
}

export async function getNewItems(
  req: Request,
  res: Response
):Promise<any> {
  const result = await KOHABiblioService.getBiblios({page: req.query.page, limit: req.query.limit, search: ''})
  return res.respondWithData(result.biblio)
}

export async function getItem(
  req: Request,
  res: Response
):Promise<any> {
  const { id } = req.params
  const result = await KOHABiblioService.getBiblioWithIds([id], { page: 1, limit: 10 })
  let existBiblio = result.biblio[0] as any
  const existView = await ViewService.getViewCount(id)
  console.log('view: ', existView)
  existBiblio.viewCount = existView.viewCount
  existBiblio.saveCount = existView.saveCount
  return res.respondWithData(existBiblio)
}