import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import BiblioRouter from './biblio'


const KOHARouter = Router()

KOHARouter.use('/library', asyncHandler(BiblioRouter))


export default KOHARouter