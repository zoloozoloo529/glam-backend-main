import { Request, Response } from 'express'
import { LogService } from '@root/services'

export async function getUserLogs (req: Request, res: Response):Promise<any> {
    const { pagination, sort } = req
    const { type, user_id } = req.query
    let filter = {}
    if(type){
        filter = Object.assign(filter, {"type":type})
    }
    if(user_id){
        filter = Object.assign(filter, {"user":user_id})
    }
    const result = await LogService.getUserLogs(filter, pagination, sort)
    res.respondWithData(result)
}