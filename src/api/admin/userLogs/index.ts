import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { 
  getUserLogs
} from './userLogs.controller'
import { PaginationMiddleware, sort } from '@root/middlewares'

const LogRouter = Router()

LogRouter.get('/', PaginationMiddleware.paginate,sort.sort, asyncHandler(getUserLogs))

export default LogRouter