import {
    UserService
} from '@root/services'
import { Request, Response } from 'express'
// import { Types } from 'mongoose'
import fs from 'fs'
import jose from 'node-jose'
import { CryptoHelpers } from '@root/utils'
import { IUserLogCreate } from '@root/interfaces'

export async function login(req: Request, res: Response): Promise<any> {
    const { email, password } = req.body
    let token, user
    try {
        const encryptedPassword = CryptoHelpers.encrypt(password)
        const login = {
            email,
            password: encryptedPassword
        }
        user = await UserService.getUser(login)
        if (!user) {
            throw 'Нэр эсвэл нууц үг буруу байна.'
        }
        if (user.activeStatus == 0) {
            throw 'Таны нэвтрэх эрх хаагдсан байна!'
        }
        if (user.role !== 'admin') {
            throw 'forbidden'
        }
        const JWKeys = fs.readFileSync('Keys.json')
        const keyStore = await jose.JWK.asKeyStore(JWKeys.toString())
        const [key] = keyStore.all({ use: 'sig' })
        const opt = { compact: true, jwk: key, fields: { typ: 'jwt' } }

        const payload = JSON.stringify({
            exp: Math.floor(Date.now() + 864000),
            iat: Math.floor(Date.now() / 1000),
            claims: {
                email: user.email,
                role: user.role,
                _id: user._id,
                verified: user.verified
            }
        })

        token = await jose.JWS.createSign(opt, key).update(payload).final()
    } catch (error) {
        if(error == 'forbidden'){
            return res.resourceForbidden()
        }
        return res.badRequest(new Error(error))
    } finally {
        const params = { ...req.params, ...req.query}
            const logdata: IUserLogCreate = {
            user: user._id,
            system: 'admin',
            type: 'Login',
            address: req.ip || req.socket.remoteAddress,
            user_agent: req.headers['user-agent'],
            params: JSON.stringify(params),
            data: JSON.stringify(req.body.email)
            }
            UserService.addLog(logdata)
        if (!user.verified) {
            return res.respondWithData({
                verified: user.verified,
                email: email
            })
        } else {
            return res.respondWithData({
                accessToken: token,
                verified: user.verified,
                email: email
            })
        }
    }
}