import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { login } from './auth.controller'

const AdminAuthRouter = Router()

AdminAuthRouter.post('/login', asyncHandler(login))

export default AdminAuthRouter
