import { Request, Response } from 'express'
import { LogService } from '@root/services'

export async function getSearchLogs(
  req: Request,
  res: Response
):Promise<any> {
  const { pagination } = req
  const logs = await LogService.getSearchLogs(pagination)
  pagination.total = await LogService.getSearchLogCount()

  return res.respondWithData(logs, pagination)
}


export async function getSearchLogCounts(
  req: Request,
  res: Response
):Promise<any> {
  const { pagination } = req
  const result = await LogService.getSearchLogCounts(pagination)
  pagination.total = await LogService.getTotalSearchLogCount()

  return res.respondWithData(result)
}

// export async function getUserLogs(
//   req: Request,
//   res: Response
// ):Promise<any> {
//   let { pagination } = req

//   const logs = await LogService.getSearchLogs(pagination)
//   pagination.total = await LogService.getSearchLogCount()

//   return res.respondWithData(logs, pagination)
// }