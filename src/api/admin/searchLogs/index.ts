import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { 
  getSearchLogCounts, 
  getSearchLogs 
} from './adminLogs.controller'
import { PaginationMiddleware } from '@root/middlewares'

const AdminLogRouter = Router()

AdminLogRouter.get('/', PaginationMiddleware.paginate, asyncHandler(getSearchLogs))
AdminLogRouter.get('/count',PaginationMiddleware.paginate, asyncHandler(getSearchLogCounts))

export default AdminLogRouter