import { AdminMiddleware, authenticationMiddleware } from '@root/middlewares'
import asyncHandler from 'express-async-handler'
import { Router } from 'express'
import {
    createBanner,
    deleteBanner,
    getBanners,
    updateBanner
} from './banner.controller'

const BannerRouter = Router()
BannerRouter.get(
    '/',
    authenticationMiddleware.checkJwt,
    authenticationMiddleware.checkVerification,
    AdminMiddleware.AdminVerify,
    asyncHandler(getBanners)
)
BannerRouter.post(
    '/',
    authenticationMiddleware.checkJwt,
    authenticationMiddleware.checkVerification,
    AdminMiddleware.AdminVerify,
    asyncHandler(createBanner)
)
BannerRouter.patch(
    '/:id',
    authenticationMiddleware.checkJwt,
    authenticationMiddleware.checkVerification,
    AdminMiddleware.AdminVerify,
    asyncHandler(updateBanner)
)
BannerRouter.delete(
    '/:id',
    authenticationMiddleware.checkJwt,
    authenticationMiddleware.checkVerification,
    AdminMiddleware.AdminVerify,
    asyncHandler(deleteBanner)
)
export default BannerRouter
