import { Request, Response } from 'express'
import { IBannerCreate } from '@root/interfaces'
import BannerService from '@root/services/banner.service'
import { Types } from 'mongoose'

export async function createBanner(req: Request, res: Response): Promise<void> {
    const { name, link, img, order, hidden } = req.body

    const bannerData: IBannerCreate = {
        name,
        link,
        img,
        order,
        hidden
    }

    try {
        const result = await BannerService.create(bannerData)
        res.respondWithData(result)
    } catch (error) {
        console.error('Error creating banner:', error)
        res.internalError(new Error(error))
    }
}
export async function getBanners(req: Request, res: Response): Promise<void> {
    try {
        const result = await BannerService.getBanners()
        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}

export async function updateBanner(req: Request, res: Response): Promise<void> {
    const { id } = req.params
    const bannerData = req.body

    try {
        const result = await BannerService.updateBanner(id, bannerData)

        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}
export async function deleteBanner(req: Request, res: Response): Promise<void> {
    const { id } = req.params

    try {
        const result = await BannerService.deleteBanner(new Types.ObjectId(id))

        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}
