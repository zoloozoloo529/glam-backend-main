import { AdminMiddleware, authenticationMiddleware } from '@root/middlewares'
import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { me, paymentWebhook } from './admin.controller'
import AdminLogRouter from './searchLogs'
import AdminAuthRouter from './auth'
import UserRouter from './user'
import StatisticRouter from './statistics'
import BannerRouter from './banner'
import ContentItemRouter from './contentItem'
import LogRouter from './userLogs'

const AdminRouter = Router()

AdminRouter.use('/auth', asyncHandler(AdminAuthRouter))
AdminRouter.use('/users', asyncHandler(UserRouter))
AdminRouter.use('/statistics', asyncHandler(StatisticRouter))

AdminRouter.use(
    '/user_logs',
    authenticationMiddleware.checkJwt,
    AdminMiddleware.AdminVerify,
    asyncHandler(LogRouter)
)
AdminRouter.use(
    '/search_logs',
    authenticationMiddleware.checkJwt,
    AdminMiddleware.AdminVerify,
    asyncHandler(AdminLogRouter)
)
AdminRouter.use(
    '/banner',
    authenticationMiddleware.checkJwt,
    AdminMiddleware.AdminVerify,
    asyncHandler(BannerRouter)
)
AdminRouter.use(
    '/content-item',
    authenticationMiddleware.checkJwt,
    AdminMiddleware.AdminVerify,
    asyncHandler(ContentItemRouter)
)
AdminRouter.get(
    '/me',
    authenticationMiddleware.checkJwt,
    AdminMiddleware.AdminVerify,
    asyncHandler(me)
)
AdminRouter.post(
    '/webhook',
    authenticationMiddleware.checkJwt,
    asyncHandler(paymentWebhook)
)

export default AdminRouter
