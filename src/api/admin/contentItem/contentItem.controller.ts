import { Request, Response } from 'express'
import { IContentItem, IContentItemCreate } from '@root/interfaces/IContentItem'
import ContentItemService from '@root/services/contentItem.service'
import { Types } from 'mongoose'

export async function createContentItem(
    req: Request,
    res: Response
): Promise<void> {
    const { img, link, order, hidden, languages } = req.body

    const contentData: IContentItemCreate = {
        img,
        link,
        order,
        languages,
        hidden
    }

    try {
        const result = await ContentItemService.create(contentData)

        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}

export async function getContentItem(
    req: Request,
    res: Response
): Promise<void> {
    const { id } = req.params

    try {
        const result = await ContentItemService.findOne(new Types.ObjectId(id))

        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}

export async function getContentItems(
    req: Request,
    res: Response
): Promise<void> {
    try {
        const result = await ContentItemService.find({ status: 1 })

        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}

export async function updateContentItem(
    req: Request,
    res: Response
): Promise<void> {
    const { id } = req.params
    const { img, link, order, hidden, languages } = req.body

    const contentData: Partial<IContentItem> = {
        img,
        link,
        order,
        languages,
        hidden
    }

    try {
        const result = await ContentItemService.update(
            new Types.ObjectId(id),
            contentData
        )

        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}

export async function deleteContentItem(
    req: Request,
    res: Response
): Promise<void> {
    const { id } = req.params

    try {
        const result = await ContentItemService.delete(new Types.ObjectId(id))

        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}
