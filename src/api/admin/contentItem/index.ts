import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { AdminMiddleware, authenticationMiddleware } from '@root/middlewares'
import { createContentItem, deleteContentItem, getContentItem, getContentItems, updateContentItem} from './contentItem.controller'

const ContentItemRouter = Router()

ContentItemRouter.get('/',authenticationMiddleware.checkJwt,authenticationMiddleware.checkVerification,AdminMiddleware.AdminVerify,asyncHandler(getContentItems))
ContentItemRouter.get('/:id',authenticationMiddleware.checkJwt,authenticationMiddleware.checkVerification,AdminMiddleware.AdminVerify,asyncHandler(getContentItem))
ContentItemRouter.post('/',authenticationMiddleware.checkJwt,authenticationMiddleware.checkVerification,AdminMiddleware.AdminVerify,asyncHandler(createContentItem))
ContentItemRouter.patch('/:id',authenticationMiddleware.checkJwt,authenticationMiddleware.checkVerification,AdminMiddleware.AdminVerify,asyncHandler(updateContentItem))
ContentItemRouter.delete('/:id',authenticationMiddleware.checkJwt,authenticationMiddleware.checkVerification,AdminMiddleware.AdminVerify,asyncHandler(deleteContentItem))
export default ContentItemRouter
