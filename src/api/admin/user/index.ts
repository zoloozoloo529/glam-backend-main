import { AdminMiddleware, authenticationMiddleware, PaginationMiddleware, sort } from '@root/middlewares'
import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { changeUserRole, deleteSearchHistory, deleteUserAllSearchHistory, downloadUsersList, editUser, getFolderItems, getFolders, getSearchList, getTotalSearchCount, getUserById, getUserLogs, getUsers, getViewCount } from './user.controller'

const UserRouter = Router()

UserRouter.get('/', authenticationMiddleware.checkJwt, AdminMiddleware.AdminVerify, PaginationMiddleware.paginate, sort.sort, asyncHandler(getUsers))
UserRouter.get('/download', authenticationMiddleware.checkJwt, AdminMiddleware.AdminVerify, PaginationMiddleware.paginate, sort.sort, asyncHandler(downloadUsersList))
UserRouter.get('/:id', authenticationMiddleware.checkJwt, AdminMiddleware.AdminVerify, asyncHandler(getUserById))
UserRouter.get('/:id/logs', authenticationMiddleware.checkJwt, AdminMiddleware.AdminVerify, PaginationMiddleware.paginate, sort.sort, asyncHandler(getUserLogs))
UserRouter.get('/:id/view_count', authenticationMiddleware.checkJwt, asyncHandler(getViewCount))
UserRouter.get('/:id/search_list', authenticationMiddleware.checkJwt, PaginationMiddleware.paginate, asyncHandler(getSearchList))
UserRouter.get('/:id/search_count', authenticationMiddleware.checkJwt, asyncHandler(getTotalSearchCount))
UserRouter.get('/:id/folders', authenticationMiddleware.checkJwt, AdminMiddleware.AdminVerify, PaginationMiddleware.paginate, sort.sort, asyncHandler(getFolders))
UserRouter.get('/:id/folders/:id', authenticationMiddleware.checkJwt, AdminMiddleware.AdminVerify, PaginationMiddleware.paginate, sort.sort, asyncHandler(getFolderItems))

UserRouter.delete('/delete_search/:id', authenticationMiddleware.checkJwt, AdminMiddleware.AdminVerify, asyncHandler(deleteSearchHistory))
UserRouter.delete('/:id/delete_all_search', authenticationMiddleware.checkJwt, AdminMiddleware.AdminVerify, asyncHandler(deleteUserAllSearchHistory))

UserRouter.patch('/:id/change-role', authenticationMiddleware.checkJwt, AdminMiddleware.AdminVerify, asyncHandler(changeUserRole))
UserRouter.patch('/:id/edit-user', authenticationMiddleware.checkJwt, AdminMiddleware.AdminVerify, asyncHandler(editUser))


export default UserRouter