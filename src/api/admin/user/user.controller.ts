import { ArtworkItemService, FolderService, KOHABiblioService, LogService, UserService } from "@root/services";
import { Types } from 'mongoose'
import { Request, Response } from 'express'
import { IUpdateUser, IUserLogCreate } from "@root/interfaces";
import { SortHelper } from "@root/utils";
import excelJS from 'exceljs'
import { DateTime } from 'luxon'

export async function getUsers(
  req: Request,
  res: Response
):Promise<any> {
  let search = req.query.search as string
  const {pagination, sort } = req
  if (!search) {
    search = ''
  }
  const filter = {}
  const users = [] as any[]
  try{
    const result = await UserService.getUsers(filter, search, pagination, sort)
    pagination.total = await UserService.getUsersCount(filter, search)
    if(pagination.total%pagination.limit){
      pagination.total_page = Math.floor(pagination.total/pagination.limit) + 1
    }else{
      pagination.total_page = Math.floor(pagination.total/pagination.limit)
    }
    result.map((el:any) => {
      users.push({
        _id: el._id,
        email: el.email,
        firstname: el.firstname,
        lastname: el.lastname,
        role: el.role,
        verified: el.verified,
        isForeigner: el.isForeigner,
        country: el.country,
        img: el.img,
        activeStatus: el.activeStatus,
        createdAt: el.createdAt,
        updatedAt: el.updatedAt
      })
    })
  }
  catch(error){
    return res.badRequest(new Error(error))
  }
  finally{
    return res.respondWithData(users, pagination)
  }
}

export async function getUserById(
  req: Request,
  res: Response
):Promise<any> {
  const id = req.params.id
  let user
  try{
    const result = await UserService.getUserById(new Types.ObjectId(id)) as any
    if(!result){
      throw "Хэрэглэгч олдсонгүй"
    }
    user = {
      _id: result._id,
      email: result.email,
      firstname: result.firstname || "",
      lastname: result.lastname || "",
      role: result.role,
      verified: result.verified,
      isForeigner: result.isForeigner,
      country: result.country,
      img: result.img,
      activeStatus: result.activeStatus,
      createdAt: result.createdAt,
      updatedAt:result.updatedAt
    }
  }
  catch(error){
    return res.badRequest(new Error(error))
  }
  finally{
    const params = { ...req.params, ...req.query}
    const logdata: IUserLogCreate = {
      user: user._id,
      system: 'admin',
      type: 'ViewUser',
      address: req.ip || req.socket.remoteAddress,
      user_agent: req.headers['user-agent'],
      params: JSON.stringify(params),
      data: JSON.stringify(req.body)
    }
    UserService.addLog(logdata)
    return res.respondWithData(user)
  }
}

export async function getViewCount(
  req: Request,
  res: Response
):Promise<any> {
  const id = req.params.id
  const { startDate, endDate } = req.query
  let result
  try{
    // const user = await UserService.getUserById(new Types.ObjectId(id)) as any
    // if(!user){
    //   throw "Хэрэглэгч олдсонгүй"
    // }
    if(!startDate || !endDate){
      throw "Start and End date missing!!!"
    }
    result = await UserService.getViewItemCount({type: "ViewItem", user: id, createdAt:{$gte: startDate, $lte: endDate,}})
  }
  catch(error){
    return res.badRequest(new Error(error))
  }
  finally{
    return res.respondWithData({userId:id, startDate, endDate, view_count:result})
  }
}

export async function getTotalSearchCount(
  req: Request,
  res: Response
):Promise<any> {
  const id = req.params.id
  const { startDate, endDate } = req.query
  let result
  try{
    if(!startDate || !endDate){
      throw "Start and End date missing!!!"
    }
    result = await LogService.getSearchDocument({user: id, createdAt:{$gte: startDate, $lte: endDate}})
  }
  catch(error){
    return res.badRequest(new Error(error))
  }
  finally{
    return res.respondWithData({userId:id, startDate, endDate, search_count:result})
  }
}

export async function getSearchList(
  req: Request,
  res: Response
):Promise<any> {
  const { pagination } = req
  const id = req.params.id
  let result
  try{
    result = await LogService.getSearchLogsWithFilter({user:id}, pagination)
    pagination.total = await LogService.getSearchLogsCountDocument({user:id})
  }
  catch(error){
    return res.badRequest(new Error(error))
  }
  finally{
    return res.respondWithData(result, pagination)
  }
}

export async function deleteSearchHistory(
  req: Request,
  res: Response
):Promise<any> {
  const logId = req.params.id as string
  // let result
  try{
    // result = await 
    LogService.updateSearchLog(new Types.ObjectId(logId), {status:0})
  }
  catch(error){
    return res.badRequest(new Error(error))
  }
  finally{
    return res.respondWithData("success")
  }
}

export async function deleteUserAllSearchHistory(
  req: Request,
  res: Response
):Promise<any> {
  const id = req.params.id
  let result
  try{
    result = await LogService.deleteUserLogs(id)
  }
  catch(error){
    return res.badRequest(new Error(error))
  }
  finally{
    return res.respondWithData(result)
  }
}

export async function downloadUsersList(
  req: Request,
  res: Response
):Promise<any> {
  let search = req.query.search as string
  const {pagination, sort } = req
  if (!search) {
    search = ''
  }
  let result
  try{
    result = await UserService.getUsers({}, search, pagination, sort)

    const workbook = new excelJS.Workbook() // Create a new workbook
    const worksheet = workbook.addWorksheet('Users') // New Worksheet

    worksheet.columns = [
        { header: '', key: '', width: 3 },
        { header: 'Имэйл', key: 'email', width:30 },
        { header: 'Овог', key: 'lastname', width:20 },
        { header: 'Нэр', key: 'firstname', width:20 },
        { header: 'Иргэншил', key: 'country', width:15 },
        { header: 'Хэрэглэгчийн эрх', key: 'role', width:10 },
        { header: 'Create date', key: 'createdAt', width:10 }
    ]
    let count = 0
    result.forEach((el:any) =>{
      count++
      worksheet.addRow([count, el.email, el.lastname, el.firstname, el.country, el.role, el.createdAt])
    })

    const dateString = DateTime.utc().plus({ hours: 8 }).toFormat('yyyy/MM/dd-HH:mm:ss')
    const filename = `UsersList-${dateString}.xlsx`

    res.setHeader(
        'Content-Type',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    res.setHeader('Content-Disposition', 'attachment; filename=' + filename)

    await workbook.xlsx.write(res)
  }
  catch(error){
    return res.badRequest(new Error(error))
  }
  finally{
    return res.respondWithData(result)
  }
}


// const results = await Promise.all(promises)

//     const workbook = new excelJS.Workbook() // Create a new workbook
//     const worksheet = workbook.addWorksheet('report') // New Worksheet

//     worksheet.columns = [
//         { header: 'name', key: 'name' },
//         { header: 'count', key: 'count' }
//     ]

//     results.forEach((result) => {
//         worksheet.addRow(result)
//     })

//     // const json2csv = new Parser()
//     // const csv = json2csv.parse(results)

//     const dateString = Date().toString()
//     const filename = `${competitionId}-${dateString}.xlsx`

//     res.setHeader(
//         'Content-Type',
//         'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
//     )
//     res.setHeader('Content-Disposition', 'attachment; filename=' + filename)

//     await workbook.xlsx.write(res)

//     return res.respondWithData([])


export async function changeUserRole(
    req: Request,
    res: Response
  ):Promise<any> {
    const { role } = req.body
    const id = req.params.id;
    // let updatedData
    try{
      const user = await UserService.getUserById(new Types.ObjectId(id)) as any
      if(!user){
        throw "Хэрэглэгч олдсонгүй"
      }
      const data:IUpdateUser = {
        role: role.toLowerCase()
      }
      // const result = await 
      UserService.updateUser(new Types.ObjectId(id), data)
    }
    catch(error){
      return res.badRequest(new Error(error))
    }
    finally{
      const params = { ...req.params, ...req.query}
        const logdata: IUserLogCreate = {
        user: req.user.claims._id,
        type: 'ChangeRole',
        address: req.ip || req.socket.remoteAddress,
        user_agent: req.headers['user-agent'],
        params: JSON.stringify(params),
        data: JSON.stringify(req.body)
        }
        UserService.addLog(logdata)
      return res.respondWithData("Амжилттай")
    }
  }

  export async function getUserLogs(
    req: Request,
    res: Response
  ):Promise<any> {
    const { id } = req.params
    const {pagination, sort } = req
    let result
    const logs = [] as any[]
    try{
      result = await UserService.getUserLogs({ user: id}, pagination, sort)
      pagination.total = await UserService.getUserLogsCount({ user: id})
      if(pagination.total%pagination.limit){
        pagination.total_page = Math.floor(pagination.total/pagination.limit) + 1
      }else{
        pagination.total_page = Math.floor(pagination.total/pagination.limit)
      }
      result.map((el:any) => {
        logs.push({
          _id: el._id,
          user: el.user,
          type: el.type,
          params: el.params,
          data: el.data,
          created_at: el.createdAt,
        })
      })
    }
    catch(error){
      return res.badRequest(new Error(error))
    }
    finally{
      return res.respondWithData(logs, pagination)
    }
  }

  export async function getFolders(
    req: Request,
    res: Response
  ):Promise<any> {
    const { id } = req.params
    const { pagination, sort } = req
    let result
    const folders = [] as any[]
    try{
      result = await FolderService.getFoldersWithPagination( id, pagination, sort)
      pagination.total = await FolderService.getFoldersCount(id)
      if(pagination.total%pagination.limit){
        pagination.total_page = Math.floor(pagination.total/pagination.limit) + 1
      }else{
        pagination.total_page = Math.floor(pagination.total/pagination.limit)
      }
      result.map((el:any) => {
        let count = 0
        Object.values(el.items).map((ll: any) => {
          count += ll.length
        })
        folders.push({
          _id: el._id,
          user: el.user,
          name: el.name,
          color: el.color,
          itemCount: count,
          createdAt: el.createdAt,
        })
      })
    }
    catch(error){
      return res.badRequest(new Error(error))
    }
    finally{
      return res.respondWithData(folders, pagination)
    }
  }

  export async function getFolderItems(
    req: Request,
    res: Response
  ):Promise<any> {
    const { id } = req.params
    const { pagination } = req
    let existFolder, total = 0, result = []
    try{
      existFolder = await FolderService.getFolder({_id: new Types.ObjectId(id)}) as any
      if(!existFolder){
        throw "Folder олдсонгүй"
      }
      const artwork = await ArtworkItemService.getItemsByIds(existFolder.items.artwork, req.query)
      const koha = await KOHABiblioService.getBiblioWithIds(existFolder.items.koha, pagination)
      Object.values(existFolder.items).forEach((value:any) => {
          total = total+value.length
        }
      )
      pagination.total = total
      const merged = artwork.data.concat(koha.biblio)
      result = SortHelper.sortByLetter(merged, "name", 1)
    }
    catch(error){
      res.badRequest(new Error(error))
    }
    finally{
      const params = { ...req.params, ...req.query}
      const logdata: IUserLogCreate = {
      user: req.user.claims._id,
      system: 'admin',
      type: 'ViewFolder',
      address: req.ip || req.socket.remoteAddress,
      user_agent: req.headers['user-agent'],
      params: JSON.stringify(params),
      data: JSON.stringify(req.body)
      }
      UserService.addLog(logdata)
      return res.respondWithData({
        folder: {
          _id: existFolder._id,
          name:existFolder.name,
          color:existFolder.color,
          createdAt: existFolder.createdAt
        },
        items: result
      }, pagination)
    }
  }

  export async function editUser(req: Request, res: Response): Promise<any> {
    const { user } = req
    const id = req.params.id;
    

    let result
    try {
        const { email, verified, img, firstname, lastname, activeStatus } = req.body  // add password with decode
        const existUsers = await UserService.getUserById(
            new Types.ObjectId(id)
        )
        if(!existUsers){
          throw "Хэрэглэгч олдсонгүй"
        }
        const data: IUpdateUser = {
            email,
            verified,
            firstname,
            lastname,
            img,
            activeStatus,
        }
        result = await UserService.updateUser(
            new Types.ObjectId(id),
            data
        ) as any
    } catch (error) {
        res.internalError(new Error(error))
    } finally {
        const params = { ...req.params, ...req.query}
        const logdata: IUserLogCreate = {
        user: user.claims._id,
        system: 'admin',
        type: 'EditUser',
        address: req.ip || req.socket.remoteAddress,
        user_agent: req.headers['user-agent'],
        params: JSON.stringify(params),
         data: JSON.stringify(req.body)
        }
        UserService.addLog(logdata)
        return res.respondWithData({
            _id: result._id,
            email: result.email,
            firstname: result.firstname || "",
            lastname: result.lastname || "",
            role: result.role,
            verified: result.verified,
            isForeigner: result.isForeigner,
            country: result.country,
            img: result.img,
            activeStatus: result.activeStatus,
            createdAt: result.createdAt,
            updatedAt:result.updatedAt
        })
    }
}