import { Invoice } from '@root/models'
import { FetchService, LibraryKohaService, UserService } from '@root/services'
import InvoiceService from '@root/services/payment/invoice.service'
import { Types } from 'mongoose'
import { Request, Response } from 'express'
import config from '@root/config'

export async function me(req: Request, res: Response): Promise<void> {
    const { user } = req
    const existUser = await UserService.getUserById(
        new Types.ObjectId(user.claims._id)
    )
    res.respondWithData({
        email: existUser.email,
        firstname: existUser.firstname || null,
        lastname: existUser.lastname || null,
        img: existUser.img,
        age: existUser.age || null,
        isForeigner: existUser.isForeigner,
        country: existUser.country
    })
}

export async function paymentWebhook(
    req: Request,
    res: Response
): Promise<void> {
    const { invoice } = req.body

    const { billNumber } = invoice

    console.log(billNumber)

    const data = await InvoiceService.getInvoice({
        billNumber
    })

    const result = await Invoice.findByIdAndUpdate(data._id, {
        $set: {
            isPaid: true
        }
    })

    res.respondWithData(result)
}