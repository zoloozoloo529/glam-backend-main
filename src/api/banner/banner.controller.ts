import { Request, Response } from 'express'
import BannerService from '@root/services/banner.service'

export async function getBanners(req: Request, res: Response): Promise<void> {
    try {
        const result = await BannerService.getBannersPublic()
        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}