import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { getBanners } from './banner.controller'

const BannerRouter = Router()

BannerRouter.get('/', asyncHandler(getBanners))

export default BannerRouter
