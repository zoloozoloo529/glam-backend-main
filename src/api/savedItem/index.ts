import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { createSavedItem, getSavedItems, getSavedItemById, deleteSavedItem, checkSaved } from './savedItem.controller'
import { PaginationMiddleware, authenticationMiddleware, sort } from '@root/middlewares'

const SavedItemRouter = Router()

SavedItemRouter.post('/', authenticationMiddleware.customMiddleware, asyncHandler(createSavedItem))
SavedItemRouter.get('/', authenticationMiddleware.customMiddleware, PaginationMiddleware.paginate, sort.sort, asyncHandler(getSavedItems))
SavedItemRouter.get('/:id', authenticationMiddleware.customMiddleware, asyncHandler(getSavedItemById))
SavedItemRouter.delete('/:id', authenticationMiddleware.customMiddleware, asyncHandler(deleteSavedItem))
SavedItemRouter.get('/check/:id', authenticationMiddleware.customMiddleware, asyncHandler(checkSaved))

export default SavedItemRouter