import { Request, Response } from 'express'
import SavedItemService from '@root/services/savedItem.service'
import { ISavedItemCreate } from '@root/interfaces'
import { Types } from 'mongoose'
import { ViewService } from '@root/services'

export async function createSavedItem(req: Request, res: Response): Promise<any> {
    const { type, item_id, item_name, main_img, heritage_type} = req.body
    const { user } = req
    let data: ISavedItemCreate
    try {
        data = {
            user_id : user.claims._id,
            type,
            item_id,
            item_name,
            main_img,
            heritage_type
        }
        const check = await SavedItemService.checkExisting({"item_id":item_id,"user_id":user.claims._id})
        if(check[0]){
            return res.respondWithError(new Error("Хадгалагдсан байна."), 500)
        }
        const update = await ViewService.addSaveCount(item_id)
        const result = await SavedItemService.createSavedItem(data)
        res.respondWithData(result)
    } catch (error) {
        return res.respondWithError(new Error("Хадгалахад алдаа гарлаа."), 500)
    }
}

export async function getSavedItems(req: Request, res: Response): Promise<any> {
    const { user } = req
    const { pagination, sort} = req
    const { type, item_id } = req.query
    let search = req.query.search as string
    let filter = {}
    if (!search) {
        search = ""
    }
    if(type){
        filter = Object.assign(filter, {"type":type})
    }
    if(item_id){
        filter = Object.assign(filter, {"item_id":item_id})
    }
    filter = Object.assign(filter,{"user_id":user.claims._id})
    const result = await SavedItemService.getSavedItems(filter, search, pagination, sort)
    pagination.total = await SavedItemService.getSavedItemsCount(search,filter)
    res.respondWithData(result,pagination)    
}

export async function getSavedItemById(req: Request, res: Response): Promise<any> {
    const { id } = req.params
    try {
        const result = await SavedItemService.getSavedItemById({
            _id: new Types.ObjectId(id)
        })
        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}

export async function deleteSavedItem(req: Request, res: Response): Promise<any> {
    const { id } = req.params
    try {
        const result = await SavedItemService.deleteSavedItem({
            _id: new Types.ObjectId(id)
        })
        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}

export async function checkSaved(req: Request, res: Response): Promise<any> {
    const { id }  = req.params
    const { user } = req
    try {
        if(!user){
            res.respondWithData
        }
        const result = await SavedItemService.checkExisting({"user_id":user.claims._id,"item_id":id})
        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}