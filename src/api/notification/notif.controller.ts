import * as admin from 'firebase-admin'
import { Request, Response } from 'express'
// import { ServiceAccount } from 'firebase-admin'
// import { SQS } from 'aws-sdk'
import { INotificationCreate, INotificationUpdate } from '@root/interfaces'
import NotificationService from '@root/services/notif.service'
import { Types } from 'mongoose'
import { UserService } from '@root/services'

// const sqs = new SQS()

// const serviceAccount: ServiceAccount = {
//     projectId: '',
//     privateKey: '',
//     clientEmail: ''
// }

// admin.initializeApp({
//     credential: admin.credential.cert(serviceAccount)
// })

export async function sendNotif(req: Request, res: Response): Promise<void> {
    const { tokens } = req.body
    for (const el of tokens) {
        const res = await admin.messaging().send(el)
        console.log('res: ', res)
    }
}

export const sendBulkNotification = async (
    req: Request,
    res: Response
): Promise<void> => {
    const { id } = req.params
    const { notifId } = req.params
    const Notif = await NotificationService.findById(new Types.ObjectId(id))
    const users = await UserService.getUsersWithFilter({
        distinct: 'fcm_token'
    })

    if (!Notif) {
        res.internalError(new Error('Notification not found'))
    }

    const tokens = users.map((user) => user.fcm_token)

    // for (let i = 0; i < users.length; i += 200) {
    //     while (tokens.length > 0) tokens.pop()
    //     for (let j = i; j < Math.min(i + 200, users.length); j++) {
    //         // tokens.push(users[j])
    //     }
    //     if (tokens.length > 0) {
    //         await sqs
    //             .sendMessage({
    //                 QueueUrl: process.env.NOTIF_QUEUE_URL,
    //                 MessageBody: JSON.stringify({
    //                     notification: Notif,
    //                     tokens,
    //                     notifId
    //                 })
    //             })
    //             .promise()
    //     }
    // }
}

export async function createNotif(req: Request, res: Response): Promise<void> {
    const { user } = req
    const { title, body, channels, attributes } = req.body

    const notificationAttributes = {
        ...attributes
    }
    const notifAttributes: INotificationCreate = {
        createdBy: user.claims._id,
        updatedBy: user.claims._id,
        title: title,
        body: body,
        channels: channels,
        attributes: notificationAttributes
    }
    try {
        const result = await NotificationService.create({
            ...notifAttributes
        })
        res.respondWithData(result)
    } catch (error) {
        console.error('Error creating notification:', error)
        res.internalError(new Error(error))
    }
}

export const updateNotif = async (req: Request, res: Response) => {
    const { user } = req
    const { id } = req.params
    const { title, body, channels, attributes } = req.body

    const notificationAttributes = {
        ...attributes
    }
    const notifAttributes: INotificationUpdate = {
        updatedBy: user.claims._id,
        title: title,
        body: body,
        channels: channels,
        attributes: notificationAttributes
    }
    try {
        const result = await NotificationService.findByIdAndUpdate(
            new Types.ObjectId(id),
            {
                ...notifAttributes
            }
        )
        if (!result) {
            res.internalError(new Error('Notification not found'))
        }
        res.respondWithData(result)
    } catch (error) {
        console.error('Error updating notification:', error)
        res.internalError(new Error(error))
    }
}
