import { Router } from 'express'
import asyncHandler from 'express-async-handler'

import { createNotif, updateNotif } from './notif.controller'

const NotificationRouter = Router()

NotificationRouter.post('/', asyncHandler(createNotif))
NotificationRouter.patch('/:id', asyncHandler(updateNotif))

export default NotificationRouter
