import { Request, Response } from 'express'
import { ILocationCreate } from '@root/interfaces'
import { HeritageService, LocationService } from '@root/services'
import { Types } from 'mongoose'

export async function createLocation (
    req: Request,
    res: Response
): Promise<void> {
    const { latitude, longitude, name, type, image, address, timetable, description } = req.body
    let data : ILocationCreate
    try {
        data = {
            latitude,
            longitude,
            name,
            description,
            type,
            image,
            address,
            timetable
        }
        const result = await LocationService.createLocation(data)
         res.respondWithData(result)
    } catch (error) {
        res.badRequest(new Error(error))
    }
}

export async function getLocations (
    req: Request,
    res: Response
): Promise<void>{
    try {
        const updateLocations = await HeritageService.getImmovableHeritages()
    for (let i = 0; i < updateLocations.total; i++) {
        const checkDuplicate = await LocationService.getLocation({"heritage_id":updateLocations.data[i].id})
        if(!checkDuplicate){
            const data : ILocationCreate = {
                latitude : updateLocations.data[i].latitude,
                longitude : updateLocations.data[i].longitude,
                name : updateLocations.data[i].heritage_name,
                description : updateLocations.data[i].heritage_description,
                type : "immovable heritage",
                image : updateLocations.data[i].mainImg,
                address : updateLocations.data[i].province + `, ` + updateLocations.data[i].region,
                protection : updateLocations.data[i].protection_priority,
                heritage_id: updateLocations.data[i].id
            }
        const update = await LocationService.createLocation(data)
        }
    }    
    } catch (error) {
        console.log("***** Heritage Service Problem *****")
    }
    
    const { pagination, sort } = req
    let filter = req.query as any
    try {
        let search = req.query.search as string
        if (!search) {
            search = ''
        }
        const typeFilter = filter.type
        filter = {
            ...filter,
            type: {
                $in: typeFilter ? typeFilter.split(',') : ['museum', 'library', 'theatre', 'artwork', 'immovable heritage']
            }
        }
        const result = await LocationService.getLocations(
            search,
            filter,
            pagination,
            sort
        )
        pagination.total = await LocationService.getLocationCount(
            search,
            filter

        )
         res.respondWithData(result, pagination)
    } catch (error) {
         res.internalError(new Error(error))
    }
}

export async function getLocation (
    req: Request,
    res: Response
): Promise<void> {
    try {
        const { id } = req.params
        const result = await LocationService.getLocation({
            _id: new Types.ObjectId(id)
        })
         res.respondWithData(result)
    } catch (error) {
         res.internalError(new Error(error))
    }
}

export async function updateLocation (
    req: Request,
    res: Response
): Promise<void> {
    try {
        const data = req.body
        const id = req.params.id
        const result = await LocationService.updateLocation(
            new Types.ObjectId(id),
            data
        )
         res.respondWithData(result)
    } catch (error) {
         res.internalError(new Error(error))
    }
}

export async function deleteLocation (
    req: Request,
    res: Response
): Promise<void> {
    try {
        const { id } = req.params
        const result = await LocationService.deleteLocation(new Types.ObjectId(id))
         res.respondWithData(result)
    } catch (error) {
         res.internalError(new Error(error))
    }
}