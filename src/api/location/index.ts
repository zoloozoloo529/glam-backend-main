import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { createLocation, updateLocation, getLocations, deleteLocation, getLocation } from './location.controller'
import { PaginationMiddleware, sort } from '@root/middlewares'

const LocationRouter = Router()

LocationRouter.post('/',  asyncHandler(createLocation))
LocationRouter.patch('/:id', asyncHandler(updateLocation))
LocationRouter.delete('/:id', asyncHandler(deleteLocation))
LocationRouter.get('/', PaginationMiddleware.paginate, sort.sort, asyncHandler(getLocations))
LocationRouter.get('/:id', PaginationMiddleware.paginate, asyncHandler(getLocation))

// LocationRouter.post('/:id/items', authenticationMiddleware.checkJwt, authenticationMiddleware.checkVerification, LocationMiddleware.validateUserPermission, bookMiddleware.updateSaveCount, asyncHandler(addItemsToLocation))

export default LocationRouter