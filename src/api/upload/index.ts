import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { uploadImage } from './upload.controller'

const UploadRouter = Router()

// UploadRouter.post('/:type', asyncHandler(uploadHandler))
UploadRouter.post('/', asyncHandler(uploadImage))

export default UploadRouter
