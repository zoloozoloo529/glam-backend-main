import { Request, Response } from 'express'
import config from '@root/config'

export async function uploadImage(req: Request, res: Response): Promise<any> {
  const image = req.files
  const imgString = JSON.stringify(image)
  const img = JSON.parse(imgString) //workaround 
  const imageUrl = config.imageStatic + '/static/' + img[0].filename
  return res.respondWithData({
      imageUrl
  })
}