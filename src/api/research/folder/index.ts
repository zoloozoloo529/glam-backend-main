import { Router } from 'express'
import asyncHandler from 'express-async-handler'

import { createFolder, updateFolder, folderInvite, getFolders, getFolder, deleteFolder, respondToInvite, leaveFromFolder } from './folder.controller'

const ResearchFolderRouter = Router()

ResearchFolderRouter.get('/', asyncHandler(getFolders))
ResearchFolderRouter.post('/', asyncHandler(createFolder))
ResearchFolderRouter.get('/:id', asyncHandler(getFolder))
ResearchFolderRouter.patch('/:id', asyncHandler(updateFolder))
ResearchFolderRouter.delete('/:id', asyncHandler(deleteFolder))
ResearchFolderRouter.post('/:id/invite', asyncHandler(folderInvite))
ResearchFolderRouter.post('/:id/respond', asyncHandler(respondToInvite))
ResearchFolderRouter.get('/:id/leave', asyncHandler(leaveFromFolder))


export default ResearchFolderRouter