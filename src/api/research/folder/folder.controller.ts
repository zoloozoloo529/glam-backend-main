import { Request, Response } from 'express';
import { Types } from 'mongoose';
import { ResearchFolderService, UserService } from '@root/services';
import { IResearchFolderAddMember } from '@root/interfaces';

export async function createFolder(req: Request, res: Response): Promise<void> {
    const { user } = req;
    const { title } = req.body;
    console.log('user', user);
    const existUser = await UserService.getUserById(
        new Types.ObjectId(user.claims._id)
    )
    console.log('existingUser', existUser);
    try {
        const result = await ResearchFolderService.create({
            title,
            user: existUser._id,
            folderOwner: {
                _id: existUser._id,
                email: existUser.email,
                firstname: existUser.firstname,
                lastname: existUser.lastname,
                img: existUser.img,
            },
        });
        res.respondWithData(result);
    } catch (error) {
        console.error('Error creating research folder:', error);
        res.internalError(new Error(error));
    }
}

export async function updateFolder(req: Request, res: Response): Promise<void> {
    const { user } = req;
    const { id } = req.params;
    const data = req.body;

    try {
        const result = await ResearchFolderService.update(
            new Types.ObjectId(id),
            {
                ...data
            }
        );
        if (!result) {
            res.internalError(new Error('Research folder not found'));
        }
        res.respondWithData(result);
    } catch (error) {
        console.error('Error updating research folder:', error);
        res.internalError(new Error(error));
    }
}


export async function getFolder(req: Request, res: Response): Promise<void> {
    const { user } = req;
    const { id } = req.params;

    try {
        const result = await ResearchFolderService.find({ _id: new Types.ObjectId(id), user: new Types.ObjectId(user.claims._id) });

        if (!result) {
            res.internalError(new Error('Research folder not found'));
        }
        res.respondWithData(result);
    } catch (error) {
        console.error('Error getting research folder:', error);
        res.internalError(new Error(error));
    }
}

export async function getFolders(req: Request, res: Response): Promise<void> {
    const { user } = req;

    try {
        const result = await ResearchFolderService.find({ user: new Types.ObjectId(user.claims._id) });

        if (!result) {
            res.internalError(new Error('Research folder not found'));
        }
        res.respondWithData(result);
    } catch (error) {
        console.error('Error getting research folder:', error);
        res.internalError(new Error(error));
    }
}

export async function deleteFolder(req: Request, res: Response): Promise<void> {
    const { user } = req;
    const { id } = req.params;

    try {
        const folder = await ResearchFolderService.findOne({ _id: new Types.ObjectId(id), user: new Types.ObjectId(user.claims._id) });

        if (!folder) {
            res.internalError(new Error('Folder not found'));
        }

        const result = await ResearchFolderService.update(
            new Types.ObjectId(id),
            {
                status: 0
            }
        );
        if (!result) {
            res.internalError(new Error('Research folder not found'));
        }
        res.respondWithData(result);
    } catch (error) {
        console.error('Error deleting research folder:', error);
        res.internalError(new Error(error));
    }
}


export async function folderInvite(req: Request, res: Response): Promise<void> {
    const { user } = req;
    const { id } = req.params;
    const { email } = req.body;


    try {
        const invitedUser = await UserService.getUser({ email: email });
        console.log('invitedUser', invitedUser);

        if (!invitedUser) {
            res.internalError(new Error('Хэрэглэгчийн бүртгэл олдсонгүй.'));
        }

        const folder = await ResearchFolderService.findOne({ _id: new Types.ObjectId(id), user: new Types.ObjectId(user.claims._id) });


        if (!folder) {
            res.internalError(new Error('Research folder not found'));
        }

        if (folder.invitedMembers.includes(invitedUser._id)) {
            res.internalError(new Error('User already invited'));
        }

        const data: IResearchFolderAddMember = {
            isShared: true,
            invitedMembers: [
                ...folder.invitedMembers,
                {
                    _id: invitedUser._id,
                }]
        };
        const result = await ResearchFolderService.inviteUser(
            new Types.ObjectId(id), data
        );

        res.respondWithData(result);
    } catch (error) {
        console.error('Error inviting person to research folder:', error);
        res.internalError(new Error(error));
    }
}

export async function respondToInvite(req: Request, res: Response): Promise<void> {
    const { user } = req;
    const { id } = req.params;
    const { isAccepted } = req.body;

    try {
        const folderId = new Types.ObjectId(id);
        const userId = new Types.ObjectId(user.claims._id);

        const folder = await ResearchFolderService.findOne({ _id: folderId, invitedMembers: { $in: [userId] } });

        if (!folder) {
            res.internalError(new Error('Research folder not found'));
        }

        let data;

        if (isAccepted) {
            // Add user to folderMembers
            const newMember = {
                _id: user.claims._id,
                firstname: user.claims.firstname,
                lastname: user.claims.lastname,
                email: user.claims.email,
                img: user.claims.img,
            };

            data = {
                inviteAccepted: true,
                folderMembers: [...folder.folderMembers, newMember],
            };
        } else {
            // Remove user from invitedMembers
            const updatedInvitedMembers = folder.invitedMembers.filter(item => item._id !== userId);
            data = { invitedMembers: updatedInvitedMembers };
        }

        const result = await ResearchFolderService.update(folderId, data);
        res.respondWithData(result);
    } catch (error) {
        console.error('Error responding to invite for research folder:', error);
        res.internalError(error);
    }
}

export async function leaveFromFolder(req: Request, res: Response): Promise<void> {
    const { user } = req;
    const { id } = req.params;

    try {
        const folderId = new Types.ObjectId(id);
        const userId = new Types.ObjectId(user.claims._id);

        const folder = await ResearchFolderService.findOne({
            _id: folderId,
            folderMembers: { $elemMatch: { _id: userId } }
        });

        console.log('folderMembers:', folder.folderMembers);

        if (!folder) {
            res.internalError(new Error('Research folder not found'));
        }

        const updatedFolderMembers = folder.folderMembers.filter(item => !item._id.equals(userId));

        const data = { folderMembers: updatedFolderMembers };

        const result = await ResearchFolderService.update(folderId, data);
        res.respondWithData(result);
    } catch (error) {
        console.error('Error leaving from research folder:', error);
        res.internalError(error);
    }
}