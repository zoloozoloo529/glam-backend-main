import { Request, Response } from 'express'
import { ICreateResearch } from '@root/interfaces'
import { ResearchService } from '@root/services'
import { Types } from 'mongoose'


export async function createResearch(req: Request, res: Response): Promise<void> {
    const { user } = req
    const {
        title,
        content_type,
        content_id,
        type,
        reference,
        page_number,
        edition,
        publisher,
        locale,
        settings,
        note
    } = req.body


    let researchData: ICreateResearch;
    try {
        switch (type) {
            case 'note':

                researchData = {
                    user: user.claims._id,
                    title,
                    pageNumber: page_number,
                    edition,
                    contentItemId: content_id,
                    contentItemType: content_type,
                    type,
                    publisher,
                    locale,

                }
                break;

            case 'master_note':
                researchData =
                {
                    user: user.claims._id,
                    type,
                    contentItemId: content_id,
                    contentItemType: content_type,
                    note
                }
                break;

            case 'quote':
                researchData = {
                    user: user.claims._id,
                    pageNumber: page_number,
                    edition,
                    type,
                    contentItemId: content_id,
                    contentItemType: content_type,
                    publisher,
                    locale,

                }
                break;

            default:
                throw new Error("Invalid research type");
        }
        researchData.settings = settings;
        researchData.reference = reference;

        console.log('researchData:', researchData);
        const result = await ResearchService.create(researchData)

        res.respondWithData(result)
    } catch (error) {
        console.error('Error creating research:', error)
        res.internalError(new Error(error))
    }
}

export async function updateResearch(req: Request, res: Response): Promise<void> {
    const { user } = req
    const { id } = req.params
    const {
        title,
        content_type,
        content_id,
        type,
        reference,
        page_number,
        edition,
        publisher,
        locale,
        settings,
        folderId,
        note
    } = req.body

    let researchData: ICreateResearch;
    try {
        switch (type) {
            case 'note':
                researchData = {
                    user: user.claims._id,
                    title,
                    pageNumber: page_number,
                    edition,
                    contentItemId: content_id,
                    contentItemType: content_type,
                    type,
                    publisher,
                    locale,

                }
                break;

            case 'master_note':
                researchData =
                {
                    user: user.claims._id,
                    type,
                    contentItemId: content_id,
                    contentItemType: content_type,
                    note
                }
                break;

            case 'quote':
                researchData = {
                    user: user.claims._id,
                    pageNumber: page_number,
                    edition,
                    type,
                    contentItemId: content_id,
                    contentItemType: content_type,
                    publisher,
                    locale,

                }
                break;

            default:
                throw new Error("Invalid research type");
        }
        researchData.settings = settings;
        researchData.reference = reference;

        if (folderId) {
            researchData.folderId = new Types.ObjectId(folderId);
        }

        console.log('researchData:', researchData);
        const result = await ResearchService.update(
            new Types.ObjectId(id),
            {
                ...researchData
            }
        )
        if (!result) {
            res.internalError(new Error('Research not found'))
        }
        res.respondWithData(result)
    } catch (error) {
        console.error('Error updating research:', error)
        res.internalError(new Error(error))
    }

}

export async function getResearchList(req: Request, res: Response): Promise<void> {
    const { user, query } = req;
    const { content_id } = query;

    try {
        const filter = {
            user: user.claims._id,
            contentItemId: content_id,
            ...query,
        };
        const result = await ResearchService.find({
            ...filter
        })

        res.respondWithData(result)
    } catch (error) {
        console.error('Error getting research list:', error)
        res.internalError(new Error(error))
    }
}

export async function deleteResearch(req: Request, res: Response): Promise<void> {
    const { id } = req.params
    try {
        const result = await ResearchService.delete(new Types.ObjectId(id))
        if (!result) {
            res.internalError(new Error('Research not found'))
        }
        res.respondWithData(result)
    } catch (error) {
        console.error('Error deleting research:', error)
        res.internalError(new Error(error))
    }
}
