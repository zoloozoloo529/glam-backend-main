import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { createResearch, deleteResearch, getResearchList, updateResearch } from './research.controller'

const ResearchRouter = Router()

ResearchRouter.get('/', asyncHandler(getResearchList))
ResearchRouter.post('/', asyncHandler(createResearch))
ResearchRouter.put('/:id', asyncHandler(updateResearch))
ResearchRouter.delete('/:id', asyncHandler(deleteResearch))
export default ResearchRouter