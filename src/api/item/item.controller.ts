import { IAddItems, ICreateSearchLog, IPagination, ISearchBody, ISearchFilter, IUserLogCreate } from '@root/interfaces'
import { KohaItem } from '@root/models'
import { ArchiveService, ArtworkItemService, FolderService, HeritageService, KOHABiblioService, LibraryBookService, LibraryKohaService, LogService, UserService, ViewService } from '@root/services'
import { SortHelper } from '@root/utils'
import { Request, Response } from 'express'
import { Types } from 'mongoose'

export async function getItems(
  req: Request,
  res: Response
): Promise<any> {
  const { pagination, user } = req
  const sort = req.sort || { name: 1 }
  const type = req.query.type as string || 'artwork,koha,archive,heritage'
  const types = type.split(',') 
  const defaultFilter = req.query as any
  // let filter: ISearchFilter
  // types.length > 1 ? filter = {} : filter
  const filter = types.length > 1 ? {} : req.body[`${types[0]}`]

  delete defaultFilter.limit 
  defaultFilter.page = pagination.page
  defaultFilter.limit = Math.floor(pagination.limit/types.length)


  // let artworkResult, kohaResult, eduResult
  pagination.total = 0
  const data: any[] = [] 
  const promises = []
  try {
    if(types.includes('artwork')) {
      promises.push(await ArtworkItemService.getItems(defaultFilter))
      // artworkResult = await ArtworkItemService.getItems(defaultFilter)
      // pagination.total += artworkResult.pagination.total
      // pagination.artwork = artworkResult.pagination.total
      // data.push(...artworkResult.data)
    }
    if(types.includes('education')) {
      promises.push(ArtworkItemService.getEduItems(defaultFilter))
    }
    if(types.includes('koha')) {
      promises.push(LibraryKohaService.getKohaBooks(defaultFilter, filter))
    }
    if(types.includes('heritage')) {
      promises.push(HeritageService.getHeritages(defaultFilter))
    }
    const result = await Promise.all(promises)
    result.map((el:any) => {
      data.push(...el.data)
      pagination.total += el.pagination.total
      switch (el.type){
        case "artwork":
          pagination.artwork = el.pagination.total;
          break;
        case "archive":
          pagination.archive = el.pagination.total;
          break;
        case "koha":
          pagination.koha = el.pagination.total;
          break;
        case "heritage":
          pagination.heritage = el.pagination.total;
          break;
      }
    })
    if(user == "anonymous"){
      data.map((el:any) => {
        el.isSaved = false
      })
    }else{
      const defaultFolder = await FolderService.getFolder({user:user.claims._id, type: "all"})
      data.map((el:any) => {
        switch (el.type){
          case "artwork":
            el.isSaved = defaultFolder?.items.artwork.indexOf(el._id) > -1;
            break;
          case "education":
            el.isSaved = defaultFolder?.items.artwork.indexOf(el._id) > -1;    /// change items-type after changed folder
            break;
          case "koha":
            el.isSaved = defaultFolder?.items.koha.indexOf(el._id) > -1;
            break;
          case "heritage":
            el.isSaved = defaultFolder?.items.heritage.indexOf(el._id) > -1;
            break;
        }
      })
    }
  }
  catch(error) {
    return res.internalError(new Error(error))
  }
  finally{
    const params = { ...req.params, ...req.query}
    const logdata: IUserLogCreate = {
      user: user !== "anonymous" ? user.claims._id : "anonymous",
      type: 'Search',
      address: req.ip || req.socket.remoteAddress,
      user_agent: req.headers['user-agent'],
      params: JSON.stringify(params),
      data: JSON.stringify(req.body)
    }
    UserService.addLog(logdata)
    const finalData = types.length > 1 ? SortHelper.sortByLetter(data, "name", 1) : data
    return res.respondWithData(finalData, pagination)
  }
}

export async function searchItems(
  req: Request,
  res: Response
): Promise<any> {
  const { user } = req
  const searchBody:ISearchBody = req.body
  const cache = searchBody.cache || true
  const type = req.body.type as string || 'artwork,koha,archive,heritage,library'
  const types = type.split(',')
  const sortOrder = searchBody.sort_order === "asc" ? "" : "-"
  const sort = sortOrder + searchBody.sort_by as string
  const defaultFilter = {
    search: searchBody.search || "",
    page: searchBody.page || 1,
    limit : Math.floor(searchBody.limit/types.length) || 10,
    sort_by: searchBody.sort_by || 'name',
    sort_order: searchBody.sort_order || 'desc'
  }
  const pagination:IPagination = {
    limit : Math.floor(searchBody.limit/types.length) || 10,
    page : searchBody.page || 1,
    offset : searchBody.limit * (searchBody.page - 1) || 0,
    total : 0
  }
  // pagination.total = 0
  const data: any[] = [] 
  const promises = []
  try {
    if(types.includes('artwork')) {
      let artworkFilter = {
        search:searchBody.search || "",
        sort,
        page:searchBody.page,
        limit:Math.floor(searchBody.limit/types.length) || 10,
      }
      artworkFilter = {...artworkFilter, ...searchBody.filter?.artwork}
      promises.push(ArtworkItemService.getItems(artworkFilter))
    }
    if(types.includes('koha')) {
      cache ? promises.push(LibraryKohaService.getKohaFromCache(searchBody.search || "", pagination, sort, searchBody?.filter?.koha))
            : promises.push(LibraryKohaService.getKohaBooks(defaultFilter, searchBody?.filter?.koha))
    }
    if(types.includes('heritage')) {
      promises.push(HeritageService.getHeritages(defaultFilter, searchBody?.filter?.heritage))
    }
    if(types.includes('archive')) {
      promises.push(ArchiveService.getArchives(defaultFilter, searchBody?.filter?.archive))
    }
    if(types.includes('library')) {
      promises.push(LibraryBookService.searchBooks(defaultFilter, searchBody?.filter?.library))
    }
    const result = await Promise.all(promises)
    result.map((el:any) => {
      data.push(...el.data)
      pagination.total += el.pagination.total
      switch (el.type){
        case "artwork":
          pagination.artwork = el.pagination.total;
          break;
        case "archive":
          pagination.archive = el.pagination.total;
          break;
        case "koha":
          pagination.koha = el.pagination.total;
          break;
        case "heritage":
          pagination.heritage = el.pagination.total;
          break;
        case "library":
          pagination.library = el.pagination.total;
          break;
      }
    })
    if(user == "anonymous"){
      data.map((el:any) => {
        el.isSaved = false
      })
    }else{
      const defaultFolder = await FolderService.getFolder({user:user.claims._id, type: "all"})
      data.map((el:any) => {
        switch (el.type){
          case "artwork":
            el.isSaved = defaultFolder.items.artwork.indexOf(el._id) > -1;
            break;
          case "education":
            el.isSaved = defaultFolder.items.artwork.indexOf(el._id) > -1;    /// have to change items-type after changed folder
            break;
          case "koha":
            el.isSaved = defaultFolder.items.koha.indexOf(el._id) > -1;
            break;
          case "heritage":
            el.isSaved = defaultFolder.items.heritage.indexOf(el._id) > -1;
            break;
          case "library":
            if(defaultFolder.items.library){
              el.isSaved = defaultFolder?.items?.library.indexOf(el._id) > -1;
            }
            break;
        }
      })
    }
  }
  catch(error) {
    return res.internalError(new Error(error))
  }
  finally{
    const params = { ...req.params, ...req.query}
    const logdata: IUserLogCreate = {
      user: user !== "anonymous" ? user.claims._id : "anonymous",
      type: 'Search',
      address: req.ip || req.socket.remoteAddress,
      user_agent: req.headers['user-agent'],
      params: JSON.stringify(params),
      data: JSON.stringify(req.body)
    }
    UserService.addLog(logdata)
    const finalData = types.length > 1 ? SortHelper.sortByLetter(data, "name", searchBody.sort_order === "asc" ? 1 : 0) : data
    return res.respondWithData(finalData, pagination)
  }
}

export async function getItemById(
  req: Request,
  res: Response
): Promise<any> {
  const { user } = req
  const { id, type } = req.params
  let heritage_type
  if(req.query.heritage_type){
    heritage_type = req.query.heritage_type as string
  }
  let item
  try {
    switch (type) {
        case 'artwork': {
            const resultArtwork = await ArtworkItemService.getItem(id)
            if (!resultArtwork) {
                throw 'Бүтээл олдсонгүй.'
            }
            item = resultArtwork
            if (user !== 'anonymous') {
                const defaultFolder  = await FolderService.getFolder({user: user.claims._id, type: "all"})
                item.isSaved = defaultFolder.items.artwork.indexOf(id) > -1
            } else {
                item.isSaved = false
            }
            break
          }
        case 'archive': {
            const resultArchive = await HeritageService.getHeritageById(id,'documentary')
            if (resultArchive.data.length === 0) {
              throw 'Бүтээл олдсонгүй.'
            }
            item = resultArchive.data
            item.type = 'archive'
            console.log("🚀 ~ file: item.controller.ts:252 ~ item:", item)
            if (user !== 'anonymous') {
                const defaultFolder  = await FolderService.getFolder({user: user.claims._id, type: "all"})
                item.isSaved = defaultFolder.items.artwork.indexOf(id) > -1
            } else {
                item.isSaved = false
            } 
            break
          }
        case 'koha': {
            const resultKoha = await LibraryKohaService.getKohaItemsWithBiblios([id], {
              limit: 1,
              page: 1
            })
            if (resultKoha.data.length === 0) {
                throw 'Бүтээл олдсонгүй.'
            }
            item = resultKoha.data[0]
            if (user !== 'anonymous') {
                const defaultFolder  = await FolderService.getFolder({user: user.claims._id, type: "all"})
                item.isSaved = defaultFolder.items.koha.indexOf(id) > -1
            } else {
                item.isSaved = false
            }
            break
          }
        case 'heritage': {
            const resultHeritage = await HeritageService.getHeritageById(id,heritage_type)
            if (resultHeritage.data.length === 0) {
                throw 'Бүтээл олдсонгүй.'
            }
            item = resultHeritage.data
            if (user !== 'anonymous') {
                const defaultFolder  = await FolderService.getFolder({user: user.claims._id, type: "all"})
                item.isSaved = defaultFolder.items.artwork.indexOf(id) > -1
            } else {
                item.isSaved = false
            } 
            break
          }
    }

    const viewStatus =  await ViewService.getViewCount(id)
    console.log("🚀 ~ viewStatus:", viewStatus)
    item.viewCount = viewStatus.viewCount
    item.saveCount = viewStatus.saveCount

    if(type == 'koha'){
      const result = await LibraryKohaService.getKohaItemsWithBiblios([id], {
        limit: 1,
        page: 1
      })
      if(result.data.length === 0){
        throw "Бүтээл олдсонгүй."
      }
      item = result.data[0]
    }

  }
  catch(error) {
    return res.badRequest(new Error(error))
  }
  finally{
    const params = { ...req.params, ...req.query}
    const logdata: IUserLogCreate = {
      user: user !== "anonymous" ? user.claims._id : "anonymous",
      type: 'ViewItem',
      address: req.ip || req.socket.remoteAddress,
      user_agent: req.headers['user-agent'],
      params: JSON.stringify(params),
      data: JSON.stringify(req.body)
    }
    UserService.addLog(logdata)
    return res.respondWithData(item)
  }
}

export async function getSearchHistory(
  req: Request,
  res: Response
): Promise<any> {
  const { user } = req
  const { pagination } = req
  const data = [] as any[]
  try {
    const result = await LogService.getSearchLogsWithFilter({user: user.claims._id, status:1}, pagination)
    pagination.total = await LogService.getSearchLogsCountDocument({user: user.claims._id, status:1})
    result.map((el:any) => {
      data.push({
        _id: el._id,
        value: el.value,
        createdAt: el.createdAt
      })
    })
  }
  catch(error) {
    return res.badRequest(new Error(error))
  }
  finally{
    return res.respondWithData(data, pagination)
  }
}

export async function deleteSearchHistory(
  req: Request,
  res: Response
): Promise<any> {
  const { user } = req
  const { id } = req.params
  try {
    const result = await LogService.updateSearchLog(new Types.ObjectId(id), { status:0 })
  }
  catch(error) {
    return res.badRequest(new Error(error))
  }
  finally{
    const params = { ...req.params, ...req.query}
    const logdata: IUserLogCreate = {
      user: user.claims._id,
      type: 'DeleteSearch',
      address: req.ip || req.socket.remoteAddress,
      user_agent: req.headers['user-agent'],
      params: JSON.stringify(params),
      data: JSON.stringify(req.body)
    }
    UserService.addLog(logdata)
    return res.respondWithData("Success")
  }
}

export async function deleteAllSearchHistory(
  req: Request,
  res: Response
): Promise<any> {
  const { user } = req
  const { id } = req.params
  try {
    const result = await LogService.deleteUserLogs(user.claims._id)
  }
  catch(error) {
    return res.badRequest(new Error(error))
  }
  finally{
    const params = { ...req.params, ...req.query}
    const logdata: IUserLogCreate = {
      user: user.claims._id,
      type: 'DeleteAllSearch',
      address: req.ip || req.socket.remoteAddress,
      user_agent: req.headers['user-agent'],
      params: JSON.stringify(params),
      data: JSON.stringify(req.body)
    }
    UserService.addLog(logdata)
    return res.respondWithData("Operation 'Extermination' success")
  }
}

export async function getCommonSearchs(
  req: Request,
  res: Response
): Promise<any> {
  const { user } = req
  let commonSearchs
  try {
    commonSearchs = await LogService.getSearchLogCounts({ limit:5, page:5 })
    console.log("🚀 ~ file: item.controller.ts:96 ~ result:", commonSearchs)
  }
  catch(error) {
    return res.badRequest(new Error(error))
  }
  finally{
    return res.respondWithData(commonSearchs)
  }
}

export async function getViewedItems(
  req: Request,
  res: Response
):Promise<any> {
  const { type } = req.query
  const { pagination, user } = req
  const itemIds = [] as any[]
  let items
  try{
    if(!type){
      throw "Type is required!(artwork, archive, koha, heritage, all)"
    }
    const viewedItems = await UserService.getUserLogsByFilter({user:user.claims._id, type:"ViewItem"}, {}, pagination)
    pagination.total = await UserService.getUserLogsCount({user:user.claims._id, type:"ViewItem"})
    let count = 0
    viewedItems.map((el:any) =>{
      if(JSON.parse(el.params).type == type){
        count++
        console.log("🚀 ~ file: item.controller.ts:415 ~ viewedItems.map ~ JSON.parse(el.params).type == type:", JSON.parse(el.params).type == type, count)
        itemIds.push(JSON.parse(el.params).id)
      }
    })
    console.log("🚀 ~ itemIds:", itemIds)
    console.log("🚀🚀🚀🚀🚀 ~ file: item.controller.ts:422 ~ req.query:", itemIds)
    if(type == "artwork"){
      const result = await ArtworkItemService.getItemsByIds(itemIds, pagination)
      pagination.artwork = count
      items = result.data
    }
    else if(type == "archive"){
      const result = await HeritageService.getHeritagesByIds(itemIds, pagination)
      pagination.archive = count
      result.data.map((el:any) =>{
        el.type = 'archive'
      })
      items = result.data
    }
    else if(type == "koha"){
      const result = await LibraryKohaService.getKohaByBibliosFromCache(itemIds)
      pagination.koha = count
      items = result.data
    }
    else if(type == "heritage"){
      const result = await HeritageService.getHeritagesByIds(itemIds, pagination)
      pagination.heritage = count
      items = result.data
    }
  }
  catch(error){
    return res.badRequest(new Error(error))
  }
  finally{
    const params = { ...req.params, ...req.query}
    const logdata: IUserLogCreate = {
    user: user.claims._id,
    type: 'ViewItem',
    address: req.ip || req.socket.remoteAddress,
    user_agent: req.headers['user-agent'],
    params: JSON.stringify(params),
    data: JSON.stringify(req.body)
    }
    UserService.addLog(logdata)
    return res.respondWithData(items, pagination)
  }
}

export async function getViewedItemsAll(
  req: Request,
  res: Response
):Promise<any> {
  const { pagination, user } = req
  const artworkIDs = [] as any[], heritageIDs = [] as any[], archiveIDs = [] as any[], kohaIDs = [] as any[], result= [] as any[]
  try{
    const viewedItems = await UserService.getUserLogsByFilter({user:user.claims._id, type:"ViewItem"}, {}, pagination)
    pagination.total = await UserService.getUserLogsCount({user:user.claims._id, type:"ViewItem"})
    viewedItems.map((el:any) =>{
      switch (JSON.parse(el.params).type){
        case "artwork":
          artworkIDs.push(JSON.parse(el.params).id)
          break;
        case "archive":
          archiveIDs.push(JSON.parse(el.params).id)
          break;
        case "koha":
          kohaIDs.push(JSON.parse(el.params).id)
          break;
        case "heritage":
          heritageIDs.push(JSON.parse(el.params).id)
          break;
      }
    })
    const promises = [
      ArtworkItemService.getItemsByIds(artworkIDs, pagination),
      ArchiveService.getArchivesByIds(archiveIDs, pagination),
      LibraryKohaService.getKohaItemsWithBiblios(kohaIDs, pagination),
      HeritageService.getHeritagesByIds(heritageIDs, pagination)
    ]
    const data = await Promise.all(promises)
    data.map((el:any) => {
      result.push(...el.data)
    })
  }
  catch(error){
    return res.badRequest(new Error(error))
  }
  finally{
    // const params = { ...req.params, ...req.query}
    // const logdata: IUserLogCreate = {
    // user: user.claims._id,
    // type: '',
    // address: req.ip || req.socket.remoteAddress,
    // user_agent: req.headers['user-agent'],
    // params: JSON.stringify(params),
    // data: JSON.stringify(req.body)
    // }
    // UserService.addLog(logdata)
    return res.respondWithData(result, pagination)
  }
}

export async function getSearchSuggest(
  req: Request,
  res: Response
): Promise<any> {
  const sort = req.sort || 1
  const filter = req.query as any

  delete filter.limit 
  filter.page = 1
  filter.limit = 10
  
  let artworkResult, kohaResult, eduResult, heritageResult
  const data = []
  try {
    artworkResult = await ArtworkItemService.getItems(filter)
    data.push(...artworkResult.data)

    eduResult = await ArtworkItemService.getEduItems(filter)
    data.push(...eduResult.data)
    
    kohaResult = await KOHABiblioService.getBiblios(filter)
    data.push(...kohaResult.biblio)

    // heritageResult = await HeritageService.getHeritages(filter)
    // data.push(...heritageResult.data)
  }
  catch(error) {
    return res.internalError(new Error(error))
  }
  finally{
    // let finalData
    const finalData = SortHelper.sortByLetter(data, "name", sort)
    const result = finalData.slice(0, 10)
    return res.respondWithData(result)
  }
}

export async function saveSearchLog(
  req: Request,
  res: Response
): Promise<any> {
  const search = req.query.search as string
  const { user } = req
  let data:ICreateSearchLog
  try {
    if(search) {
      if( user === 'anonymous'){
        data = {
          value: search.toLowerCase(),
          user: "anonymous"
        }
      }
      else{
        data = {
          value: search.toLowerCase(),
          user: user.claims._id
        }
      }
      await LogService.pushSearchLog(data)
      await LogService.addSearchLogCount(search.toLowerCase())
    }else{
      throw "Search is undefined!"
    }
  }
  catch(error) {
    return res.badRequest(new Error(error))
  }
  finally{
    return res.respondWithData("Success :)")
  }
}

export async function megaTest(
  req: Request,
  res: Response
): Promise<any> {
  const { pagination } = req
  const filter = req.query as any
  delete filter.limit 
  filter.page = pagination.page
  filter.limit = pagination.limit
  console.log("🚀 ~ file: item.controller.ts:639 ~ filter:", filter)
  let test
  try {
    test = await HeritageService.getHeritages(filter)
  }
  catch(error) {
    return res.internalError(new Error(error))
  }
  finally{
    return res.respondWithData(test.data)
  }
}

// filter: { search: 'ту', sort: 'name', page: 1, limit: 6 }

export async function getViewCountsOrder(req: Request, res: Response): Promise <any> {
  const { pagination } = req
  console.log(pagination)
  const sort = req.sort || 1
  const type = req.query.type
  let filter = {}
  if(type){
    filter = Object.assign(filter, {"type":type})
  }
  try {
    const result = await ViewService.getAllViewCount(filter, pagination, sort)
    return res.respondWithData(result)
  } catch (error) {
    return res.internalError(new Error(error))
  }
}