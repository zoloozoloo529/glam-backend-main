import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { 
    deleteAllSearchHistory, deleteSearchHistory, getCommonSearchs, getItemById, getItems, getSearchHistory, getSearchSuggest, 
    getViewedItems, getViewedItemsAll, megaTest, saveSearchLog, searchItems, getViewCountsOrder } from './item.controller'
import { authenticationMiddleware, itemMiddleware, PaginationMiddleware, saveLog, UserValidate } from '@root/middlewares'
import sortMiddleware from '@root/middlewares/sort.middleware'
import { filterValidator } from '@root/validators'

const ItemRouter = Router()

ItemRouter.get('/', authenticationMiddleware.customMiddleware, PaginationMiddleware.paginate, sortMiddleware.sort, asyncHandler(getItems))
ItemRouter.post('/', authenticationMiddleware.customMiddleware, filterValidator.searchBody, asyncHandler(searchItems))
ItemRouter.get('/mega_test', authenticationMiddleware.customMiddleware, PaginationMiddleware.paginate, sortMiddleware.sort, asyncHandler(megaTest))
ItemRouter.get('/common', asyncHandler(getCommonSearchs))
ItemRouter.get('/search_suggest', asyncHandler(getSearchSuggest))
ItemRouter.get('/save_search', authenticationMiddleware.customMiddleware, asyncHandler(saveSearchLog))
ItemRouter.get('/view_history', authenticationMiddleware.checkJwt, PaginationMiddleware.paginate, asyncHandler(getViewedItems))
ItemRouter.get('/view_history_all', authenticationMiddleware.checkJwt, PaginationMiddleware.paginate, asyncHandler(getViewedItemsAll))
ItemRouter.get('/history', authenticationMiddleware.checkJwt, PaginationMiddleware.paginate,  asyncHandler(getSearchHistory))
ItemRouter.get('/:id/:type', authenticationMiddleware.customMiddleware, itemMiddleware.updateViewCount, asyncHandler(getItemById))
ItemRouter.delete('/search-history', authenticationMiddleware.checkJwt, asyncHandler(deleteAllSearchHistory))
ItemRouter.delete('/search-history/:id', authenticationMiddleware.checkJwt, UserValidate.validateUserWithHistory, asyncHandler(deleteSearchHistory))
ItemRouter.get('/viewCount', authenticationMiddleware.customMiddleware, PaginationMiddleware.paginate, sortMiddleware.sort, asyncHandler(getViewCountsOrder))

export default ItemRouter