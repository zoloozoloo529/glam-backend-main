import { ArtworkCategoryService } from '@root/services'
import { Request, Response } from 'express'

export async function getCategories (
  req: Request,
  res: Response
):Promise<any> {
  const parent = req.query.parent as string
  let result
  if(!parent) {
    result = await ArtworkCategoryService.getCategories()
  }
  else {
    result = await ArtworkCategoryService.getCategories(parent)
  }
  return res.respondWithData(result.data)
}