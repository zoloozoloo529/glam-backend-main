import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { getCategories } from './category.controller'

const CategoryRouter = Router()

CategoryRouter.get('/', asyncHandler(getCategories))

export default CategoryRouter