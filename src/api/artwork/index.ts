import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import CategoryRouter from './category'
import TagRouter from './tag'
import OrganizationRouter from './organization'

const ArtworkRouter = Router()

ArtworkRouter.use('/categories', asyncHandler(CategoryRouter))
ArtworkRouter.use('/tags', asyncHandler(TagRouter))
ArtworkRouter.use('/organizations', asyncHandler(OrganizationRouter))

export default ArtworkRouter