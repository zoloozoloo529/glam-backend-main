import { ArtworkTagService } from '@root/services'
import { Request, Response } from 'express'

export async function getTags (
  req: Request,
  res: Response
):Promise<any> {
  const result = await ArtworkTagService.getTags(req.query)
  return res.respondWithData(result.data, result.pagination)
}