import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { getTags } from './tag.controller'

const TagRouter = Router()

TagRouter.get('/', asyncHandler(getTags))

export default TagRouter