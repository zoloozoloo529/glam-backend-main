import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { getOrganizations } from './organization.controller'

const OrganizationRouter = Router()

OrganizationRouter.get('/', asyncHandler(getOrganizations))

export default OrganizationRouter