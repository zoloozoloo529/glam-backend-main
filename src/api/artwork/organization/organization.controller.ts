import { OrganizationService } from '@root/services'
import { Request, Response } from 'express'

export async function getOrganizations(
  req: Request,
  res: Response
):Promise<any> {
  const result = await OrganizationService.getOrganizations(req.query)
  return res.respondWithData(result.data, result.pagination)
}