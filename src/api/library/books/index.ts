import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { checkBiblioFromDL, getBookById, getBooks, getLibraryBookCategories, getLibraryBookTypes, getNewItems, getSimilarItems, getSpecialItems } from './book.controller'



const BookRouter = Router()


BookRouter.get('/special_books', asyncHandler(getSpecialItems))
BookRouter.get('/new_books', asyncHandler(getNewItems))
BookRouter.get('/category_list', asyncHandler(getLibraryBookCategories))
BookRouter.get('/type_list', asyncHandler(getLibraryBookTypes))
BookRouter.get('/check_biblio/:id', asyncHandler(checkBiblioFromDL))
BookRouter.get('/related_books/:id', asyncHandler(getSimilarItems))
BookRouter.get('/:id', asyncHandler(getBookById))
BookRouter.post('/find_books', asyncHandler(getBooks))

export default BookRouter