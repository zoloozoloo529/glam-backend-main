import config from "@root/config";
import { IPagination } from "@root/interfaces";
import { LibraryBookService } from "@root/services";
import { Request, Response } from "express";
import { text } from "pdfkit";

export async function getSpecialItems(
  req: Request,
  res: Response
):Promise<any> {
    let result
    const final = [] as any[]
    try{
        result = await LibraryBookService.getSpecialItems()
        console.log("🚀 ~ file: book.controller.ts:11 ~ result:", result)
        if(!result.success){
            throw result?.error?.message || "There was a problem connecting to the digital library."
        }
        result.data.map((el:any) => {
            final.push({
                _id: el.id.toString(),
                name: el.book_name,
                description: el.note,
                itemInf: {
                    artist:{
                        name: `${el.authors[0]?.last_name[0]}`+`. `+`${el.authors[0]?.first_name}` || ""
                        // name: el.authors[0]?.last_name || ""
                    }
                },
                // mainImg: "https://cdn.steppearena.mn/dev/glam/3419233.png",
                mainImg: `${config.digital_library.library_pro_base_url}/file/${el.url_image}`,
                fields: [
                    {
                        name: "ISBN Code",
                        t_value: el.isbn_code,
                        value: el.isbn_code,
                        type: "text",
                    },
                    {
                        name: "Хэвлэгдсэн он",
                        t_value: el.press_year,
                        value: el.press_year,
                        type: "text",
                    },
                ]
            })
        })
    }
    catch(error){
        return res.badRequest(new Error(error))
    }
    finally{
        return res.respondWithData(final)
    }
}

export async function getNewItems(
    req: Request,
    res: Response
  ):Promise<any> {
        const final = [] as any[]
      try{
          const result = await LibraryBookService.getNewBooks()
          if(!result.success){
              throw result?.error?.message || "There was a problem connecting to the digital library."
          }
          result.data.map((el:any) => {
            final.push({
                _id: el.id.toString(),
                name: el.book_name,
                description: el.note,
                itemInf: {
                    artist:{
                        name: `${el.authors[0]?.last_name[0]}`+`. `+`${el.authors[0]?.first_name}` || ""
                        // name: el.authors[0]?.last_name || ""
                    }
                },
                // mainImg: el.url_image,
                // mainImg: "https://cdn.steppearena.mn/dev/glam/3419233.png",
                mainImg: `${config.digital_library.library_pro_base_url}/file/${el.url_image}`,
                fields: [
                    {
                        name: "ISBN Code",
                        t_value: el.isbn_code,
                        value: el.isbn_code,
                        type: "text",
                    },
                    {
                        name: "Хэвлэгдсэн он",
                        t_value: el.press_year,
                        value: el.press_year,
                        type: "text",
                    },
                ]
            })
        })
      }
      catch(error){
        return res.badRequest(new Error(error))
      }
      finally{
          return res.respondWithData(final)
      }
}

export async function getSimilarItems(
    req: Request,
    res: Response
  ):Promise<any> {
    const { id } = req.params
    console.log("🚀 ~ file: book.controller.ts:110 ~ id:", id)
    const final = [] as any[]
      try{
          const result = await LibraryBookService.getSimilarBooks(Number(id))
          if(!result.success){
            //   throw "There was a problem connecting to the digital library."
            throw result?.error?.message || "There was a problem connecting to the digital library."
          }
          result.data.map((el:any) => {
            final.push({
                _id: el.id.toString(),
                name: el.book_name,
                description: el.note,
                itemInf: {
                    artist:{
                        name: `${el.authors[0]?.last_name[0]}`+`. `+`${el.authors[0]?.first_name}` || ""
                        // name: el.authors[0]?.last_name || ""
                    }
                },
                // mainImg: el.url_image,
                // mainImg: "https://cdn.steppearena.mn/dev/glam/3419233.png",
                mainImg: `${config.digital_library.library_pro_base_url}/file/${el.url_image}`,
                fields: [
                    {
                        name: "ISBN Code",
                        t_value: el.isbn_code,
                        value: el.isbn_code,
                        type: "text",
                    },
                    {
                        name: "Хэвлэгдсэн он",
                        t_value: el.press_year,
                        value: el.press_year,
                        type: "text",
                    },
                ]
            })
        })
      }
      catch(error){
        return res.badRequest(new Error(error))
      }
      finally{
          return res.respondWithData(final)
      }
}

export async function getBooks(
    req: Request,
    res: Response
  ):Promise<any> {
    const filter = req.body
    //   console.log("🚀 ~ file: book.controller.ts:81 ~ filters:", filter)
      let result
      const final = [] as any[]
      let pagination = {} as any
      try{
          result = await LibraryBookService.getBooks(filter)
          console.log("🚀 ~ file: book.controller.ts:167 ~ result:", result)
          if(!result.success){
              throw result?.error?.message || "There was a problem connecting to the digital library."
          }
          result.data.map((el:any) => {
              final.push({
                  _id: el.id.toString(),
                  name: el.book_name,
                //   description: el.note,
                  itemInf: {
                      artist:{
                          name: `${el.authors[0]?.last_name[0]}`+`. `+`${el.authors[0]?.first_name}` || ""
                          // name: el.authors[0]?.last_name || ""
                      }
                  },
                  // mainImg: el.url_image,
                  // mainImg: "https://cdn.steppearena.mn/dev/glam/3419233.png",
                  mainImg: `${config.digital_library.library_pro_base_url}/file/${el.url_image}`,
                //   fields: [
                //       {
                //           name: "ISBN Code",
                //           t_value: el.isbn_code,
                //           value: el.isbn_code,
                //           type: "text",
                //       },
                //       {
                //           name: "Хэвлэгдсэн он",
                //           t_value: el.press_year,
                //           value: el.press_year,
                //           type: "text",
                //       },
                //   ]
                  type: "library"
              })
          })
          pagination.limit = result.metaData.recordsPerPage
          pagination.page = result.metaData.pageNumber
          pagination.total = result.metaData.numberOfRecords
          if(pagination.total%pagination.limit){
            pagination.total_page = Math.floor(pagination.total/pagination.limit) + 1
          }else{
            pagination.total_page = Math.floor(pagination.total/pagination.limit)
          }
      }
      catch(error){
        return res.badRequest(new Error(error))
      }
      finally{
          return res.respondWithData(final, pagination)
      }
  }

export async function getBookById(
    req: Request,
    res: Response
):Promise<any> {
    const { id } = req.params
    let result
    let final
    try{
        result = await LibraryBookService.getBook(id)
        if(!result.success){
            throw result?.error?.message || "There was a problem connecting to the digital library."
        }
        final = {
            _id: result.data.id.toString(),
            name: result.data.book_name,
            description: result.data.note,
            itemInf: {
                artist:{
                    name: `${result.data.authors[0]?.last_name[0]}`+`. `+`${result.data.authors[0]?.first_name}` || ""
                }
            },
            file_type: result.data.dl_book_file_types || [],
            // mainImg: "https://cdn.steppearena.mn/dev/glam/3419233.png",
            mainImg: `${config.digital_library.library_pro_base_url}/file/${result.data.url_image}`,
              fields: [
                {
                    name: "Ангилал",
                    value: result.data.book_category_name,
                    type: "text",
                },
                {
                    name: "Төрөл",
                    value: result.data.book_type_name,
                    type: "text",
                },
                {
                    name: "Хуудас",
                    value: result.data.page_number,
                    type: "number",
                },
                {
                    name: "Үнэ",
                    value: result.data.price,
                    type: "text",
                },
                {
                    name: "Хямдрал",
                    value: result.data.discount,
                    type: "text",
                },
                {
                    name: "Хямдарсан үнэ",
                    value: result.data.discount_price,
                    type: "text",
                },
                {
                    name: "Хямдрал дуусах",
                    value: result.data.discount_end,
                    type: "text",
                },
                {
                    name: "ISBN Code",
                    value: result.data.isbn_code,
                    type: "text",
                },
                {
                    name: "Хэвлэгдсэн он",
                    value: result.data.press_year,
                    type: "text",
                },
                {
                    name: "Хэл",
                    value: result.data.languages_koha,
                    type: "text",
                },
                {
                    name: "Хамтрагч",
                    value: result.data.partner_name,
                    type: "text",
                },
                {
                    name: "Хамтрагч лого",
                    value: result.data.partner_image_url,
                    type: "link",
                },
              ],
              type: "library"
        }
    }
    catch(error){
        return res.badRequest(new Error(error))
    }
    finally{
        return res.respondWithData(final)
    }
}

export async function getLibraryBookCategories(
    req: Request,
    res: Response
  ):Promise<any> {
      let result
      try{
          result = await LibraryBookService.getCategories()
          if(!result.success){
              throw result?.error?.message || "There was a problem connecting to the digital library."
          }
      }
      catch(error){
        return res.badRequest(new Error(error))
      }
      finally{
          return res.respondWithData(result.data)
      }
}

export async function getLibraryBookTypes(
    req: Request,
    res: Response
  ):Promise<any> {
      let result
      try{
          result = await LibraryBookService.getTypes()
          if(!result.success){
              throw result?.error?.message || "There was a problem connecting to the digital library."
          }
      }
      catch(error){
        return res.badRequest(new Error(error))
      }
      finally{
          return res.respondWithData(result.data)
      }
}

export async function checkBiblioFromDL(
    req: Request,
    res: Response
  ):Promise<any> {
    const { id } = req.params
    let data
      try{
        const result = await LibraryBookService.checkBiblioFromDL(id)
        if(result.success){
            data ={
                id: result.data.id,
                isDigital:result.success
            }
        }else{
            data={
                isDigital:result.success
            }
        }
      }
      catch(error){
        return res.badRequest(new Error(error))
      }
      finally{
          return res.respondWithData(data)
      }
}