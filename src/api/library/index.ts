import asyncHandler from 'express-async-handler'
import { Router } from 'express'
import BookRouter from './books'
import { connectToDL, getDLtoken, koha_book_take_list } from './library.controller'
import { authenticationMiddleware, PaginationMiddleware } from '@root/middlewares'

const LibraryRouter = Router()

LibraryRouter.post('/connect', authenticationMiddleware.checkJwt, asyncHandler(connectToDL))
LibraryRouter.get('/get_token', authenticationMiddleware.checkJwt, asyncHandler(getDLtoken))
LibraryRouter.get('/get_koha_take_list', authenticationMiddleware.checkJwt, PaginationMiddleware.paginate, asyncHandler(koha_book_take_list))

LibraryRouter.use('/books', asyncHandler(BookRouter))

export default LibraryRouter