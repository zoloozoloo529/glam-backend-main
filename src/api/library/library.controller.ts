import config from '@root/config'
import fetch from 'node-fetch'
import { IKohaItemCreate, IUpdateUser, IUserLogCreate } from '@root/interfaces'
import { FetchService, LibraryAuthService, LibraryKohaService, UserService } from '@root/services'
import { CryptoHelpers } from '@root/utils'
import { Request, Response } from 'express'
import { Types } from 'mongoose'

export async function connectToDL(req: Request, res: Response): Promise<any> {
    const { library_password } = req.body
    const { user } = req
    let respond
    try {
        const result = await LibraryAuthService.login(
            user.claims.email,
            library_password
        )
        console.log('🚀 ~ file: library.controller.ts:16 ~ result:', result)
        if (result.success) {
            const encryptedPassword = CryptoHelpers.encrypt(library_password)
            const updateUser: IUpdateUser = {
                library_password: encryptedPassword,
                library_isConnected: true,
                library_access_token: result.data.access_token
            }
            await UserService.updateUser(
                new Types.ObjectId(user.claims._id),
                updateUser
            )
            respond = 'success'
        } else {
            if (result.error.code === 'INFO_UN_REGISTERD_EMAIL') {
                throw 'Таны и-мэйл хаяг Дижитал номын санд бүртгэлгүй байгаа тул бүртгэл үүсгэнэ үү!'
            } else {
                throw 'Нууц үг буруу байна.'
            }
        }
    } catch (error) {
        return res.badRequest(new Error(error))
    } finally {
        const params = { ...req.params, ...req.query }
        const logdata: IUserLogCreate = {
            user: user.claims._id,
            type: 'ConnectDL',
            address: req.ip || req.socket.remoteAddress,
            user_agent: req.headers['user-agent'],
            params: JSON.stringify(params),
            data: JSON.stringify(req.body)
        }
        UserService.addLog(logdata)
        return res.respondWithData(respond)
    }
}

export async function getDLtoken(req: Request, res: Response): Promise<any> {
    const { user } = req
    let respond
    try {
        const userData = await UserService.getUserById(
            new Types.ObjectId(user.claims._id)
        )
        if (!userData.library_isConnected) {
            throw 'Please, connect the Digital library first'
        }
        const result = await LibraryAuthService.login(
            user.claims.email,
            CryptoHelpers.decrypt(userData.library_password)
        )
        if (result.success) {
            const updateUser: IUpdateUser = {
                library_access_token: result.data.access_token
            }
            await UserService.updateUser(
                new Types.ObjectId(user.claims._id),
                updateUser
            )
            respond = result.data.access_token
        } else {
            if (result.error.code === 'INFO_UN_REGISTERD_EMAIL') {
                throw 'Таны и-мэйл хаяг Дижитал номын санд бүртгэлгүй байгаа тул бүртгэл үүсгэнэ үү!'
            } else {
                throw 'Хадгалсан нууц үг буруу байна.'
            }
        }
    } catch (error) {
        return res.badRequest(new Error(error))
    } finally {
        return res.respondWithData(respond)
    }
}

export async function koha_book_take_list(
    req: Request,
    res: Response
): Promise<any> {
    const { user } = req
    const search = req.query.search as string
    const sortBy = req.query.sortBy as string
    const sortOrder = req.query.sortOrder as string
    const { pagination } = req
    let respond
    try {
        const userData = await UserService.getUserById(
            new Types.ObjectId(user.claims._id)
        )
        if (!userData.library_isConnected) {
            throw 'Please, connect the Digital library first'
        }
        const result = await LibraryAuthService.koha_book_take_list(
            userData.library_access_token,
            search,
            pagination.page.toString(),
            pagination.limit.toString(),
            sortBy,
            sortOrder
        )
        if (result.success) {
            respond = result.data
            pagination.total = result.metaData.numberOfRecords
            pagination.total_page = result.metaData.numberOfPages
        } else {
            if (result.error.code === 'WARN_LOGIN_REQUIRED') {
                const getToken = await LibraryAuthService.login(
                    user.claims.email,
                    CryptoHelpers.decrypt(userData.library_password)
                )
                if (getToken.success) {
                    const updateUser: IUpdateUser = {
                        library_access_token: getToken.data.access_token
                    }
                    await UserService.updateUser(
                        new Types.ObjectId(user.claims._id),
                        updateUser
                    )
                    const resultWithNewToken =
                        await LibraryAuthService.koha_book_take_list(
                            userData.library_access_token,
                            search,
                            pagination.page.toString(),
                            pagination.limit.toString(),
                            sortBy,
                            sortOrder
                        )
                    if (resultWithNewToken.success) {
                        respond = resultWithNewToken.data
                        pagination.total =
                            resultWithNewToken.metaData.numberOfRecords
                        pagination.total_page =
                            resultWithNewToken.metaData.numberOfPages
                    } else {
                        throw 'Алдаа гарлаа///'
                    }
                } else {
                    if (result.error.code === 'INFO_UN_REGISTERD_EMAIL') {
                        throw 'Таны и-мэйл хаяг Дижитал номын санд бүртгэлгүй байгаа тул бүртгэл үүсгэнэ үү!'
                    } else {
                        throw 'Хадгалсан нууц үг буруу байна.'
                    }
                }
            }
        }
    } catch (error) {
        return res.badRequest(new Error(error))
    } finally {
        return res.respondWithData(respond, pagination)
    }
}