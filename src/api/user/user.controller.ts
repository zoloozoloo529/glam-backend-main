import { UserService } from '@root/services'
import { Request, Response } from 'express'
import { Types } from 'mongoose'

export async function me(req: Request, res: Response): Promise<void> {
    const { user } = req
    console.log('claims: ', user)
    const existUser = await UserService.getUserById(
        new Types.ObjectId(user.claims._id)
    )
    if (!existUser) {
        throw 'User not found'
    }
    // console.log('user: ', existUser)
    res.respondWithData({
        email: existUser.email,
        firstname: existUser.firstname || null,
        lastname: existUser.lastname || null,
        img: existUser.img || null,
        age: existUser.age || null,
        isForeigner: existUser.isForeigner,
        country: existUser.country
    })
}

export async function deleteUser(req: Request, res: Response): Promise<void> {
    const { user } = req
    const existUser = await UserService.getUserById(
        new Types.ObjectId(user.claims._id)
    )
    if (!existUser) {
        res.respondWithError(new Error("Хэрэглэгчийн бүртгэл олдсонгүй!"),400)
    } else {
        const result = await UserService.deleteUser(user.claims._id)
        res.respondWithData({result})
    }
}