import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { me, deleteUser } from './user.controller'

const UserRouter = Router()

UserRouter.get('/me', asyncHandler(me))
UserRouter.delete('/', asyncHandler(deleteUser))

export default UserRouter