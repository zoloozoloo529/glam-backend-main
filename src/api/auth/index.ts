import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import {
    register,
    login,
    resetPassword,
    forgotPassword,
    verify,
    resend,
    editInfo,
    getJWKSKeys
} from './auth.controller'
import { authenticationMiddleware } from '@root/middlewares'
import { userValidator } from '@root/validators'
import { access_granted, getDanInfo, getDANLoginRedirect } from './dan.controller'

const AuthRouter = Router()

AuthRouter.post('/register', userValidator.register, asyncHandler(register))
AuthRouter.post('/login', asyncHandler(login))
AuthRouter.patch('/verify', asyncHandler(verify))
AuthRouter.patch('/resend', asyncHandler(resend))
AuthRouter.patch('/forgotPassword', asyncHandler(forgotPassword))
AuthRouter.patch(
    '/changePassword',
    authenticationMiddleware.checkJwt,
    userValidator.resetPassword,
    asyncHandler(resetPassword)
)
AuthRouter.patch(
    '/info',
    authenticationMiddleware.checkJwt,
    asyncHandler(editInfo)
)

AuthRouter.get('/jwks', asyncHandler(getJWKSKeys))

AuthRouter.get('/dan', authenticationMiddleware.checkJwt, asyncHandler(getDANLoginRedirect))
AuthRouter.get('/access_grant', asyncHandler(access_granted))
AuthRouter.get('/dan_info', authenticationMiddleware.checkJwt, asyncHandler(getDanInfo))

export default AuthRouter
