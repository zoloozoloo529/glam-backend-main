import { ICreateUser, IUpdateUser, IUserLogCreate } from '@root/interfaces'
import { EmailService, UserService, FolderService } from '@root/services'
import { Request, Response } from 'express'
import { Types } from 'mongoose'
import fs from 'fs'
import jose from 'node-jose'
import { CryptoHelpers } from '@root/utils'

export async function register(req: Request, res: Response): Promise<any> {
    const { email, password, firstname, lastname, img, isForeigner, country } =
        req.body
    const filter = { email: email }
    let user
    try {
        const existUsers = await UserService.getUser(filter)
        const verification = Math.floor(100000 + Math.random() * 900000)

        if (existUsers) {
            throw 'Хэрэглэгч бүртгэлтэй байна.'
        }
        const encryptedPassword = CryptoHelpers.encrypt(password)
        const data: ICreateUser = {
            email,
            password: encryptedPassword,
            verification: `${verification}`,
            firstname,
            lastname,
            img,
            isForeigner,
            country
        }

        const emailData = {
            to: email,
            subject: 'GLAM: Баталгаажуулах',
            html: `Баталгаажуулах код: <b>${verification}</b>`
        }
        user = await UserService.createUser(data)
        FolderService.createFolder({
            user: user._id,
            type: 'all',
            name: 'Бүгд',
            color: 'F15E22'
        })
        EmailService.sendMail(emailData)
    } catch (error) {
        return res.badRequest(new Error(error))
    } finally {
        const params = { ...req.params, ...req.query }
        const logdata: IUserLogCreate = {
            user: user._id,
            type: 'Register',
            address: req.ip || req.socket.remoteAddress,
            user_agent: req.headers['user-agent'],
            params: JSON.stringify(params),
            data: JSON.stringify(req.body)
        }
        UserService.addLog(logdata)
        return res.respondWithData({ email: email })
    }
}

export async function login(req: Request, res: Response): Promise<any> {
    const { email, password } = req.body
    let token, user
    try {
        const encryptedPassword = CryptoHelpers.encrypt(password)
        const login = {
            email,
            password: encryptedPassword
        }
        user = await UserService.getUser(login)
        if (!user) {
            throw 'Нэр эсвэл нууц үг буруу байна.'
        }
        if (user.activeStatus == 0) {
            throw 'Таны нэвтрэх эрх хаагдсан байна!'
        }
        const JWKeys = fs.readFileSync('Keys.json')
        const keyStore = await jose.JWK.asKeyStore(JWKeys.toString())
        const [key] = keyStore.all({ use: 'sig' })
        const opt = { compact: true, jwk: key, fields: { typ: 'jwt' } }

        const payload = JSON.stringify({
            exp: Math.floor(Date.now() + 864000),
            iat: Math.floor(Date.now() / 1000),
            claims: {
                email: user.email,
                role: user.role,
                _id: user._id,
                verified: user.verified
            }
        })

        token = await jose.JWS.createSign(opt, key).update(payload).final()
    } catch (error) {
        return res.badRequest(new Error(error))
    } finally {
        if (!user.verified) {
            return res.respondWithData({
                verified: user.verified,
                email: email
            })
        } else {
            const params = { ...req.params, ...req.query }
            const logdata: IUserLogCreate = {
                user: user._id,
                type: 'Login',
                address: req.ip || req.socket.remoteAddress,
                user_agent: req.headers['user-agent'],
                params: JSON.stringify(params),
                data: JSON.stringify(req.body)
            }
            UserService.addLog(logdata)
            return res.respondWithData({
                accessToken: token,
                verified: user.verified,
                email: email
            })
        }
    }
}

export async function verify(req: Request, res: Response): Promise<any> {
    const { email, verification } = req.body
    let existingUser
    try {
        existingUser = await UserService.getUser({
            email: email,
            verification: verification
        })
        if (!existingUser) {
            throw 'Баталгаажуулах код буруу байна.'
        }
        await UserService.updateUser(new Types.ObjectId(existingUser._id), {
            verified: true
        })
    } catch (error) {
        return res.badRequest(new Error(error))
    } finally {
        const params = { ...req.params, ...req.query }
        const logdata: IUserLogCreate = {
            user: existingUser._id,
            type: 'Verify',
            address: req.ip || req.socket.remoteAddress,
            user_agent: req.headers['user-agent'],
            params: JSON.stringify(params),
            data: JSON.stringify(req.body)
        }
        UserService.addLog(logdata)
        return res.respondWithData({ verified: true })
    }
}

export async function resend(req: Request, res: Response): Promise<any> {
    const { email } = req.body
    let existUser
    try {
        existUser = await UserService.getUser({ email: email })
        console.log('user:', existUser)
        if (!existUser) {
            throw 'Хэрэглэгч олдсонгүй'
        }
        const verification = Math.floor(100000 + Math.random() * 900000)
        const data = {
            verification: `${verification}`
        }
        const emailData = {
            to: email,
            subject: 'GLAM: Баталгаажуулах',
            html: `Баталгаажуулах код: <b>${verification}</b>`
        }
        await UserService.updateUser(new Types.ObjectId(existUser._id), data)
        await EmailService.sendMail(emailData)
    } catch (error) {
        return res.badRequest(new Error(error))
    } finally {
        const params = { ...req.params, ...req.query }
        const logdata: IUserLogCreate = {
            user: existUser._id,
            type: 'Resend',
            address: req.ip || req.socket.remoteAddress,
            user_agent: req.headers['user-agent'],
            params: JSON.stringify(params),
            data: JSON.stringify(req.body)
        }
        UserService.addLog(logdata)
        return res.respondWithData({ email: email })
    }
}

export async function resetPassword(
    req: Request,
    res: Response
): Promise<any> {
    const { newPassword, oldPassword } = req.body
    const { user } = req
    let result
    try {
        const encryptedPassword = CryptoHelpers.encrypt(oldPassword)
        const filter = {
            email: req.user.claims.email,
            password: encryptedPassword
        }
        result = await UserService.getUser(filter)
        if (!result) {
            throw 'Нууц үг буруу байна.'
        }
        const encryptedNewPassword = CryptoHelpers.encrypt(newPassword)
        const data = {
            password: encryptedNewPassword
        }
        const userId = user.claims._id
        await UserService.updateUser(new Types.ObjectId(userId), data)
    } catch (error) {
        return res.badRequest(new Error(error))
    } finally {
        const params = { ...req.params, ...req.query }
        const logdata: IUserLogCreate = {
            user: result._id,
            type: 'ResetPassword',
            address: req.ip || req.socket.remoteAddress,
            user_agent: req.headers['user-agent'],
            params: JSON.stringify(params),
            data: JSON.stringify(req.body)
        }
        UserService.addLog(logdata)
        return res.respondWithData('Амжилттай')
    }
}

export async function forgotPassword(
    req: Request,
    res: Response
): Promise<any> {
    const { email } = req.body
    let existUsers
    try {
        existUsers = await UserService.getUser({ email: email })
        if (!existUsers) {
            throw 'Хэрэглэгч олдсонгүй'
        }
        const newPassword = await makePassword(10)

        const emailData = {
            to: email,
            subject: 'GLAM: Нууц үг сэргээх.',
            html: `Нууц үг: <b>${newPassword}</b>`
        }

        const encryptedPassword = CryptoHelpers.encrypt(newPassword)
        const data = {
            password: encryptedPassword
        }
        await UserService.updateUser(new Types.ObjectId(existUsers._id), data)
        const sentInfo = await EmailService.sendMail(emailData)
        if (sentInfo.accepted.length === 0) {
            throw 'Failed to sent mail'
        }
    } catch (error) {
        return res.badRequest(new Error(error))
    } finally {
        const params = { ...req.params, ...req.query }
        const logdata: IUserLogCreate = {
            user: existUsers._id,
            type: 'ForgetPassword',
            address: req.ip || req.socket.remoteAddress,
            user_agent: req.headers['user-agent'],
            params: JSON.stringify(params),
            data: JSON.stringify(req.body)
        }
        UserService.addLog(logdata)
        return res.respondWithData(
            'Таны и-мэйл хаяг руу нэг удаагийн нууц үг илгээгдлээ'
        )
    }
}

export async function editInfo(req: Request, res: Response): Promise<any> {
    const { user } = req
    let result
    try {
        const { img, firstname, lastname, age, fcm_token } = req.body
        const data: IUpdateUser = {
            firstname,
            lastname,
            img,
            age,
            fcm_token
        }
        result = await UserService.updateUser(
            new Types.ObjectId(user.claims._id),
            data
        )
    } catch (error) {
        return res.internalError(new Error(error))
    } finally {
        const params = { ...req.params, ...req.query }
        const logdata: IUserLogCreate = {
            user: user !== 'anonymous' ? user.claims._id : 'anonymous',
            type: 'EditInfo',
            address: req.ip || req.socket.remoteAddress,
            user_agent: req.headers['user-agent'],
            params: JSON.stringify(params),
            data: JSON.stringify(req.body)
        }
        UserService.addLog(logdata)
        return res.respondWithData({
            email: result.email,
            firstname: result.firstname,
            lastname: result.lastname,
            img: result.img,
            age: result.age
        })
    }
}

export async function getJWKSKeys(req: Request, res: Response): Promise<any> {
    const ks = fs.readFileSync('Keys.json')

    const keyStore = await jose.JWK.asKeyStore(ks.toString())

    return res.send(keyStore.toJSON())
}

const makePassword = async (length: number) => {
    const charset =
        'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ000111222333444555666777888999'
    let retVal = ''
    for (let i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n))
    }
    return retVal
}
