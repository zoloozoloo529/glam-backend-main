import config from '@root/config'
import { FetchService, UserService } from '@root/services';
import { Types } from 'mongoose'
import { Request, Response } from 'express'
import { IUpdateUser } from '@root/interfaces';


export async function getDANLoginRedirect(
    req: Request,
    res: Response
): Promise<any> {
    let result
    const { user } = req
    try {
        const header = {
            action: 'DAN_Login',
            id: user.claims._id,
            code: await makePassword(10)
        };
        const state = Buffer.from(JSON.stringify(header)).toString('base64');
        console.log('🚀🚀🚀🚀🚀-CHECKINGGGGGG:', state);

        const service_structure = [
            {
                'services': [
                    "WS100101_getCitizenIDCardInfo",
                ],
                'wsdl': "https://xyp.gov.mn/citizen-1.3.0/ws?WSDL",

            },
        ]
        const encoded = btoa(JSON.stringify(service_structure))
        console.log("🚀 ~ file: dan.controller.ts:31 ~ encoded:", encoded)
        result = `${config.dan.base_url}/login?response_type=code&client_id=${config.dan.client_id}&redirect_uri=${config.dan.redirect_url}&scope=${encoded}&state=${state}`
    }
    catch (error) {
        return res.badRequest(new Error(error))
    }
    finally {
        //   const params = { ...req.params, ...req.query}
        //   const logdata: IUserLogCreate = {
        //   user: user.claims._id,
        //   type: 'CreateFolder',
        //   address: req.ip || req.socket.remoteAddress,
        //   user_agent: req.headers['user-agent'],
        //   params: JSON.stringify(params),
        //   data: JSON.stringify(req.body)
        //   }
        //   UserService.addLog(logdata)
        return res.respondWithData(result)
    }
}

export async function access_granted(
    req: Request,
    res: Response
): Promise<any> {
    console.log("🚀🚀🚀🚀🚀🚀🚀🚀🚀REDIRECTED🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀")
    console.log("🚀🚀 query:", req.query)
    let result
    const { code, state } = req.query
    try {
        if (!state || !code) {
            throw "Error - state or code missing."
        }
        const header = JSON.parse(Buffer.from(state as string, 'base64').toString('utf-8'));

        console.log("🚀 ~ file: dan.controller.ts:31 ~ header", header);
        console.log(typeof header.id, header.id);

        const user = await UserService.getUserById(new Types.ObjectId(header.id))

        console.log("🚀 ~ file: dan.controller.ts:31 ~ user", user);

        if (!user) {
            throw "Error - user not found."
        }
        result = header
        const token = await FetchService.post(`${config.dan.base_url}/oauth2/token?grant_type=authorization_code&code=${code}&client_secret=${config.dan.client_secret}&client_id=${config.dan.client_id}&redirect_uri=${config.dan.redirect_url}`, {})
        console.log("🚀 ~ token:", token)
        if (!token.access_token) {
            throw "Error - Failed to receive token."
        }
        const options = {
            authorization: `Bearer ${token.access_token}`
        }
        const data = await FetchService.get(`${config.dan.base_url}/oauth2/api/v1/service`, options)
        console.log("🚀 ~ data:", data)
        if (data.error) {
            throw "Error - data missing."
        } else {
            const citizenIDCardInfo = data.filter((el: any) => el.services.hasOwnProperty('WS100101_getCitizenIDCardInfo'))
            const updateUser: IUpdateUser = {
                dan: {
                    isConnected: true,
                    data: citizenIDCardInfo
                }
            }
            await UserService.updateUser(new Types.ObjectId(header.id), updateUser)
        }
    }
    catch (error) {
        return res.resourceForbidden(new Error(error))
    }
    finally {
        return res.respondWithData({})
    }
}

export async function getDanInfo(
    req: Request,
    res: Response
): Promise<any> {
    const { user } = req
    try {
        const userData = await UserService.getUserById(user.claims._id)
        if (!userData.dan.isConnected) {
            throw "First, connect to DAN"
        }
        return res.respondWithData(userData.dan.data)
    }
    catch (error) {
        return res.documentNotFound(new Error(error))
    }
    finally {
        // return res.respondWithData(userData)
    }
}

//  eyJhY3Rpb24iOiJMb2dpbiIsImlkIjoiNjRiNjA3NGE0MWM3NzA5M2E3N2NjMjI0In0=

const makePassword = async (length: number) => {
    const charset =
        'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ000111222333444555666777888999'
    let retVal = ''
    for (let i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n))
    }
    return retVal
}