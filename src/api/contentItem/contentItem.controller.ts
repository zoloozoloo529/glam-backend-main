import { Request, Response } from 'express'
import ContentItemService from '@root/services/contentItem.service'

export async function getContentItems(
    req: Request,
    res: Response
): Promise<void> {
    try {
        const publicCretiria = {
            hidden: false,
            status: 1
        }
        const result = await ContentItemService.find(publicCretiria)

        res.respondWithData(result)
    } catch (error) {
        res.internalError(new Error(error))
    }
}
