import { Router } from 'express'
import asyncHandler from 'express-async-handler'
import { getContentItems } from './contentItem.controller'

const ContentItemRouter = Router()

ContentItemRouter.get('/', asyncHandler(getContentItems))

export default ContentItemRouter
